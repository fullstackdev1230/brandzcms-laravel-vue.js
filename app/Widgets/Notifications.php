<?php

namespace App\Widgets;

use App\Repositories\Notifications\NotificationsRepositoryInterface;
use Arrilot\Widgets\AbstractWidget;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use TCG\Voyager\Facades\Voyager;

class Notifications extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];
    protected $notification;

    public function __construct(NotificationsRepositoryInterface $notification)
    {
        $this->notification = $notification;
    }

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $count = $this->notification->getModel()->count();
        $string = trans_choice('dimmer.notification', $count);

        return view('widgets.notifications', array_merge($this->config, [
            'icon'   => 'voyager-chat',
            'title'  => "{$string}",
            'text'   => __('dimmer.notification_text', ['string' => Str::lower($string)]),
            'button' => [
                'text' => __('dimmer.notification_link_text'),
                'link' => route('voyager.notifications.index')
            ],
            'image' => voyager_asset('images/widget-backgrounds/0.jpg'),
            'notifications' => $this->notification->all()

        ]));
    }

    /**
     * Determine if the widget should be displayed.
     *
     * @return bool
     */
    public function shouldBeDisplayed()
    {
        return Auth::user()->can('browse', Voyager::model('User'));
    }
}
