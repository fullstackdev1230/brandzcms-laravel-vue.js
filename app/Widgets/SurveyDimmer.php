<?php

namespace App\Widgets;

use App\Repositories\Surveys\SurveysRepositoryInterface;
use Arrilot\Widgets\AbstractWidget;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use TCG\Voyager\Facades\Voyager;

class SurveyDimmer extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];
    protected $survey;

    public function __construct(SurveysRepositoryInterface $survey)
    {
        $this->survey = $survey;
    }

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $count = $this->survey->getModel()->count();
        $string = trans_choice('dimmer.survey', $count);

        return view('voyager::dimmer', array_merge($this->config, [
            'icon'   => 'voyager-gift',
            'title'  => "{$count} {$string}",
            'text'   => __('dimmer.survey_text', ['count' => $count, 'string' => Str::lower($string)]),
            'button' => [
                'text' => __('dimmer.survey_link_text'),
                'link' => route('voyager.surveys.index'),
                'add_new' => route('voyager.surveys.create'),
            ],
            'image' => voyager_asset('images/widget-backgrounds/04.jpg'),
        ]));
    }

    /**
     * Determine if the widget should be displayed.
     *
     * @return bool
     */
    public function shouldBeDisplayed()
    {
        return Auth::user()->can('browse', Voyager::model('User'));
    }
}
