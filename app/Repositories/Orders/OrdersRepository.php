<?php

namespace App\Repositories\Orders;

use App\Models\Order;
use App\Repositories\Orders\OrdersRepositoryInterface;
use App\Repositories\BaseRepository;


class OrdersRepository extends BaseRepository implements OrdersRepositoryInterface
{
	protected $model;

	/**
	 * Constructor
	 * @param Orders
	 */
	public function __construct(Order $orders)
	{
		$this->model = $orders;
	}
}