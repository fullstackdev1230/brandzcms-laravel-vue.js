<?php

namespace App\Repositories\Notifications;

use App\Models\Notification;
use App\Repositories\Notifications\NotificationsRepositoryInterface;
use App\Repositories\BaseRepository;


class NotificationsRepository extends BaseRepository implements NotificationsRepositoryInterface
{
	protected $model;

	/**
	 * Constructor
	 * @param Notifications
	 */
	public function __construct(Notification $notification)
	{
		$this->model = $notification;
	}
}