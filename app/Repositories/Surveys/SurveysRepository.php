<?php

namespace App\Repositories\Surveys;

use App\Models\Survey;
use App\Repositories\Surveys\SurveysRepositoryInterface;
use App\Repositories\BaseRepository;


class SurveysRepository extends BaseRepository implements SurveysRepositoryInterface
{
	protected $model;

	/**
	 * Constructor
	 * @param Brochures
	 */
	public function __construct(Survey $surveys)
	{
		$this->model = $surveys;
	}

}