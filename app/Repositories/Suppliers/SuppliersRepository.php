<?php

namespace App\Repositories\Suppliers;

use App\Models\Supplier;
use App\Repositories\Suppliers\SuppliersRepositoryInterface;
use App\Repositories\BaseRepository;


class SuppliersRepository extends BaseRepository implements SuppliersRepositoryInterface
{
	protected $model;

	/**
	 * Constructor
	 * @param Suppliers
	 */
	public function __construct(Supplier $suppliers)
	{
		$this->model = $suppliers;
	}
}