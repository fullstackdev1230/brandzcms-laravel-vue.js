<?php

namespace App\Repositories\Layouts;

use App\Models\Layout;
use App\Repositories\Layouts\LayoutsRepositoryInterface;
use App\Repositories\BaseRepository;


class LayoutsRepository extends BaseRepository implements LayoutsRepositoryInterface
{
	protected $model;

	/**
	 * Constructor
	 * @param Layouts
	 */
	public function __construct(Layout $layout)
	{
		$this->model = $layout;
	}
}