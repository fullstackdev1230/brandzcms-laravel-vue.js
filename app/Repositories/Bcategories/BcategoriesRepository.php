<?php

namespace App\Repositories\Bcategories;

use App\Models\Bcategory;
use App\Repositories\Bcategories\BcategoriesRepositoryInterface;
use App\Repositories\BaseRepository;


class BcategoriesRepository extends BaseRepository implements BcategoriesRepositoryInterface
{
	protected $model;

	/**
	 * Constructor
	 * @param Bcategories
	 */
	public function __construct(Bcategory $bcategory)
	{
		$this->model = $bcategory;
	}
}