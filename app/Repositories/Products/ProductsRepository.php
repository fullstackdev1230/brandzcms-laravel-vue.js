<?php

namespace App\Repositories\Products;

use App\Models\Product;
use App\Repositories\Products\ProductsRepositoryInterface;
use App\Repositories\BaseRepository;


class ProductsRepository extends BaseRepository implements ProductsRepositoryInterface
{
	protected $model;

	/**
	 * Constructor
	 * @param Products
	 */
	public function __construct(Product $products)
	{
		$this->model = $products;
	}
}