<?php

namespace App\Repositories\Users;

use App\Models\User;
use App\Repositories\Users\UsersRepositoryInterface;
use App\Repositories\BaseRepository;


class UsersRepository extends BaseRepository implements UsersRepositoryInterface
{
	protected $model;

	/**
	 * Constructor
	 * @param Users
	 */
	public function __construct(User $users)
	{
		$this->model = $users;
	}
}