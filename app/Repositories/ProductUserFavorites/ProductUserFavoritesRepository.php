<?php

namespace App\Repositories\ProductUserFavorites;

use App\Models\ProductUserFavorite;
use App\Repositories\ProductUserFavorites\ProductUserFavoritesRepositoryInterface;
use App\Repositories\BaseRepository;


class ProductUserFavoritesRepository extends BaseRepository implements ProductUserFavoritesRepositoryInterface
{
	protected $model;

	/**
	 * Constructor
	 * @param ProductUserFavorites
	 */
	public function __construct(ProductUserFavorite $productUserFavorite)
	{
		$this->model = $productUserFavorite;
	}
}