<?php

namespace App\Repositories\Pcategories;

use App\Models\Pcategory;
use App\Repositories\Pcategories\PcategoriesRepositoryInterface;
use App\Repositories\BaseRepository;


class PcategoriesRepository extends BaseRepository implements PcategoriesRepositoryInterface
{
	protected $model;

	/**
	 * Constructor
	 * @param Pcategories
	 */
	public function __construct(Pcategory $pcategory)
	{
		$this->model = $pcategory;
	}
}