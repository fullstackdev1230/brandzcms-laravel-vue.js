<?php

namespace App\Repositories\Categories;

use App\Models\Category;
use App\Repositories\Categories\CategoriesRepositoryInterface;
use App\Repositories\BaseRepository;


class CategoriesRepository extends BaseRepository implements CategoriesRepositoryInterface
{
	protected $model;

	/**
	 * Constructor
	 * @param Categories
	 */
	public function __construct(Category $category)
	{
		$this->model = $category;
	}
}