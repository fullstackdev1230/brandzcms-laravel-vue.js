<?php

namespace App\Repositories\Groups;

use App\Models\Group;
use App\Repositories\Groups\GroupsRepositoryInterface;
use App\Repositories\BaseRepository;


class GroupsRepository extends BaseRepository implements GroupsRepositoryInterface
{
	protected $model;

	/**
	 * Constructor
	 * @param Groups
	 */
	public function __construct(Group $group)
	{
		$this->model = $group;
	}
}