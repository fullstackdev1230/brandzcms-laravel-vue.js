<?php

namespace App\Repositories\Brochures;

use App\Models\Brochure;
use App\Repositories\Brochures\BrochuresRepositoryInterface;
use App\Repositories\BaseRepository;


class BrochuresRepository extends BaseRepository implements BrochuresRepositoryInterface
{
	protected $model;

	/**
	 * Constructor
	 * @param Brochures
	 */
	public function __construct(Brochure $brochures)
	{
		$this->model = $brochures;
	}
}