<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $objOrder;
    
    public function __construct($objOrder)
    {
        $this->objOrder = $objOrder;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
                return $this->view('mails.order')
                    ->text('mails.order_plain');               
    }
}
