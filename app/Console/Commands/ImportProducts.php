<?php

namespace App\Console\Commands;

use App\Models\Pcategory;
use App\Models\Product;
use App\Models\Supplier;
use Illuminate\Console\Command;
use Rap2hpoutre\FastExcel\FastExcel;

use Illuminate\Support\Facades\URL;

class ImportProducts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'products:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Imports product from csv or excel file';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $products = (new FastExcel)->import(storage_path() . '/imports/products.xlsx');

        $this->info('Starting import...');
        $bar = $this->output->createProgressBar(count($products));

        foreach ($products as $key => $product) {

            //We will first create the product supplier if not yet in our DB
            $product_supplier = $product['supplier'];

            $supplier = Supplier::updateOrCreate(['name' => $product_supplier],
                ['name'          => $product_supplier,
                    'contact_person' => "",
                    'email'          => "",
                    'phone'          => "",
                ]);

            //We will then create the categories where the product will belong
            if(!empty($product['categories'])){
                $product_categories = explode('|', $product['categories']);
                $prod_categ_array = []; //We need this to attach the categories to the product

                foreach ($product_categories as $pcateg_key => $pcategory) {

                    //We check if category has children
                    if (strpos($pcategory, '->') !== false) {

                        //Category has children and we will work on each of them
                        $pcategory_children = explode('->', $pcategory);

                        foreach ($pcategory_children as $pc_key => $pcategory_child) {

                            //Supplier->Mondelez->Mondelez Seasonal->Christmas

                            $pnew_category = $pcategory_child;

                            //We get parent of the child
                            $parent_id = null;
                            $parent_name = "";
                            if ($pc_key !== 0) {
                                $parent_name = $pcategory_children[$pc_key - 1];
                                $parent_slug = str_slug($parent_name, '-');
                                $parent_category = Pcategory::firstOrCreate(['name' => $parent_name],
                                    ['slug' => $parent_slug]);
                                $parent_id = $parent_category->id;
                                $parent_name = $parent_category->name;
                            }
                            $slug_str = $parent_name . ' ' . $pnew_category;
                            $pnew_category_slug = str_slug($slug_str, '-');
                            $pCategory = Pcategory::updateOrCreate(['name' => $pnew_category, 'parent_id' => $parent_id],
                                ['name'     => $pnew_category,
                                    'parent_id' => $parent_id,
                                    'slug'      => $pnew_category_slug,
                                ]);
                            //To avoid duplicate id
                            if (!in_array($pCategory->id, $prod_categ_array)) {
                                array_push($prod_categ_array, $pCategory->id);
                            }

                        }

                    } else {
                        //It is a main category and has no parent
                        $pnew_category = $pcategory;
                        $pnew_category_slug = str_slug($pnew_category, '-');

                        $pCategory = Pcategory::updateOrCreate(['name' => $pnew_category, 'parent_id' => null],
                            ['name' => $pnew_category,
                                'slug'  => $pnew_category_slug]);

                        //To avoid duplicate id
                        if (!in_array($pCategory->id, $prod_categ_array)) {
                            array_push($prod_categ_array, $pCategory->id);
                        }

                    }
                }
            }


            //We will now start inserting the product details
            $bc_image_path = "";
            $prod_image_path = "";
            $product_name = $product['name'];
            $product_desc = str_replace('åÊ', '', $product['description']); //We clean the description
            $supplier_id = Supplier::where('name', $product['supplier'])->first()->id; //supplier id based from the supplier name supplied
            $items_per_outer = intval(preg_replace("/[^0-9]/", "", $product['items_per_outer'])); //We clean the items_per_outer

            $barcode_unit = $product['barcode_unit'] ? $product['barcode_unit'] : "";
            $barcode_shipper = $product['barcode_shipper'] ? $product['barcode_shipper'] : "";
            $barcode_outer = $product['barcode_outer'] ? $product['barcode_outer'] : "";

            $barcode_default = $product['barcode_default'];
            $rrp = empty($product['rrp'])?0:(float)$product['rrp'];
            $height = intval($product['height']);
            $width = intval($product['width']);
            $depth = intval($product['depth']);
            $dimension_unit = $product['dimension_unit'];
            $weight = intval($product['weight']);
            $weight_unit = $product['weight_unit'];
            $visible_from = $product['visible_from'] ? $product['visible_from'] : null;
            $visible_to = $product['visible_to'] ? $product['visible_to'] : null;
            $images_name = $product['image_name'] ? $product['image_name'] : null;
            $images_bc = $product['image_bc'] ? $product['image_bc'] : null;
            $product_is_merchandising = intval($product['product_is_merchandising']);
            $product_is_featured = intval($product['product_is_featured']);
            $product_is_new = intval($product['product_is_new']);
            $add_to_widget = intval($product['add_to_widget']);
            $add_to_app = intval($product['add_to_app']);
            $add_to_website = intval($product['add_to_website']);

            $import_product = Product::updateOrCreate(['name' => $product_name, 'supplier_id' => $supplier_id],
                ['supplier_id'             => $supplier_id,
                    'name'                     => $product_name,
                    'description'              => $product_desc,
                    'barcode_unit'             => $barcode_unit,
                    'barcode_outer'            => $barcode_outer,
                    'barcode_shipper'          => $barcode_shipper,
                    'barcode_default'          => $barcode_default,
                    'rrp'                      => $rrp,
                    'items_per_outer'          => $items_per_outer,
                    'height'                   => $height,
                    'width'                    => $width,
                    'depth'                    => $depth,
                    'dimension_unit'           => $dimension_unit,
                    'weight'                   => $weight,
                    'weight_unit'              => $weight_unit,
                    'visible_from'             => $visible_from,
                    'visible_to'               => $visible_to,
                    'product_is_merchandising' => $product_is_merchandising,
                    'product_is_featured'      => $product_is_featured,
                    'product_is_new'           => $product_is_new,
                    'add_to_widget'            => $add_to_widget,
                    'add_to_app'               => $add_to_app,
                    'add_to_website'           => $add_to_website]);

            //We will create the categories and product relationship
            if(!empty($product['categories'])){
                $import_product->categories()->detach(); //we first detach first attached if there is any
                $import_product->categories()->attach($prod_categ_array);
            }

            //We will save the product image in the media library and attach it to the product
            if (is_null($import_product->images) && !empty($images_name)) {

                if(strpos($product_supplier,"'")){
                    $prod_supplier_path = str_replace("'", "_", $product_supplier);
                }else{
                    $prod_supplier_path = $product_supplier;
                }

                $prod_image_path = storage_path() . '/imports/images/' . $prod_supplier_path . '/' . $images_name;

                //we check if the file we will attach exists
                if (file_exists($prod_image_path)) {


                    $prod_update_img = Product::find($import_product->id);
                    $prod_update_img->copyMedia($prod_image_path)->toMediaCollection('images');
                    $media_file = $prod_update_img->getMedia('images');
                    $image_array = [];
                    $image_array[] = $media_file[0]->getFullUrl('thumb');
                    $json_encoded_image = json_encode($image_array);

                    $prod_update_img->images = $json_encoded_image;
                    $prod_update_img->save();

                }
            }


            //We will save the barcode image and attach it to the product
            if (is_null($import_product->barcode_image) && !empty($images_bc)) {

                $bc_image_path = storage_path() . '/imports/barcode_images/' . $images_bc;

                //we check if the file we will attach exists
                if (file_exists($bc_image_path)) {

                    $prod_update_bc = Product::find($import_product->id);
                    $prod_update_bc->copyMedia($bc_image_path)->toMediaCollection('barcode_image');
                    $media_file_bc = $prod_update_bc->getMedia('barcode_image');
                    $prod_update_bc->barcode_image = $media_file_bc[0]->getFullUrl('thumb');
                    $prod_update_bc->save();

                }
            }

            $bar->advance();
        }

        $bar->finish();
        $this->info("\n\nImport finished!");
    }
}
