<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use App\Models\Group;


class RenewSubscriptions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user-group:renew';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Renew Group Subscriptions';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $groupsExpiring = Group::expired()->get();

        if($groupsExpiring->isNotEmpty()){

            foreach ($groupsExpiring as $group) {
                if($group->auto_renew == 1){
                    $expiredGroup = Group::find($group->id);

                    $expiredGroup->subscription_start = Carbon::now();
                    $expiredGroup->subscription_expiry = Carbon::now()->addMonths($group->term);
                    $expiredGroup->save();
                }
            }
        }
    }
}
