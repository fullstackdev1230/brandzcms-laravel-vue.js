<?php

namespace App\Console\Commands;


use App\Models\Product;
use App\Models\User;
use App\Models\Survey;
use App\Models\Notification;
use Carbon\Carbon;

use Illuminate\Console\Command;

class UpdateNotifications extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notifications:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create new notifications';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $current = Carbon::now();

        $subscription_warning = 7; //days
        $warning_date = $current->addDays($subscription_warning);

        //Check if there are users (role_id 2) about to expire within x days
        $usersExpiring = User::where('role_id',2)->where('subscription_expiry','<',$warning_date)->get();

        if($usersExpiring->isNotEmpty()){

            //We will create a notification for each user
            foreach ($usersExpiring as $key => $user) {
                //$expiry_date = Carbon::parse($user->subscription_expiry)->format('d/m/Y');
                //We check if this notification for user has already been created before
                $notification = Notification::firstOrCreate(['notification_title' => "User Expiring:".$user->id], 
                                                            ['notification_content' => $user->name."'s subscription is about to expire",
                                                             'visible_to_app' => 1, 'visible_to_app' => 1,
                                                             'publish_date' => Carbon::now(), 'expiry_date' => Carbon::now()->addDays(7),
                                                             'link' => route("voyager.users.show",['id' => $user->id])]);

            }

        }

        //Check if there are surveys about to expire within x days
        $surveysExpiring = Survey::where('expiry_date','<',$warning_date)->get();

        if($surveysExpiring->isNotEmpty()){

            //We will create a notification for each survey
            foreach ($surveysExpiring as $key => $survey) {
                //$expiry_date = Carbon::parse($user->subscription_expiry)->format('d/m/Y');
                //We check if this notification for survey has already been created before
                $notification = Notification::firstOrCreate(['notification_title' => "Survey Expiring:".$survey->id], 
                                                            ['notification_content' => $survey->survey_title." survey is about to expire",
                                                             'visible_to_app' => 1, 'visible_to_app' => 1,
                                                             'publish_date' => Carbon::now(), 'expiry_date' => Carbon::now()->addDays(7),
                                                             'link' => route("voyager.surveys.show",['id' => $survey->id])]);
            }

        }


    }
}
