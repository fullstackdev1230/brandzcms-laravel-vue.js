<?php

namespace App\Providers;

use App\Repositories\Brochures\BrochuresRepository;
use App\Repositories\Brochures\BrochuresRepositoryInterface;
use App\Repositories\Categories\CategoriesRepository;
use App\Repositories\Categories\CategoriesRepositoryInterface;
use App\Repositories\Groups\GroupsRepository;
use App\Repositories\Groups\GroupsRepositoryInterface;
use App\Repositories\Layouts\LayoutsRepository;
use App\Repositories\Layouts\LayoutsRepositoryInterface;
use App\Repositories\Notifications\NotificationsRepository;
use App\Repositories\Notifications\NotificationsRepositoryInterface;
use App\Repositories\Orders\OrdersRepository;
use App\Repositories\Orders\OrdersRepositoryInterface;
use App\Repositories\Pcategories\PcategoriesRepository;
use App\Repositories\Pcategories\PcategoriesRepositoryInterface;
use App\Repositories\Products\ProductsRepository;
use App\Repositories\Products\ProductsRepositoryInterface;
use App\Repositories\ProductUserFavorites\ProductUserFavoritesRepository;
use App\Repositories\ProductUserFavorites\ProductUserFavoritesRepositoryInterface;
use App\Repositories\Suppliers\SuppliersRepository;
use App\Repositories\Suppliers\SuppliersRepositoryInterface;
use App\Repositories\Surveys\SurveysRepository;
use App\Repositories\Surveys\SurveysRepositoryInterface;
use App\Repositories\Users\UsersRepository;
use App\Repositories\Users\UsersRepositoryInterface;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind( BcategoriesRepositoryInterface::class,BcategoriesRepository::class);
        $this->app->bind( BrochuresRepositoryInterface::class,BrochuresRepository::class);
        $this->app->bind( CategoriesRepositoryInterface::class,CategoriesRepository::class);
        $this->app->bind( GroupsRepositoryInterface::class,GroupsRepository::class);
        $this->app->bind( LayoutsRepositoryInterface::class,LayoutsRepository::class);
        $this->app->bind( NotificationsRepositoryInterface::class,NotificationsRepository::class);
        $this->app->bind( OrdersRepositoryInterface::class,OrdersRepository::class);
        $this->app->bind( PcategoriesRepositoryInterface::class,PcategoriesRepository::class);
        $this->app->bind( ProductsRepositoryInterface::class,ProductsRepository::class);
        $this->app->bind( ProductUserFavoritesRepositoryInterface::class,ProductUserFavoritesRepository::class);
        $this->app->bind( SuppliersRepositoryInterface::class,SuppliersRepository::class);
        $this->app->bind( SurveysRepositoryInterface::class,SurveysRepository::class);
        $this->app->bind( UsersRepositoryInterface::class,UsersRepository::class);
    }
}
