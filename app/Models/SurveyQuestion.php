<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\SurveyQuestionAnswer;
use App\Models\SurveySubmissionAnswer;
use App\Models\Survey;


class SurveyQuestion extends Model
{

	protected $table = 'survey_questions';

    protected $fillable = ['question_type','question_text','question_order','survey_id'];

	/**
	* Survey
	* @return Object
	*/
    public function survey()
    {
    	return $this->belongsTo(Survey::class,'id','survey_id')->withTimestamps();
    }

    public function answers()
    {
    	return $this->hasMany(SurveyQuestionAnswer::class,'survey_question_id','id');
    }

    public function submitted_answers()
    {
        return $this->hasMany(SurveySubmissionAnswer::class,'survey_question_id','id');
    }
}
