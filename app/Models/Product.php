<?php

namespace App\Models;

use App\Models\Pcategory;
use App\Models\ProductUserFavorite;
use App\Models\Supplier;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;
use Carbon\Carbon;

class Product extends Model implements HasMedia
{
    use HasMediaTrait;

    static $tnh = 368;
    static $tnw = 232;

    protected $perPage = 10;

    private $thumbNailWidth;
    private $thumbNailHeight;
    private $quantity;

    protected $fillable = [
        'supplier_id',
        'name',
        'description',
        'barcode_image',
        'barcode_unit',
        'barcode_outer',
        'barcode_shipper',
        'barcode_default',
        'rrp',
        'items_per_outer',
        'height',
        'width',
        'depth',
        'dimension_unit',
        'weight',
        'weight_unit',
        'visible_from',
        'visible_to',
        'images',
        'product_is_merchandising',
        'product_is_featured',
        'product_is_new',
        'add_to_widget',
        'add_to_app',
        'add_to_website'];

    // Custom Attributes
    protected $appends = ['full_image','full_image_bc'];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->setThumbNailWidth(self::$tnh);
        $this->setThumbNailHeight(self::$tnw);
    }

    public function supplier()
    {
        return $this->belongsTo(Supplier::class);
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')
            ->width($this->getThumbNailWidth())
            ->height($this->getThumbNailHeight())
            ->sharpen(10);
    }

    private function setThumbNailWidth($thumbNailWidth)
    {
        $this->thumbNailWidth = $thumbNailWidth;
    }

    private function getThumbNailWidth()
    {
        return $this->thumbNailWidth;
    }

    private function setThumbNailHeight($thumbNailHeight)
    {
        $this->thumbNailHeight = $thumbNailHeight;
    }

    private function getThumbNailHeight()
    {
        return $this->thumbNailHeight;
    }

    public function categories()
    {
        return $this->belongsToMany(Pcategory::class, 'product_category', 'product_id', 'pcategory_id');
    }

    public function activeCategories()
    {
        return $this->belongsToMany(Pcategory::class, 'product_category', 'product_id', 'pcategory_id')->where('active', '=', 1);
    }

    public function productuserfavorite()
    {
        return $this->belongsToMany(ProductUserFavorite::class, 'product_user_favorites', 'product_id', 'id');
    }

    /**
     * Get Full Image
     */
    public function getFullImageAttribute()
    {
        $image = $this->getMedia('images');

        if (!$image->isEmpty()) {

            return $image[0]->getFullUrl();
        }

    }

    /**
     * Get BC Full Image
     */
    public function getFullImageBcAttribute()
    {

        $barcode_image = $this->getMedia('barcode_image');

        if (!$barcode_image->isEmpty()) {

            return $barcode_image[0]->getFullUrl();
        }
    }

    /**
     * Product default ordering by name
     */
    public function scopeOrdered($query)
    {
        return $query->orderBy('name', 'asc');
    }

    /**
     * Products that have not expired yet
     */
    public function scopeNotExpired($query)
    {

        return $query->whereDate('visible_to', '>', Carbon::now())
                    ->orWhereNull('visible_to');
    }

    //We format date format returned
    public function getCreatedAtAttribute($value) {
        if($value!=null){
            return Carbon::parse($value)->format('d-m-Y');
        }
    }

    public function getUpdatedAtAttribute($value) {
        if($value!=null){
            return Carbon::parse($value)->format('d-m-Y');
        }
    }

    public function getVisibleFromAttribute($value) {
        if($value!=null){
            return Carbon::parse($value)->format('d-m-Y');
        }
    }

    public function getVisibleToAttribute($value) {
        if($value!=null){
            return Carbon::parse($value)->format('d-m-Y');
        }
    }

}
