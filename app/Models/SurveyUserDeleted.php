<?php

namespace App\Models;

use App\Models\Survey;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class SurveyUserDeleted extends Model
{
	protected $table = 'survey_userdeleted';

    protected $fillable = [
		'survey_id',
		'user_id',
	];

	public function surveys()
	{
		return $this->belongsToMany(Survey::class);
	}

	public function users()
	{
		return $this->belongsToMany(User::class);
	}

}
