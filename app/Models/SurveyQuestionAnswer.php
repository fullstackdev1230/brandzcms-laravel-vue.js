<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\SurveyQuestion;
use App\Models\SurveySubmissionAnswer;

class SurveyQuestionAnswer extends Model
{
    //
    protected $table = 'survey_answers';

    protected $fillable = ['answer_text', 'survey_question_id'];

    public function question()
    {
    	return $this->belongsTo(SurveyQuestion::class,'id','survey_question_id')->withTimestamps();
    }

    public function chosen()
    {
    	return $this->hasMany(SurveySubmissionAnswer::class,'survey_answer_id','id');
    }
}
