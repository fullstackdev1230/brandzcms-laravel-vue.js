<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Product;
use App\Models\OrderProduct;
use App\Models\User;
use Carbon\Carbon;

class Order extends Model
{
    protected $table = 'orders';

    protected $fillable = ['user_id', 'subject','to_email','notes'];

	/**
	 * User
	 * @return Object
	 */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Products
     * @return Object
     */
    public function products()
    {
        return $this->belongsToMany(Product::class, 'order_products', 'order_id');
    }

    /**
     * Order_products
     * @return Object
     */
    public function orderProducts()
    {
        return $this->hasMany(OrderProduct::class);
    }

    //We format date format returned
    public function getCreatedAtAttribute($value) {
        if($value!=null){
            return Carbon::parse($value)->format('d-m-Y: H:i:s');
        }
    }

    public function getUpdatedAtAttribute($value) {
        if($value!=null){
            return Carbon::parse($value)->format('d-m-Y: H:i:s');
        }
    }
}
