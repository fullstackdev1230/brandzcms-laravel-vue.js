<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;
use Carbon\Carbon;

class LayoutImageLink extends Model implements HasMedia
{
    //
    use HasMediaTrait;

    static $tnh = 368;
    static $tnw = 232;

    private $thumbNailWidth;
    private $thumbNailHeight;

    protected $fillable = [
        'layout_id',
        'td_id',
        'image',
        'link',
    ];

    // Custom attributes
    protected $appends = ['full_image'];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->setThumbNailWidth(self::$tnh);
        $this->setThumbNailHeight(self::$tnw);
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')
            ->width($this->getThumbNailWidth())
            ->height($this->getThumbNailHeight())
            ->sharpen(10);
    }

    private function setThumbNailWidth($thumbNailWidth)
    {
        $this->thumbNailWidth = $thumbNailWidth;
    }

    private function getThumbNailWidth()
    {
        return $this->thumbNailWidth;
    }

    private function setThumbNailHeight($thumbNailHeight)
    {
        $this->thumbNailHeight = $thumbNailHeight;
    }

    private function getThumbNailHeight()
    {
        return $this->thumbNailHeight;
    }

    public function getFullImageAttribute()
    {
        $image = $this->getMedia('image');

        if (!$image->isEmpty()) {

            return $image[0]->getFullUrl();
        }
    }

    //We format date format returned
    public function getCreatedAtAttribute($value) {
        if($value!=null){
            return Carbon::parse($value)->format('d-m-Y');
        }
    }

    public function getUpdatedAtAttribute($value) {
        if($value!=null){
            return Carbon::parse($value)->format('d-m-Y');
        }
    }
}
