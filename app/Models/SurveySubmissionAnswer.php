<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Survey;
use App\Models\SurveySubmission;

class SurveySubmissionAnswer extends Model
{
    protected $table = 'survey_submission_answers';

    protected $fillable = ['survey_submission_id', 'survey_question_id','survey_answer_id','answer_text'];

    /**
     * Relationship to Survey Submission
     * @return [type] [description]
     */
    public function surveySubmission()
    {
    	return $this->belongsTo(SurveySubmission::class,'id','survey_submission_id');
    }
}
