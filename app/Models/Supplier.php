<?php

namespace App\Models;

use App\Models\Product;
use Illuminate\Database\Eloquent\Model;
use App\Models\Group;
use App\Models\Pcategory;
use Spatie\MediaLibrary\Models\Media;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Carbon\Carbon;

class Supplier extends Model implements HasMedia
{
    use HasMediaTrait;

    protected $table = 'suppliers';

    protected $fillable = ['name', 'contact_person','email','phone'];


    static $tnh = 368;
    static $tnw = 232;
    private $thumbNailWidth;
    private $thumbNailHeight;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->setThumbNailWidth(self::$tnh);
        $this->setThumbNailHeight(self::$tnw);
    }

    public function registerMediaConversions(Media $media = null)
    {
       $this->addMediaConversion('thumb')
          ->width($this->getThumbNailWidth())
          ->height($this->getThumbNailHeight())
          ->sharpen(10);
    }

    private function setThumbNailWidth($thumbNailWidth) {
        $this->thumbNailWidth = $thumbNailWidth;
    }

    private function getThumbNailWidth() {
        return $this->thumbNailWidth;
    }

    private function setThumbNailHeight($thumbNailHeight) {
        $this->thumbNailHeight = $thumbNailHeight;
    }

    private function getThumbNailHeight() {
        return $this->thumbNailHeight;
    }

	/**
	 * Products
	 * @return Object
	 */
    public function products()
    {
    	return $this->hasMany(Product::class);
    }

    /**
	 * Group
	 * @return Object
	 */
    public function groups()
    {
    	return $this->belongsToMany(Group::class,'group_supplier')->withTimestamps();
    }

    /**
     * Ordering by name
     */
    public function scopeOrdered($query)
    {
        return $query->orderBy('name', 'asc');
    }

    //We format date format returned
    public function getCreatedAtAttribute($value) {
        if($value!=null){
            return Carbon::parse($value)->format('d-m-Y');
        }
    }

    public function getUpdatedAtAttribute($value) {
        if($value!=null){
            return Carbon::parse($value)->format('d-m-Y');
        }
    }
}
