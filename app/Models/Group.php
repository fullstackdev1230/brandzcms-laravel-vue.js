<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Supplier;
use App\Models\Survey;
use App\Models\Category;
use App\Models\Brochure;
use App\Models\Layout;
use Carbon\Carbon;

class Group extends Model
{
	/**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'state' => 'array',
    ];

	/**
	 * Supplier
	 * @return Object
	 */
    public function suppliers()
    {
    	return $this->belongsToMany(Supplier::class,'group_supplier')->orderBy('name')->withTimestamps();
    }

	/**
	 * Category
	 * @return Object
	 */
    public function categories()
    {
    	return $this->belongsToMany(Category::class,'group_category')->orderBy('name')->withTimestamps();
    }

    /**
     * Brochure
     * @return Object
     */
    public function brochures()
	{
		return $this->belongsToMany(Brochure::class,'brochure_group')->withTimestamps();
	}

    /**
     * Layout
     * @return Object
     */
    public function layouts()
    {
        return $this->belongsToMany(Layout::class,'layout_group')->withTimestamps();
    }

    /**
     * Survey
     * @return Object
     */
    public function surveys()
    {
        return $this->belongsToMany(Survey::class,'group_survey')->withTimestamps();
    }

    /**
     * Category
     * @return Object
     */
    public function parentcategories()
    {
        return $this->belongsToMany(Category::class,'group_category')->where('parent_id', '=', NULL)->distinct('id');
    }

    //We format date format returned
    public function getCreatedAtAttribute($value) {
        if($value!=null){
            return Carbon::parse($value)->format('d-m-Y');
        }
    }

    public function getUpdatedAtAttribute($value) {
        if($value!=null){
            return Carbon::parse($value)->format('d-m-Y');
        }
    }

    public function scopeExpired($query)
    {
        return $query->whereDate('subscription_expiry', '<=', Carbon::now());
    }

}
