<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Group;
use App\Models\SurveyQuestion;
use App\Models\SurveySubmission;
use App\Models\SurveyUserDeleted;
use App\Models\SurveyUserViewed;
use Carbon\Carbon;

class Survey extends Model
{

    protected $table = 'surveys';

    protected $fillable = ['survey_title', 'publish_date','expiry_date','survey_description'];

	/**
	* Group
	* @return Object
	*/
    public function groups()
    {
    	return $this->belongsToMany(Group::class,'group_survey')->withTimestamps();
    }

    /**
    * Survey Submissions
    * @return Object
    */
    public function submissions()
    {
        return $this->hasMany(SurveySubmission::class,'survey_id','id');
    }

    /**
    * Questions
    * @return Object
    */
    public function questions()
    {
    	return $this->hasMany(SurveyQuestion::class,'survey_id','id');
    }

    //We format date format returned
    public function getPublishDateAttribute($value) {
        if($value!=null){
            return Carbon::parse($value)->format('d-m-Y');
        }
    }

    public function getExpiryDateAttribute($value) {
        if($value!=null){
            return Carbon::parse($value)->format('d-m-Y');
        }
    }

    public function getCreatedAtAttribute($value) {
        if($value!=null){
            return Carbon::parse($value)->format('d-m-Y');
        }
    }

    public function getUpdatedAtAttribute($value) {
        if($value!=null){
            return Carbon::parse($value)->format('d-m-Y');
        }
    }

    public function surveyUserViewed(){
        return $this->hasMany(SurveyUserViewed::class,'survey_id');
    }

    public function surveyUserDeleted(){
        return $this->hasMany(SurveyUserDeleted::class,'survey_id');
    }

}
