<?php

namespace App\Models;
use Laravel\Passport\HasApiTokens;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;
use App\Models\Group;
use App\Models\ProductUserFavorite;
use Carbon\Carbon;

class User extends \TCG\Voyager\Models\User implements HasMedia
{
    use Notifiable;
    use HasMediaTrait;
    use HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'group_id',
        'role_id',
        'access_to_app',
        'access_to_widget',
        'state',
        'subscription_start',
        'subscription_expiry',
        'accent_colour',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    static $tnh = 368;
    static $tnw = 232;

    private $thumbNailWidth;
    private $thumbNailHeight;
    private $favorites;
    private $layout;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->setThumbNailWidth(self::$tnh);
        $this->setThumbNailHeight(self::$tnw);
    }

    public function registerMediaConversions(Media $media = null)
    {
       $this->addMediaConversion('thumb')
          ->width($this->getThumbNailWidth())
          ->height($this->getThumbNailHeight())
          ->sharpen(10);
    }

    private function setThumbNailWidth($thumbNailWidth) {
        $this->thumbNailWidth = $thumbNailWidth;
    }

    private function getThumbNailWidth() {
        return $this->thumbNailWidth;
    }

    private function setThumbNailHeight($thumbNailHeight) {
        $this->thumbNailHeight = $thumbNailHeight;
    }

    private function getThumbNailHeight() {
        return $this->thumbNailHeight;
    }

    public function group()
    {
        return $this->belongsTo(Group::class);
    }

    public function favoriteProducts(){
        return $this->hasMany(ProductUserFavorite::class);
    }

    /**
    * Survey Submissions
    * @return Object
    */
    public function survey_submissions()
    {
        return $this->hasMany(SurveySubmission::class,'user_id','id');
    }

    //We format date format returned
    public function getCreatedAtAttribute($value) {
        if($value!=null){
            return Carbon::parse($value)->format('d-m-Y');
        }
    }

    public function getUpdatedAtAttribute($value) {
        if($value!=null){
            return Carbon::parse($value)->format('d-m-Y');
        }
    }

    public function getSubscriptionStartAttribute($value) {
        if($value!=null){
            return Carbon::parse($value)->format('d-m-Y');
        }
    }

    public function getSubscriptionExpiryAttribute($value) {
        if($value!=null){
            return Carbon::parse($value)->format('d-m-Y');
        }
        
    }

    public function scopeNotExpired($query)
    {
        return $query->whereDate('subscription_expiry', '>', Carbon::now())
                    ->orWhereNull('subscription_expiry');
    }

}
