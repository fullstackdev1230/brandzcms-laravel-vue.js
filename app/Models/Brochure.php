<?php

namespace App\Models;

use App\Models\Bcategory;
use App\Models\Group;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\Models\Media;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Carbon\Carbon;

class Brochure extends Model implements HasMedia
{
	use HasMediaTrait;

	static $tnh = 368;
	static $tnw = 232;

    private $thumbNailWidth;
    private $thumbNailHeight;

    protected $fillable = [
		'supplier_id',
		'display_order',
        'display_image',
        'brochure_files',
        'height',
        'width',
        'name',
        'description',
        'add_to_website',
        'add_to_app',
        'visible_to',
        'visible_from',
	];

	public function __construct(array $attributes = [])
	{
		parent::__construct($attributes);

		$this->setThumbNailWidth(self::$tnh);
		$this->setThumbNailHeight(self::$tnw);
	}

	public function registerMediaConversions(Media $media = null)
    {
       $this->addMediaConversion('thumb')
          ->width($this->getThumbNailWidth())
          ->height($this->getThumbNailHeight())
          ->sharpen(10);
    }

    private function setThumbNailWidth($thumbNailWidth) {
        $this->thumbNailWidth = $thumbNailWidth;
    }

    private function getThumbNailWidth() {
        return $this->thumbNailWidth;
    }

    private function setThumbNailHeight($thumbNailHeight) {
        $this->thumbNailHeight = $thumbNailHeight;
    }

    private function getThumbNailHeight() {
        return $this->thumbNailHeight;
    }

	public function categories()
	{
		return $this->belongsToMany(Bcategory::class,'brochure_category')->withTimestamps();
	}

	public function groups()
	{
		return $this->belongsToMany(Group::class,'brochure_group')->withTimestamps();
	}

	/**
     * Scope find by Category Id.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param mixed $type
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeByCategoryId($query, $categoryId)
    {
        return $query->whereHas('categories', function ($query) use ($categoryId) {
        	$query->where('bcategory_id', $categoryId); });
    }


    /**
     * Scope find by Category Name.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param mixed $type
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeByCategoryName($query, $categoryName)
    {
        return $query->whereHas('categories', function ($query) use ($categoryName) {
            $query->where('bcategories.name', $categoryName); });
    }

    //We format date format returned
    public function getCreatedAtAttribute($value) {
        if($value!=null){
            return Carbon::parse($value)->format('d-m-Y');
        }
    }

    public function getUpdatedAtAttribute($value) {
        if($value!=null){
            return Carbon::parse($value)->format('d-m-Y');
        }
    }

    public function getVisibleFromAttribute($value) {
        if($value!=null){
            return Carbon::parse($value)->format('d-m-Y');
        }
    }

    public function getVisibleToAttribute($value) {
        if($value!=null){
            return Carbon::parse($value)->format('d-m-Y');
        }
    }
}
