<?php

namespace App\Models;

use App\Models\Group;
use App\Models\LayoutImageLink;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Layout extends Model
{

    protected $fillable = [
		'name',
		'cells',
	];

	public function groups()
	{
		return $this->belongsToMany(Group::class,'layout_group')->withTimestamps();
	}

	public function layoutImageLinks()
	{
		return $this->hasMany(LayoutImageLink::class, 'layout_id');
	}

    //We format date format returned
    public function getCreatedAtAttribute($value) {
        if($value!=null){
            return Carbon::parse($value)->format('d-m-Y');
        }
    }

    public function getUpdatedAtAttribute($value) {
        if($value!=null){
            return Carbon::parse($value)->format('d-m-Y');
        }
    }
}
