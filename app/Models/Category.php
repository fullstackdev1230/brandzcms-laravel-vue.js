<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Group;
use Carbon\Carbon;

class Category extends Model 
{
    protected $table = 'categories';

    /**
	 * Group
	 * @return Object
	 */
    public function groups()
    {
    	return $this->belongsToMany(Group::class,'group_supplier')->withTimestamps();
    }

    /**
     * Ordering by name
     */
    public function scopeOrdered($query)
    {
        return $query->orderBy('name', 'asc')->get();
    }

    //We format date format returned
    public function getCreatedAtAttribute($value) {
        if($value!=null){
            return Carbon::parse($value)->format('d-m-Y');
        }
    }

    public function getUpdatedAtAttribute($value) {
        if($value!=null){
            return Carbon::parse($value)->format('d-m-Y');
        }
    }
}
