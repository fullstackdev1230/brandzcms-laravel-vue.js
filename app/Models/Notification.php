<?php

namespace App\Models;

use App\Models\Group;
use App\Models\NotificationUserDeleted;
use App\Models\NotificationUserViewed;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\Models\Media;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Notification extends Model implements HasMedia
{
	use HasMediaTrait;
    use SoftDeletes;

	static $tnh = 368;
	static $tnw = 232;

    private $thumbNailWidth;
    private $thumbNailHeight;

    protected $fillable = ['notification_title','notification_content',
                            'notification_type','visible_to_app',
                            'visible_to_widget','publish_date','expiry_date','images'];

    protected $dates = ['deleted_at'];

	public function __construct(array $attributes = [])
	{
		parent::__construct($attributes);

		$this->setThumbNailWidth(self::$tnh);
		$this->setThumbNailHeight(self::$tnw);
	}

	public function registerMediaConversions(Media $media = null)
    {
       $this->addMediaConversion('thumb')
          ->width($this->getThumbNailWidth())
          ->height($this->getThumbNailHeight())
          ->sharpen(10);
    }

    private function setThumbNailWidth($thumbNailWidth)
    {
        $this->thumbNailWidth = $thumbNailWidth;
    }

    private function getThumbNailWidth()
    {
        return $this->thumbNailWidth;
    }

    private function setThumbNailHeight($thumbNailHeight)
    {
        $this->thumbNailHeight = $thumbNailHeight;
    }

    private function getThumbNailHeight() {
        return $this->thumbNailHeight;
    }


    //We format date format returned
    public function getCreatedAtAttribute($value) {
        if($value!=null){
            return Carbon::parse($value)->format('d-m-Y');
        }
    }

    public function getPublishDateAttribute($value) {
        if($value!=null){
            return Carbon::parse($value)->format('d-m-Y');
        }
    }

    public function getExpiryDateAttribute($value) {
        if($value!=null){
            return Carbon::parse($value)->format('d-m-Y');
        }
    }

    	/**
	 * Groups
	 * @return Object
	 */
    public function groups()
    {
    	return $this->belongsToMany(Group::class,'notification_group')->withTimestamps();
    }

    public function notificationUserViewed(){
        return $this->hasMany(NotificationUserViewed::class,'notification_id');
    }

    public function notificationUserDeleted(){
        return $this->hasMany(NotificationUserDeleted::class,'notification_id');
    }
    
}
