<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Order;
use App\Models\Product;
use App\Models\User;
use Carbon\Carbon;

class OrderProduct extends Model
{
    protected $table = 'order_products';

    protected $fillable = ['order_id', 'product_id','quantity'];

	/**
	 * User
	 * @return Object
	 */
    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    /**
     * Product
     * @return Object
     */
    public function product()
    {
        return $this->belongsTo(Product::class);
    }


    /**
     * Group
     * @return Object
     */
    public function orders()
    {
        return $this->belongsToMany(Order::class,'order_products')->withTimestamps();
    }

    //We format date format returned
    public function getCreatedAtAttribute($value) {
        if($value!=null){
            return Carbon::parse($value)->format('d-m-Y');
        }
    }

    public function getUpdatedAtAttribute($value) {
        if($value!=null){
            return Carbon::parse($value)->format('d-m-Y');
        }
    }
}
