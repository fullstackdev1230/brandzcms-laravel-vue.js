<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Survey;
use App\Models\SurveySubmissionAnswer;

class SurveySubmission extends Model
{
    protected $table = 'survey_submissions';

    protected $fillable = ['survey_id', 'user_id'];

    /**
     * Relationship to Survey Submission Answers
     * @return [type] [description]
     */
    public function answers()
    {
    	return $this->hasMany(SurveySubmissionAnswer::class,'survey_submission_id','id');
    }

    /**
    * Survey 
    * @return Object
    */
    public function survey()
    {
        return $this->belongsTo(Survey::class,'id','survey_id');
    }
    
}
