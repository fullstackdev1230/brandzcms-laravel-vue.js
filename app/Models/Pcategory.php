<?php

namespace App\Models;

use App\Models\Product;
use App\Models\Pcategory;
use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Traits\HasRelationships;
use TCG\Voyager\Traits\Translatable;
use Carbon\Carbon;

class Pcategory extends Model
{
    use Translatable,
        HasRelationships;

    protected $translatable = ['slug', 'name'];

    protected $table = 'pcategories';

    protected $fillable = ['slug', 'name','parent_id','order','active'];

    public function parentId()
    {
        return $this->belongsTo(self::class,'id','parent_id')->whereNull('parent_id')->with('parent');
    }

    public function children()
    {
        return $this->hasMany(self::class,'parent_id','id')->with('children');
    }

    public function products()
    {
        return $this->belongsToMany(Product::class);
    }

    /**
     * Category
     * @return Object
     */
    public function categories()
    {
        return $this->belongsToMany(Pcategory::class,'product_category')->withTimestamps();
    }

    /**
     * PCategory
     * @return Object
     */
    public function subcategories()
    {
        return $this->belongsToMany(Pcategory::class, 'pcategories');
    }

    /**
     * Ordering by name
     */
    public function scopeOrdered($query)
    {
        return $query->orderBy('name', 'asc');
    }

    /**
     * get Active
     */
    public function scopeActive($query)
    {
        return $query->where('active', '=', 1);
    }

    //We format date format returned
    public function getCreatedAtAttribute($value) {
        if($value!=null){
            return Carbon::parse($value)->format('d-m-Y');
        }
    }

    public function getUpdatedAtAttribute($value) {
        if($value!=null){
            return Carbon::parse($value)->format('d-m-Y');
        }
    }
}
