<?php

function list_categories($categories){

  $data = [];

  foreach($categories as $category){
    $data[] = ['id' => $category->id,
               'name' => $category->name,
               'children' => list_categories($category->children)];
  }

  return $data;

}