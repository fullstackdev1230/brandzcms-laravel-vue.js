<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\NotificationUserDeleted;
use App\Models\NotificationUserViewed;
use App\Repositories\Notifications\NotificationsRepository;
use App\Repositories\Users\UsersRepository;
use Illuminate\Http\Request;

class NotificationsController extends Controller
{
    protected $notifications;
    protected $users;

    public function __construct(NotificationsRepository $notifications, UsersRepository $users)
    {
        $this->notifications = $notifications;
        $this->users = $users;
    }

    public function getAll(Request $request)
    {

        $notifications = $this->notifications->getModel();

        if (isset($request->notification_type)) {
            $notifications = $this->notifications->getModel()->where('notification_type', '=', $request->notification_type);
        }

        $notifications = $notifications->get();

        if (isset($request->user_id)) {

            //Value = 0 IF not viewed yet by the user_id. Value = 1 IF viewed by the user_id
            if (!$notifications->isEmpty()) {

                foreach ($notifications as $key => $notification) {

                    //set all to is_viewed and is_deleted = 0
                    $notification->is_viewed = 0;
                    $notification->is_deleted = 0;

                    //remove html tag
                    $notification->notification_content = str_replace('<p>', '', $notification->notification_content);
                    $notification->notification_content = str_replace('</p>', '', $notification->notification_content);
                    $notificationUserViewed = $notification->notificationUserViewed->where('user_id', $request->user_id);

                    foreach ($notificationUserViewed as $userviewed) {

                        if ($notification->id == $userviewed->notification_id && $userviewed->app == 1) {
                            $notification->is_viewed = 1;
                        }
                    }

                    $notificationUserDeleted = $notification->notificationUserDeleted->where('user_id', $request->user_id);

                    foreach ($notificationUserDeleted as $userdeleted) {

                        if ($notification->id == $userdeleted->notification_id && $userdeleted->app == 1) {
                            $notification->is_deleted = 1;
                        }
                    }
                }
            }
        }

        if (!$notifications->isEmpty()) {

            foreach ($notifications as $notification) {

                unset($notification->notificationUserViewed);
                unset($notification->notificationUserDeleted);
            }

            return $notifications->values()->all();

        } else {

            return response()->json(['message' => 'Data not found']);
        }

    }

    public function addNotificationUserViewed(Request $request)
    {
        $request->validate([
            'notification_id' => 'required',
            'user_id' => 'required',
        ]);

        //check if notification exists
        $notification = $this->notifications->getModel()->where('id', '=', $request->notification_id)->first();

        if (!isset($notification)) {
            return response()->json(['message' => 'notification does not exists']);
        }

        //check if user exists
        $user = $this->users->getModel()->where('id', '=', $request->user_id)->first();

        if (!isset($user)) {
            return response()->json(['message' => 'user does not exists']);
        }

        $existingUserViewed = $notification->notificationUserViewed->where('user_id', '=', $user->id)->first();

        if (isset($existingUserViewed)) {

            return response()->json(['message' => 'data already exists']);

        } else {

            // If input doesn't exist, create new
            $notificationUserViewed = new NotificationUserViewed;

            $notificationUserViewed->notification_id = $request->notification_id;
            $notificationUserViewed->user_id = $request->user_id;
            $notificationUserViewed->app = 1;

            if ($notificationUserViewed->save()) {
                return response()->json(['message' => 'success']);
            } else {
                return response()->json(['message' => 'failed to save notification userviewed']);
            }
        }
    }

    public function addNotificationUserDeleted(Request $request)
    {
        $request->validate([
            'notification_id' => 'required',
            'user_id' => 'required',
        ]);

        //check if notification exists
        $notification = $this->notifications->getModel()->where('id', '=', $request->notification_id)->first();

        if (!isset($notification)) {
            return response()->json(['message' => 'notification does not exists']);
        }

        //check if user exists
        $user = $this->users->getModel()->where('id', '=', $request->user_id)->first();

        if (!isset($user)) {
            return response()->json(['message' => 'user does not exists']);
        }

        $existingUserDeleted = $notification->notificationUserDeleted->where('user_id', '=', $user->id)->first();

        if (isset($existingUserDeleted)) {
            return response()->json(['message' => 'data already exists']);
        } else {
            // If input doesn't exist, create new
            $notificationUserDeleted = new NotificationUserDeleted;

            $notificationUserDeleted->notification_id = $request->notification_id;
            $notificationUserDeleted->user_id = $request->user_id;
            $notificationUserDeleted->app = 1;

            if ($notificationUserDeleted->save()) {
                return response()->json(['message' => 'success']);
            } else {
                return response()->json(['message' => 'failed to save notification userdeleted']);
            }
        }
    }

}
