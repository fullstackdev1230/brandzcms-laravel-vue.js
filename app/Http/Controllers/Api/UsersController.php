<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Repositories\Layouts\LayoutsRepository;
use App\Repositories\Users\UsersRepository;
use Auth;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    protected $users;
    protected $layouts;
    protected $layoutImageLinks;

    public function __construct(UsersRepository $users, LayoutsRepository $layouts)
    {
        $this->users = $users;
        $this->layouts = $layouts;
    }

    public function getAll(Request $request)
    {

        $users = $this->users->getModel()->get();

        if (!$users->isEmpty()) {
            return $users;

        } else {
            return response()->json(['message' => 'Data not found']);
        }

    }

    public function get(Request $request)
    {

        $request->validate([
            'user_id' => 'required',
        ]);

        $user = $this->users->getModel()->where('id', $request->user_id)->first();

        if (isset($user)) {

            $user->favorites = $user->favoriteProducts->count();

            unset($user->favoriteProducts);

            if ($user->group) {

                $user->suppliers = $user->group->suppliers;

                unset($user->group->suppliers);
            }

            return $user;

        } else {

            return response()->json(['message' => 'Data not found']);
        }

    }

    public function login(Request $request)
    {

        $request->validate([
            'email'     => 'required',
            'device_id_check' => 'sometimes|filled|boolean',
            'device_id' => 'required_if:device_id_check,1',

        ]);

        $user = $this->users->getModel()->where('email', '=', $request->email)->first();
        $userValidate = false;

        // If password are provided
        if (isset($request->password)) {

            $userValidate = Auth::guard('web')->attempt(['email' => $request->email, 'password' => $request->password]);

        } else {

            $userValidate = Auth::guard('web')->loginUsingId($user->id);

        }

        if ($userValidate) {

            if (!isset($user->device_id)) {

                //if device id is null, store it
                $user->device_id = $request->device_id;
                $user->save();

            } else {

                if ( $request->device_id_check && $request->device_id != $user->device_id) {

                    return response()->json(['message' => 'device id does not match']);
                }
            }

            if ($user->group) {

                //if super user, return active users
                if ($user->group->group_name == "Super User") {
                    $user->users = $this->users->getModel()->select('id', 'name', 'email', 'device_id')->notExpired()->get();
                }

                $user->suppliers = $user->group->suppliers;

                unset($user->group->suppliers);

                $user->layout = $this->getLayout($user->group);

                if ($user->layout) {
                    $htmlTable = $user->layout->table_html;

                    $userImageLinks = $user->layout->layoutImageLinks;

                    foreach ($userImageLinks as $userImageLink) {

                        $searchVal = '>' . $userImageLink->td_id . '<';

                        $imgTag = '><img src="' . $userImageLink->full_image . '" data-type="' . $userImageLink->data_type . '" data-type_value="' . $userImageLink->data_type_value . '" /><';

                        $htmlTable = str_replace($searchVal, $imgTag, $htmlTable);
                    }

                    $user->layout->table_html = stripslashes(str_replace(["\r\n", "\t"], ["", ""], $htmlTable));

                    unset($user->layout->id);

                    //return layout cells array
                    $cells_array = [];
                    $cell_details = $user->layout->layoutImageLinks;

                    foreach ($cell_details as $key => $cell) {

                        unset($cell->id);
                        unset($cell->layout_id);
                        unset($cell->created_at);
                        unset($cell->updated_at);

                        $cells_array[$cell->td_id] = $cell;

                        unset($cell->td_id);

                    }

                    $user->layout->cells_array = $cells_array;

                    unset($user->layout->layoutImageLinks);
                }

                unset($user->group->layouts);
            }

            $user->favorites = $user->favoriteProducts->count();
            unset($user->favoriteProducts);

            return $user;

        } else {

            return response()->json(['code' => 403, 'error' => 'Incorrect Login Details.']);

        }
    }

    private function getLayout($group)
    {
        $userLayout = $group->layouts->where('set_as_default', '=', 1)->first();

        unset($userLayout->set_as_default);
        unset($userLayout->created_at);
        unset($userLayout->updated_at);
        unset($userLayout->pivot);

        return $userLayout;
    }

    public function register(Request $request)
    {
        $request->validate([
            'email'    => 'required|unique:users',
            'name'     => 'required',
            'password' => 'required',
        ]);

        $user = new User;

        $user->email = $request->email;
        $user->name = $request->name;
        $user->password = $request->password;

        if ($user->save()) {
            return response()->json(['message' => 'success']);
        } else {
            return response()->json(['message' => 'failed to save user']);
        }
    }

    public function update(Request $request)
    {
        $request->validate([
            'email'    => 'required_without:password',
            'password' => 'required_without:email',
        ]);

        $user = $this->users->getModel();

        if (isset($request->email)) {
            $user->email = $request->email;
        }

        if (isset($request->password)) {
            $user->password = bcrypt($request->password);
        }

        $user = $user->first();

        if (isset($user)) {
            if ($user->save()) {
                return $user;
            } else {
                return response()->json(['message' => 'failed to save user']);
            }
        } else {
            return response()->json(['message' => 'user does not exists']);
        }
    }

    public function edit(Request $request)
    {
        $request->validate([
            'user_id'       => 'required',
            'email'         => 'sometimes|filled|unique:users,email|email',
            'password'      => 'sometimes|filled',
            'accent_colour' => 'sometimes|filled',
            'device_id'     => 'sometimes|filled',
        ]);

        $user = $this->users->getModel()->where('id', '=', $request->user_id)->first();

        if (isset($user)) {

            if (isset($request->email)) {
                $user->email = $request->email;
            }

            if (isset($request->password)) {
                $user->password = bcrypt($request->password);
            }

            if (isset($request->accent_colour)) {
                $user->accent_colour = $request->accent_colour;
            }

            if (isset($request->device_id)) {
                $user->device_id = $request->device_id;
            }

            if ($user->save()) {

                if ($user->group) {

                    $user->suppliers = $user->group->suppliers;
                    unset($user->group->suppliers);
                }

                return $user;

            } else {

                return response()->json(['message' => 'failed to save user']);
            }

        } else {
            return response()->json(['message' => 'user does not exists']);
        }
    }

}
