<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use App\Models\Product;
use App\Models\Brochure;
use App\Models\LayoutImageLink;

use TCG\Voyager\Http\Controllers\VoyagerBaseController as BaseVoyagerBaseController;

class ActionsController extends Controller
{
    //
	public function generatePassword(Request $request){

		$random_password = Str::random(12);

	    return $random_password;
	}

	public function sendLoginDetails(Request $request){
		//INSERT SEND LOGIN DETAILS CODE HERE
		
		return 'success';
	}

	public function getTermsByGroupId(Request $request){
		$id = $_GET['id'];
		$query = DB::table('groups')->where('id', $id)->first()->term;

		return $query;
	}

	public function getAllProducts(Request $request){
		$query = DB::table('products')->get();
		return $query;
	}

    public function getExpiryDate(Request $request)
    {  
    	$expiryDate = null;
    	if(isset($_GET['product_id'])){
    		$product_id = $_GET['product_id'];	
    		$expiryDate = Product::where('id', '=', $product_id)->first();
    	}elseif(isset($_GET['brochure_id'])){
    		$brochure_id = $_GET['brochure_id'];
    		$expiryDate = Brochure::where('id', '=', $brochure_id)->first();
    	}

        if(!isset($expiryDate->visible_to)){
            return "not found";     
        }else{
            return trim($expiryDate->visible_to);
        }

    }

    public function updateImageLink(Request $request){

    	if(isset($_GET['layout_id']) && isset($_GET['layoutCells'])){

    		$cells = DB::table('layout_image_links')->where('layout_id', $_GET['layout_id'])->get();
    		if(isset($cells)){
    			foreach ($cells as $cell) {
    				if(!in_array( $cell->td_id , $_GET['layoutCells'] )){
    					LayoutImageLink::destroy($cell->id);
    				}
    			}
    			return 'success';
    		}else{
    			return 'cells not found';
    		}

    	}
    }

}
