<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Suppliers\SuppliersRepository;
use App\Repositories\Products\ProductsRepository;
use App\Repositories\Pcategories\PcategoriesRepository;

class SuppliersController extends Controller
{
	protected $suppliers;
    protected $products;
    protected $pcategories;
    protected $psubcategories;

	public function __construct(SuppliersRepository $suppliers, ProductsRepository $products, PcategoriesRepository $pcategories, PcategoriesRepository $psubcategories)
	{
		$this->suppliers = $suppliers;
        $this->products = $products;
        $this->pcategories = $pcategories;
        $this->psubcategories = $psubcategories;
	}

	public function getAll(Request $request){

    	$suppliers = $this->suppliers->getModel()->orderBy('name')->get();

    	if (!$suppliers->isEmpty()) {
        	return $suppliers;
           
        }else{
        	 return response()->json(['message' => 'Data not found']);
        }
        
	}

	public function get(Request $request){

		$request->validate([
    		'id' => 'required',
		]);

    	$suppliers = $this->suppliers->getModel()->where('id', $request->id)->first();

        $products = $this->products->getModel();

        $pcategories = [];
        $cat_id_list = [];

    	if (isset($suppliers)) {
            //get categories from products

            $products=$products->where('supplier_id', '=', $suppliers->id)->orderBy('name')->get();
            
            if(!$products->isEmpty()){
                foreach ($products as $p) {

                foreach ($p->categories as $category) {
                    
                    if(!in_array($category->name, $cat_id_list)){
                        array_push($cat_id_list, $category->name);
                        array_push($pcategories, $category);
                    }
                }
                
            }
            sort($pcategories);
            }

            $suppliers->pcategories = $pcategories;

        	return $suppliers;
           
        }else{
        	 return response()->json(['message' => 'Data not found']);
        }
	}

    public function getCategories(Request $request){

        $request->validate([
            'supplier_id' => 'required',
        ]);

        $suppliers = $this->suppliers->getModel()->where('id', $request->supplier_id)->first();

        $products = $this->products->getModel();

        //check if supplier exists
        if(isset($suppliers)){
            $products = $products->where('supplier_id', '=', $suppliers->id)->orderBy('name')->get();
        }else{
            return response()->json(['message' => 'Supplier not found']);
        }

        $cat_id_list = [];
        if (!$products->isEmpty()) {
            foreach ($products as $p) {

                foreach ($p->categories as $category) {
                    
                    if(!in_array($category->id, $cat_id_list)){
                        array_push($cat_id_list, $category->id);
                    }
                }
                
            }
            sort($cat_id_list);

            $pcategories = $this->pcategories->getModel()->whereIn('id',$cat_id_list )->where('parent_id', '=', NULL)->orderBy('name')->get();

            $psubcategories = $this->psubcategories->getModel()->whereIn('id',$cat_id_list )->where('parent_id', '!=', NULL)->orderBy('name')->get();


            //append parent of subcategories if not yet in the list
            $parentList = [];

            foreach ($psubcategories as $key => $psubcategory) {
                if (!in_array($psubcategory->parent_id, $cat_id_list) && !in_array($psubcategory->parent_id, $parentList))  {
                    array_push($parentList, $psubcategory->parent_id);
                }
            }
            //push additional parent categories
            foreach ($parentList as $parentCategory) {
                $pcategories->push($this->pcategories->getModel()->where('id', '=',$parentCategory)->first());
            }
            
            //assign subcategories to correspoding parent categories
            foreach ($pcategories as $key => $pcategory) {
                $tempSubCat = [];

                foreach ($psubcategories as $key => $psubcategory) {
                    if($psubcategory->parent_id == $pcategory->id){
                        array_push($tempSubCat , $psubcategory);
                    }
                    
                }
                $pcategory->subcategories = $tempSubCat;

            }
            
            return $pcategories;
        }else{
             return response()->json(['message' => 'Data not found']);
        }
    }

}
