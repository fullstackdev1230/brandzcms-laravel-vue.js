<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Users\UsersRepository;
use App\Repositories\Categories\CategoriesRepository;
use App\Repositories\Products\ProductsRepository;
use App\Repositories\Pcategories\PcategoriesRepository;
class CategoriesController extends Controller
{

	protected $users;
	protected $categories;
	protected $products;
	protected $pcategories;
	protected $psubcategories;

	public function __construct(UsersRepository $users, CategoriesRepository $categories, ProductsRepository $products, PcategoriesRepository $pcategories, PcategoriesRepository $psubcategories)
	{
		$this->users = $users;
		$this->categories = $categories;
		$this->products = $products;
		$this->pcategories = $pcategories;
		$this->psubcategories = $psubcategories;
	}

	public function getAll(Request $request){

		$request->validate([
    		'user_id' => 'required_without:parent_id',
            'parent_id' => 'required_without:user_id',
		]);

		if(isset($request->user_id)){
			$users = $this->users->getModel()->where('id', $request->user_id)->first();

	        if (isset($users)) {

	            if($users->group) {
	            	//return ALL unique parent category that is linked to a user
					$suppliers = $users->group->suppliers;

					$supplier_ids = [];
					if(!$suppliers->isEmpty()){
						foreach ($suppliers as $supplier) {
							array_push($supplier_ids, $supplier->id);
						}
						$products = $this->products->getModel()->wherein('supplier_id', $supplier_ids)->orderBy('name')->get();
	
						if(!$products->isEmpty()){

							$cat_id_list = [];
					        if (!$products->isEmpty()) {
					            foreach ($products as $p) {

					                foreach ($p->activeCategories as $category) {
					                    
					                    if(!in_array($category->id, $cat_id_list)){
					                        array_push($cat_id_list, $category->id);
					                    }
					                }
					                
					            }
					            sort($cat_id_list);

					            $pcategories = $this->pcategories->getModel()->whereIn('id',$cat_id_list )->where('parent_id', '=', NULL)->ordered()->get();

					            $psubcategories = $this->psubcategories->getModel()->whereIn('id',$cat_id_list )->where('parent_id', '!=', NULL)->ordered()->get();


					            //append parent of subcategories if not yet in the list
					            $parentList = [];
					            foreach ($psubcategories as $key => $psubcategory) {
					                if (!in_array($psubcategory->parent_id, $cat_id_list) && !in_array($psubcategory->parent_id, $parentList)) {
					                    array_push($parentList, $psubcategory->parent_id);
					                }
					            }
					            //push additional parent categories
					            foreach ($parentList as $parentCategory) {
					                $pcategories->push($this->pcategories->getModel()->where('id', '=',$parentCategory)->first());
					            }
					            
					            //assign subcategories to correspoding parent categories
					            foreach ($pcategories as $key => $pcategory) {
					                $tempSubCat = [];

					                foreach ($psubcategories as $key => $psubcategory) {
					                    if($psubcategory->parent_id == $pcategory->id){
					                        array_push($tempSubCat , $psubcategory);
					                    }
					                    
					                }
					                $pcategory->subcategories = $tempSubCat;

					            }

					            return $pcategories;
					        }







						}

					}

	            }else{
	            	return response()->json(['message' => 'Data not found']);
	            }

	        }else{
	        	 return response()->json(['message' => 'User does not exists']);
	        }

		}else if(isset($request->parent_id)){
			//return ALL categories who's parent_id is equal to the parent_id provided
			$categories = $this->categories->getModel()->where('parent_id', $request->parent_id)->orderBy('name')->get();

	        if (!$categories->isEmpty()) {

	        	return $categories;
	           
	        }else{
	        	 return response()->json(['message' => 'Data not found']);
	        }
		}

	}

	public function get(Request $request){

		$request->validate([
    		'id' => 'required',
		]);


		$category = $this->pcategories->getModel()->where('id', $request->id)->first();

		if(isset($category)){
			if($category->parent_id ==null){
				//get subcategories
				$subCategories = $this->pcategories->getModel()->where('parent_id', $request->id)->orderBy('name')->get();
				$category->subcategories = $subCategories;
			}
			return $category;
		}else{
			return response()->json(['message' => 'Data not found']);
		}

	}

}