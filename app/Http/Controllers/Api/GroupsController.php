<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Groups\GroupsRepository;

class GroupsController extends Controller
{

	protected $groups;

	public function __construct(GroupsRepository $groups)
	{
		$this->groups = $groups;
	}

	public function get(Request $request){

		$request->validate([
    		'id' => 'required',
		]);

        $groups = $this->groups->getModel()->where('id', $request->id)->get();

        if (!$groups->isEmpty()) {
        	return $groups;
           
        }else{
        	 return response()->json(['message' => 'Data not found']);
        }
	}

}
