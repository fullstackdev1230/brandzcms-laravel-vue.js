<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\OrderProduct;
use App\Repositories\Orders\OrdersRepository;
use App\Repositories\Products\ProductsRepository;
use App\Repositories\Users\UsersRepository;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class OrdersController extends Controller
{
    protected $orders;
    protected $users;

    public function __construct(OrdersRepository $orders, UsersRepository $users, ProductsRepository $products)
    {
        $this->orders = $orders;
        $this->users = $users;
        $this->products = $products;
    }

    public function getHistory(Request $request)
    {
        $request->validate([
            'user_id' => 'required',
        ]);

        $user = $this->users->getModel()->where('id', '=', $request->user_id)->first();

        if (isset($user)) {

            $orders = $this->orders->getModel()->where('user_id', '=', $request->user_id)->get();

            if (!$orders->isEmpty()) {
                foreach ($orders as $order) {

                    foreach ($order->orderProducts as $orderProduct) {
                        $orderProduct->product = $orderProduct->product;
                        unset($orderProduct->product_id);
                        unset($orderProduct->order_id);
                        unset($orderProduct->created_at);
                        unset($orderProduct->updated_at);

                    }

                }

                return $orders;
            } else {
                return response()->json(['message' => 'Data not found']);
            }

        } else {
            return response()->json(['message' => 'User does not exists']);
        }
    }

    public function deleteHistory(Request $request)
    {
        $request->validate([
            'order_id' => 'required_without:user_id',
            'user_id'  => 'required_without:order_id',

        ]);

        if (isset($request->order_id)) {

            $order = $this->orders->getModel()->where('id', '=', $request->order_id)->first();

            if (isset($order)) {

                OrderProduct::where('order_id', '=', $order->id)->delete();

                Order::where('id', '=', $order->id)->delete();

                return response()->json(['message' => 'success']);

            } else {
                return response()->json(['message' => 'Data not found']);
            }

        } else if (isset($request->user_id)) {

            $orders = $this->orders->getModel()->where('user_id', '=', $request->user_id)->get();

            if (!$orders->isEmpty()) {

                $order_ids = [];

                foreach ($orders as $order) {
                    array_push($order_ids, $order->id);
                }

                OrderProduct::wherein('order_id', $order_ids)->delete();

                Order::wherein('id', $order_ids)->delete();

                return response()->json(['message' => 'success']);

            } else {
                return response()->json(['message' => 'Data not found']);
            }

        }

    }

    public function add(Request $request)
    {

        $request->validate([
            'user_id' => 'required',
            'to'      => 'required',
        ]);

        $user = $this->users->getModel()->where('id', '=', $request->user_id)->first();

        if (isset($user)) {

            $order = new Order;

            $order->user_id = $request->user_id;
            $order->subject = $request->subject;
            $order->to_email = $request->to_email;
            $order->notes = $request->notes;

            if ($order->save()) {

                return response()->json(['message' => 'success']);

            } else {

                return response()->json(['message' => 'failed to save order']);
            }

        } else {
            return response()->json(['message' => 'User does not exists']);
        }
    }

    public function saveOrders(Request $request)
    {

        $request->validate([
            'orders' => 'required',
        ]);

        $data = json_decode($request->orders);

        if (isset($data)) {

            foreach ($data as $d) {

                //check if user exists
                $user = $this->users->getModel()->where('id', '=', $d->user_id)->first();

                if (isset($user)) {

                    $user_id = $user->id;
                    $products = $d->products;
                    $message = $d->message;

                    $newOrder = new Order();

                    $newOrder->user_id = $user_id;
                    $newOrder->subject = $message->subject;
                    $newOrder->to_email = $message->to;
                    $newOrder->notes = $message->notes;

                    if ($newOrder->save()) {
                        if (isset($products)) {
                            foreach ($products as $product) {
                                $newOrderProduct = new OrderProduct();

                                $newOrderProduct->order_id = $newOrder->id;
                                $newOrderProduct->product_id = $product->product_id;
                                $newOrderProduct->quantity = $product->quantity;

                                //check if product exists
                                $product = $this->products->getModel()->where('id', '=', $product->product_id)->first();

                                if (isset($product)) {
                                    $newOrderProduct->save();
                                }

                            }

                        }
                        //call function to send email
                        return $this->sendOrderEmail($message->notes, $products, $message->to, $message->subject);

                    } else {
                        return response()->json(['message' => 'failed to save order']);
                    }

                } else {
                    return response()->json(['message' => 'User does not exists']);
                }

            }
        }

    }

    public function sendOrderEmail($notes, $products, $to, $subject)
    {
        //create Order object for Sending Email
        $objOrder = new Order();
        $objOrder->notes = $notes;
        $objOrder->products = $products;

        if (isset($objOrder->products)) {
            foreach ($objOrder->products as $product) {
                $p = $this->products->getModel()->where('id', '=', $product->product_id)->first();
                if ($p) {
                    $product->name = $p->name;
                    $product->supplier = $p->supplier->name;
                    $product->rrp = $p->rrp;
                    $product->units_per_outer = $p->items_per_outer;
                    $product->quantity = $product->quantity;
                }
            }
        }

        Mail::send('mails.order', ['objOrder' => $objOrder], function ($message) use ($to, $subject) {

            $message->from(env('ORDER_FROM_EMAIL','info@brandzonline.com.au'),env('ORDER_FROM_NAME','Info'))->to($to)->subject($subject);

        });

        if (Mail::failures()) {
            return response()->json(['message' => 'success|failed to send email']);
        } else {
            return response()->json(['message' => 'success|email sent']);
        }

    }
}
