<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Bcategories\BcategoriesRepository;
use App\Repositories\Brochures\BrochuresRepository;
use App\Repositories\Users\UsersRepository;
use Illuminate\Support\Facades\Storage;

class BrochuresController extends Controller
{
	protected $brochures;
	protected $bcategories;
	protected $users;

	public function __construct(BcategoriesRepository $bcategories, BrochuresRepository $brochures, UsersRepository $users)
	{
		$this->brochures = $brochures;
		$this->bcategories = $bcategories;
		$this->users = $users;
	}

	public function getAll(Request $request){

		$request->validate([
    		'category' => 'required',
		]);

		$brochures = $this->brochures->getModel();

		$brochures_id = [];
        if(isset($request->user_id)){
        	$user = $this->users->getModel()->where('id', $request->user_id)->first();
        	if(isset($user)){
        		if(isset($user->group->brochures)){
        			//get brochure ids that belong to the user's group
        			foreach ($user->group->brochures as $brochure) {
        				if(!in_array($brochure->id, $brochures_id)){
        					array_push($brochures_id, $brochure->id);
        				}
        			}

        			$brochures = $brochures->byCategoryName($request->category)->wherein('id', $brochures_id)->orderBy('display_order','asc')->get();
        		}else{
        			return response()->json(['message' => 'Data not found']);
        		}
        	}else{
        		return response()->json(['message' => 'user does not exists']);
        	}

        }else{
        	$brochures = $brochures->byCategoryName($request->category)->orderBy('display_order','asc')->get();
        }

        if (!$brochures->isEmpty()) {

	        foreach ($brochures as $brochure) {

	        	$brochure_files =  json_decode($brochure->brochure_files);

				if(isset($brochure_files)) {

					foreach ($brochure_files as $file) {

						$filename = trim($file->download_link);
						$file_url = Storage::disk('s3')->url($filename);
						$file_url = str_replace('\\', '/', $file_url);

						$brochure->brochure_files = $file_url;
					}
				}
	        }

        	return $brochures;

        } else {

        	 return response()->json(['message' => 'Data not found']);
        }
	}

	public function get(Request $request){

		$request->validate([
    		'id' => 'required',
		]);

        $brochure = $this->brochures->getModel()->where('id', $request->id)->first();

        if ($brochure) {

        	$brochure_files =  json_decode($brochure->brochure_files);

			if(isset($brochure_files)) {

				foreach ($brochure_files as $file) {

					$filename = trim($file->download_link);
					$file_url = Storage::disk('s3')->url($filename);
					$file_url = str_replace('\\', '/', $file_url);

					$brochure->brochure_files = $file_url;
				}
			}

        	return $brochure;

        } else {
        	 return response()->json(['message' => 'Data not found']);
        }

	}

}

