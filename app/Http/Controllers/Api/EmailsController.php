<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\EmailContent;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Repositories\Brochures\BrochuresRepository;
use Illuminate\Support\Facades\Storage;
use \stdClass;

class EmailsController extends Controller
{
    protected $brochures;

    public function __construct(BrochuresRepository $brochures)
    {
        $this->brochures = $brochures;
    }

    public function send(Request $request)
    {

        $request->validate([
            'email_address' => 'required',
            'email_name'    => 'required',
            'subject'       => 'required',
        ]);

        $to = $request->email_address;
        $to_name = $request->email_name;
        $subject = $request->subject;

        $emailContent = new EmailContent();
        $emailContent->notes = $request->notes;

        $file = $request->file('attachment');
        $brochure_file = null;
        $file_name = "";

        if(isset($request->brochure_id)){
            $brochure = $this->brochures->getModel()->find($request->brochure_id);

            if(isset($brochure)){
                 $brochure_files = json_decode($brochure->brochure_files);


                if(isset($brochure_files)){
                    $attachments = [];

                    foreach ($brochure_files as $brochure_file) {
                        $original_name = trim($brochure_file->original_name);

                        $filename = trim($brochure_file->download_link);                   
                        $file_url = Storage::disk('s3')->url($filename);

                        //get file name, remove directory
                        $filename = explode("\\",$filename);
                        $filename = end($filename);

                        //replace backslash
                        $file_url = str_replace('\\', '/', $file_url);

                        $link =  new stdClass();
                        $link->url = $file_url;
                        $link->file_name = $filename;
                        array_push($attachments, $link);
                    }
                    $emailContent->attachments = $attachments;
                }
            }else{
                return response()->json(['message' => 'brochure does not exists']);
            }
            
        }        

        $user = Auth::user();

        Mail::send('mails.email', ['emailContent' => $emailContent], function ($message) use ($user, $to, $subject, $file, $attachments) {

            if (isset($file) && $file->isValid()) {
                $message->attach($file->getRealPath(), array(
                    'as'   => $file->getClientOriginalName(),
                    'mime' => $file->getMimeType()));
            }

            $message->from(env('ORDER_FROM_EMAIL', 'info@brandzonline.com.au'), env('ORDER_FROM_NAME', 'Info'))->to($to)->subject($subject);

        });

        if (Mail::failures()) {
            return response()->json(['message' => 'success|failed to send email']);
        } else {
            return response()->json(['message' => 'success|email sent']);
        }
    }

}
