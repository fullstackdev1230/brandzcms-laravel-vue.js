<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Pcategories\PcategoriesRepository;

class PcategoriesController extends Controller
{
	protected $pcategories;

	public function __construct(PcategoriesRepository $pcategories)
	{
		$this->pcategories = $pcategories;
	}

	public function getAll(Request $request){

    	$pcategories = $this->pcategories->getModel()->get();

    	if (!$pcategories->isEmpty()) {
        	return $pcategories;
           
        }else{
        	 return response()->json(['message' => 'Data not found']);
        }
        
	}

	public function get(Request $request){

		$request->validate([
    		'id' => 'required',
		]);

    	$pcategories = $this->pcategories->getModel()->where('id', $request->id)->get();

    	if (!$pcategories->isEmpty()) {
        	return $pcategories;
           
        }else{
        	 return response()->json(['message' => 'Data not found']);
        }

	}

}
