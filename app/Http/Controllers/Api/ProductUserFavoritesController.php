<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\ProductUserFavorites\ProductUserFavoritesRepository;
use App\Repositories\Products\ProductsRepository;
use App\Repositories\Users\UsersRepository;
use App\Models\ProductUserFavorite;
use App\Models\Products;
use App\Models\Users;
class ProductUserFavoritesController extends Controller
{
	protected $product_user_favorites;
    protected $products;
    protected $users;

	public function __construct(ProductUserFavoritesRepository $product_user_favorites, ProductsRepository $products, UsersRepository $users)
	{
		$this->product_user_favorites = $product_user_favorites;
        $this->products = $products;
        $this->users = $users;
	}

	public function get(Request $request){

		$request->validate([
    		'user_id' => 'required_without:product_id',
            'product_id' => 'required_without:user_id',
		]);

        $product_user_favorites = $this->product_user_favorites->getModel();
   

        if(isset($request->user_id)){
            $user = $this->users->getModel()->where('id', '=', $request->user_id)->first();
            if(isset($user)){
                $product_user_favorites = $product_user_favorites->where('user_id', '=', $request->user_id); 
            }else{
                return response()->json(['message' => 'User does not exists']);
            }
        }

        if(isset($request->product_id)){

            $product = $this->products->getModel()->where('id', '=', $request->product_id)->first();

            if(isset($product)){
                $product_user_favorites = $product_user_favorites->where('product_id', '=', $request->product_id);
            }else{
                return response()->json(['message' => 'Product does not exists']);
            }
        }

        $product_user_favorites = $product_user_favorites->get();

        $products = [];
    	if (!$product_user_favorites->isEmpty()) {
            foreach ($product_user_favorites as $product_user_favorite) {
                array_push($products, $product_user_favorite->product);
            }

            if(isset($products)){
                foreach ($products as $product) {
                    //add supplier_name
                    $product->supplier_name = $product->supplier->name;
                    unset($product->supplier);
                }

                return $products;
            }

           
        }else{
        	 return response()->json(['message' => 'Data not found']);
        }

	}

    public function save(Request $request){

        $request->validate([
            'user_id' => 'required',
            'product_id' => 'required',
        ]);

        //check if user exists
        $user = $this->users->getModel()->where('id', '=', $request->user_id)->first();

        if(!isset($user)){
            return response()->json(['message' => 'User does not exists']);
        }

        //check if product exists
        $product = $this->products->getModel()->where('id', '=', $request->product_id)->first();

        if(!isset($product)){
            return response()->json(['message' => 'Product does not exists']);
        }

        // Check if input already exists
        $product_user_favorites = $this->product_user_favorites->getModel()->where('user_id', '=', $request->user_id)->where('product_id', '=', $request->product_id)->get();
        if(!$product_user_favorites->isEmpty()){
            return response()->json(['message' => 'data already exists']);
        }

        // If input doesn't exist, create new
        $product_user_favorites = new ProductUserFavorite;

        $product_user_favorites->user_id = $request->user_id;
        $product_user_favorites->product_id = $request->product_id;

        if($product_user_favorites->save()){

            $product_user_favorites = $product_user_favorites->where('user_id', '=', $request->user_id)->get(); 

            $products = [];
            foreach ($product_user_favorites as $product_user_favorite) {
                array_push($products, $product_user_favorite->product);
            }

            if(isset($products)){
                foreach ($products as $product) {
                    //add supplier_name
                    $product->supplier_name = $product->supplier->name;
                    unset($product->supplier);
                }

                return $products;
            }

        }else{
            return response()->json(['message' => 'failed to save product user favorites']);
        }


    }


    public function delete(Request $request){

        $request->validate([
            'user_id' => 'required',
            'product_id' => 'required_without:user_id',
        ]);

        //check if user exists
        $user = $this->users->getModel()->where('id', '=', $request->user_id)->first();

        if(!isset($user)){
            return response()->json(['message' => 'User does not exists']);
        }

        if(isset($request->product_id)){
            //check if product exists
            $product = $this->products->getModel()->where('id', '=', $request->product_id)->first();

            if(!isset($product)){
                return response()->json(['message' => 'Product does not exists']);
            }
        }


        // Check if input already exists
        $product_user_favorites = $this->product_user_favorites->getModel()->where('user_id', '=', $request->user_id);

        if(isset($request->product_id)){
            $product_user_favorites->where('product_id', '=', $request->product_id);
        }

        $product_user_favorites->get();

        if(isset($product_user_favorites)){
            $delete = ProductUserFavorite::where('user_id', '=', $request->user_id);

            if(isset($request->product_id)){
                $delete->where('product_id', '=', $request->product_id);
            }


            if($delete->delete()){

                if(isset($request->product_id)){
                    //return list of user's favorites after deleting one product
                    $product_user_favorites = $this->product_user_favorites->getModel()->where('user_id', '=', $request->user_id)->get();

                    $products = [];
                    if (!$product_user_favorites->isEmpty()) {
                        foreach ($product_user_favorites as $product_user_favorite) {
                            array_push($products, $product_user_favorite->product);
                        }

                        if(isset($products)){
                            foreach ($products as $product) {
                                //add supplier_name
                                $product->supplier_name = $product->supplier->name;
                                unset($product->supplier);
                            }

                            return $products;
                        }

                       
                    }else{
                         return response()->json(['message' => 'Data not found']);
                    }
                }

                return response()->json(['message' => 'success']);
            }else{
                return response()->json(['message' => 'failed to delete product user favorites']);
            }


        }else{
            return response()->json(['message' => 'Data not found']);
        }






    }
}
