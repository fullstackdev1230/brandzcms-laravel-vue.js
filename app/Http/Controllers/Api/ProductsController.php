<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Repositories\Pcategories\PcategoriesRepository;
use App\Repositories\Products\ProductsRepository;
use App\Repositories\Suppliers\SuppliersRepository;
use App\Repositories\Users\UsersRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductsController extends Controller
{
    protected $products;
    protected $suppliers;
    protected $pcategories;
    protected $users;

    public function __construct(ProductsRepository $products, SuppliersRepository $suppliers, PcategoriesRepository $pcategories, UsersRepository $users)
    {
        $this->products = $products;
        $this->suppliers = $suppliers;
        $this->pcategories = $pcategories;
        $this->users = $users;
    }

    public function getAll(Request $request)
    {
        if (isset($request->product_is)) {

            $request->validate([
                'user_id' => 'required',
            ]);

            $product_is = ['new', 'featured', 'merchandising'];

            if (!in_array($request->product_is, $product_is)) {
                return response()->json(['message' => 'Invalid product_is']);

            }

            $user = $this->users->getModel()->where('id', '=', $request->user_id)->first();

            if ($user) {

                if ($user->group) {

                    if ($user->group->suppliers) {

                        $suppliers = $user->group->suppliers;

                        $product_ids = [];

                        foreach ($suppliers as $supplier) {

                            $products = $supplier->products;

                            foreach ($products as $product) {
                                array_push($product_ids, $product->id);
                            }
                        }

                        $product_is = 'product_is_' . $request->product_is;

                        $products = $this->products->getModel()->wherein('id', $product_ids)->where($product_is, '=', 1);

                        if(isset($request->limit)) {

                            $products = $products->take($request->limit);

                              if(isset($request->offset)) {

                                $products = $products->skip($request->offset);
                            }
                        }

                        $products = $products->notExpired()->ordered()->get();

                        if (!$products->isEmpty()) {

                            foreach ($products as $product) {
                                //add pcategories array
                                $product->categories = $product->categories;
                                //add supplier_name
                                $product->supplier_name = $product->supplier->name;
                                unset($product->supplier);
                            }

                            return $products;

                        } else {
                            return response()->json(['message' => 'Data not found']);
                        }

                    } else {
                        return response()->json(['message' => 'Suppliers not found']);
                    }
                } else {
                    return response()->json(['message' => 'User Group not found']);
                }

            } else {
                return response()->json(['message' => 'User does not exists']);
            }
        }

        $products = $this->products->getModel();
        $suppliers = $this->suppliers->getModel();
        $pcategories = $this->pcategories->getModel();

        if (isset($request->supplier_id)) {

            $suppliers = $suppliers->where('id', '=', $request->supplier_id)->first();

            if (empty($suppliers)) {

                return response()->json(['message' => 'Supplier not found']);
            }

            $products = $products->where('supplier_id', '=', $suppliers->id);
        }

        if (isset($request->category_id)) {

            // Get pcategories by name
            $pcategories = $pcategories->where('id', '=', $request->category_id)->first();

            if (empty($pcategories)) {

                return response()->json(['message' => 'Category not found']);

            } else {

                // Get product_ids by pcategory_id from product_category table

                //CHANGE THIS TO MODEL RELATIONSHIP - belongstomany
                $product_category = DB::table('product_category')->select('product_id')->where('pcategory_id', '=', $pcategories->id)->get();

                $product_id_arr = [];

                foreach ($product_category as $key) {

                    array_push($product_id_arr, $key->product_id);
                }

                $products = $products->whereIn('id', $product_id_arr);

            }
        }

        if(isset($request->limit)) {

            $products = $products->take($request->limit);

              if(isset($request->offset)) {

                $products = $products->skip($request->offset);
            }
        }

        $products = $products->notExpired()->ordered()->get();

        if (!$products->isEmpty()) {

            foreach ($products as $product) {

                //add pcategories array
                $product->categories = $product->categories;

                //add supplier_name
                $product->supplier_name = $product->supplier->name;

                unset($product->supplier);
                unset($product->media);
            }

            return $products;

        } else {
            return response()->json(['message' => 'Data not found']);
        }

    }

    public function get(Request $request)
    {

        $request->validate([
            'id' => 'required',
        ]);

        $products = $this->products->getModel()->where('id', $request->id)->get();

        if (!$products->isEmpty()) {
            return $products;

        } else {
            return response()->json(['message' => 'Data not found']);
        }
    }

    public function search(Request $request)
    {

        $request->validate([
            'keyword' => 'required',
        ]);

        $keyword = '%' . $request->keyword . '%';
        $products = $this->products->getModel()
                        ->where('barcode_unit', 'LIKE', $keyword)
                        ->orWhere('name', 'LIKE', $keyword)
                        ->orWhere('description', 'LIKE', $keyword);

        // Offset Limit
        if(isset($request->limit)) {

            $products = $products->take($request->limit);

              if(isset($request->offset)) {

                $products = $products->skip($request->offset);
            }
        }

        $products = $products->notExpired()->ordered()->get();

        if (!$products->isEmpty()) {

            foreach ($products as $product) {

                //add pcategories array
                $product->categories = $product->categories;

                //add supplier_name
                $product->supplier_name = $product->supplier->name;
                unset($product->supplier);
            }
            return $products;

        } else {
            return response()->json(['message' => 'Data not found']);
        }
    }

}
