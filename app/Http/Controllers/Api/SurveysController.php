<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\SurveyQuestion;
use App\Models\SurveySubmission;
use App\Models\SurveySubmissionAnswer;
use App\Models\SurveyUserDeleted;
use App\Models\SurveyUserViewed;
use App\Repositories\Surveys\SurveysRepository;
use App\Repositories\Users\UsersRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SurveysController extends Controller
{
    protected $surveys;
    protected $users;

    public function __construct(SurveysRepository $surveys, UsersRepository $users)
    {
        $this->surveys = $surveys;
        $this->users = $users;
    }

    public function getAll(Request $request)
    {

        $surveys = $this->surveys->getModel()->with(['questions', 'questions.answers'])->get();

        if (isset($request->user_id)) {

            if (!$surveys->isEmpty()) {

                foreach ($surveys as $survey) {

                    //set all to is_viewed and is_deleted = 0
                    $survey->is_viewed = 0;
                    $survey->is_deleted = 0;

                    $surveyUserViewed = $survey->surveyUserViewed->where('user_id', '=', $request->user_id);

                    foreach ($surveyUserViewed as $userviewed) {

                        if ($survey->id == $userviewed->survey_id) {
                            $survey->is_viewed = 1;
                        }
                    }

                    $surveyUserDeleted = $survey->surveyUserDeleted->where('user_id', '=', $request->user_id);

                    foreach ($surveyUserDeleted as $userdeleted) {
                        if ($survey->id == $userdeleted->survey_id) {
                            $survey->is_deleted = 1;
                        }
                    }
                }
            }
        }

        if ($surveys->isEmpty()) {

            return response()->json(['message' => 'Data not found']);

        } else {

            return $surveys;
        }

    }

    public function submitSurvey(Request $request)
    {

        $request->validate([
            'user_id'   => 'required',
            'survey_id' => 'required',
        ]);

        $survey_id = $request->survey_id;
        $user_id = $request->user_id;

        // We check first if survey exists
        $survey = $this->surveys->getModel()->where('id', $survey_id)->first();

        if ($survey === null) {

            return response()->json(['message' => 'Invalid Survey ID']);

        } else {

            DB::beginTransaction();

            try {

                // We create entry for the submission of the survey
                $submit_data = new SurveySubmission(['survey_id' => $survey->id, 'user_id' => $user_id]);
                $survey_submit = $survey->submissions()->save($submit_data);

                // We will now insert the survey answers
                $questions = $request->question;

                // We will loop through the answered questions
                foreach ($questions as $question_id => $answer) {

                    // We check question type
                    $check_question = SurveyQuestion::find($question_id);

                    if ($check_question->question_type == 'checkbox') {
                        //We save each checkbox entry
                        foreach ($answer as $key => $value) {
                            $submit_data_answer = new SurveySubmissionAnswer();
                            $submit_data_answer->survey_submission_id = $survey_submit->id;
                            $submit_data_answer->survey_question_id = $question_id;
                            $submit_data_answer->survey_answer_id = $value;
                            $submit_data_answer->save();
                        }
                    } else {
                        $submit_data_answer = new SurveySubmissionAnswer();
                        $submit_data_answer->survey_submission_id = $survey_submit->id;
                        $submit_data_answer->survey_question_id = $question_id;
                        if ($check_question->question_type != "text") {
                            $submit_data_answer->survey_answer_id = $answer; //We save the id
                        } else {
                            $submit_data_answer->answer_text = $answer; //we save the text
                        }
                        $submit_data_answer->save();
                    }

                }

            } catch (\Illuminate\Database\QueryException $e) {

                DB::rollback();
                return response()->json(['message' => 'Submitting Survey Failed!']);

            } catch (\Exception $e) {

                DB::rollback();
                throw $e;
            }

            DB::commit();
            return response()->json(['message' => 'Submitting Survey Success!']);

        }

    }

    public function addSurveyUserViewed(Request $request)
    {
        $request->validate([
            'survey_id' => 'required',
            'user_id'   => 'required',
        ]);

        //check if survey exists
        $survey = $this->surveys->getModel()->where('id', '=', $request->survey_id)->first();

        if (!isset($survey)) {
            return response()->json(['message' => 'survey does not exists']);
        }

        //check if user exists
        $user = $this->users->getModel()->where('id', '=', $request->user_id)->first();

        if (!isset($user)) {
            return response()->json(['message' => 'user does not exists']);
        }

        $existingUserViewed = $survey->surveyUserViewed->where('user_id', '=', $user->id)->first();

        if (isset($existingUserViewed)) {
            return response()->json(['message' => 'data already exists']);
        } else {
            // If input doesn't exist, create new
            $surveyUserViewed = new SurveyUserViewed;

            $surveyUserViewed->survey_id = $request->survey_id;
            $surveyUserViewed->user_id = $request->user_id;

            if ($surveyUserViewed->save()) {
                return response()->json(['message' => 'success']);
            } else {
                return response()->json(['message' => 'failed to save survey userviewed']);
            }
        }
    }

    public function addSurveyUserDeleted(Request $request)
    {
        $request->validate([
            'survey_id' => 'required',
            'user_id'   => 'required',
        ]);

        //check if survey exists
        $survey = $this->surveys->getModel()->where('id', '=', $request->survey_id)->first();

        if (!isset($survey)) {
            return response()->json(['message' => 'survey does not exists']);
        }

        //check if user exists
        $user = $this->users->getModel()->where('id', '=', $request->user_id)->first();

        if (!isset($user)) {
            return response()->json(['message' => 'user does not exists']);
        }

        $existingUserDeleted = $survey->surveyUserDeleted->where('user_id', '=', $user->id)->first();

        if (isset($existingUserDeleted)) {
            return response()->json(['message' => 'data already exists']);
        } else {
            // If input doesn't exist, create new
            $surveyUserDeleted = new SurveyUserDeleted;

            $surveyUserDeleted->survey_id = $request->survey_id;
            $surveyUserDeleted->user_id = $request->user_id;

            if ($surveyUserDeleted->save()) {
                return response()->json(['message' => 'success']);
            } else {
                return response()->json(['message' => 'failed to save survey userdeleted']);
            }
        }
    }

}
