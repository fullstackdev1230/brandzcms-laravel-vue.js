<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Voyager\VoyagerBaseController as BaseVoyagerController;
use Illuminate\Http\Request;
use TCG\Voyager\Facades\Voyager;
use App\Repositories\Bcategories\BcategoriesRepository;
use App\Repositories\Brochures\BrochuresRepository;
use Auth;
class BrochuresController extends BaseVoyagerController
{

	protected $brochures;
	protected $bcategories;

	public function __construct(BcategoriesRepository $bcategories, BrochuresRepository $brochures)
	{
		$this->brochures = $brochures;
		$this->bcategories = $bcategories;
	}

    public function displayOrder(Request $request, $categoryId = null)
    {
        $dataType = Voyager::model('DataType')->where('slug', '=', 'brochures')->first();

        $brochures = [];

        if(!empty($categoryId)){
        	$brochures = $this->brochures->getModel()->byCategoryId($categoryId)->orderBy('display_order','asc')->get();
        }

    	return view('vendor.voyager.brochures.order-display',[
    		'dataType' => $dataType,
    		'bcategories' => $this->bcategories->all(['*'],'name'),
    		'brochures' => $brochures,
    		'brochureId' => $categoryId,
    	]);
    }

    public function changeOrder(Request $request)
    {
    	$orderedId = $request->get('order');

    	foreach ($orderedId as $key => $value) {

    		$this->brochures->update(['display_order' => $key+1],$value);
    	}

    	return response()->json([
			'message' => 'success'
		],200);
    }

    public function index(Request $request)
    {
        $user = Auth::user();
        $suppliers = $user->group->suppliers;
        $supplierList = [];
        if(!$suppliers->isEmpty()){
            foreach ($suppliers as $supplier) {
                array_push($supplierList, $supplier->id);
            }
        }

        // GET THE SLUG, ex. 'posts', 'pages', etc.
        $slug = $this->getSlug($request);

        // GET THE DataType based on the slug
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('browse', app($dataType->model_name));

        $getter = $dataType->server_side ? 'paginate' : 'get';

        $search = (object) ['value' => $request->get('s'), 'key' => $request->get('key'), 'filter' => $request->get('filter')];
        $searchable = $dataType->server_side ? array_keys(SchemaManager::describeTable(app($dataType->model_name)->getTable())->toArray()) : '';
        $orderBy = $request->get('order_by');
        $sortOrder = $request->get('sort_order', null);

        // Next Get or Paginate the actual content from the MODEL that corresponds to the slug DataType
        if (strlen($dataType->model_name) != 0) {
            $relationships = $this->getRelationships($dataType);

            $model = app($dataType->model_name);
            $query = $model::select('*')->with($relationships);
            $query = $query->wherein('supplier_id', $supplierList);
            // If a column has a relationship associated with it, we do not want to show that field
            $this->removeRelationshipField($dataType, 'browse');

            if ($search->value && $search->key && $search->filter) {
                $search_filter = ($search->filter == 'equals') ? '=' : 'LIKE';
                $search_value = ($search->filter == 'equals') ? $search->value : '%'.$search->value.'%';
                $query->where($search->key, $search_filter, $search_value);
            }

            if ($orderBy && in_array($orderBy, $dataType->fields())) {
                $querySortOrder = (!empty($sortOrder)) ? $sortOrder : 'DESC';
                $dataTypeContent = call_user_func([
                    $query->orderBy($orderBy, $querySortOrder),
                    $getter,
                ]);
            } elseif ($model->timestamps) {
                $dataTypeContent = call_user_func([$query->latest($model::CREATED_AT), $getter]);
            } else {
                $dataTypeContent = call_user_func([$query->orderBy($model->getKeyName(), 'DESC'), $getter]);
            }

            // Replace relationships' keys for labels and create READ links if a slug is provided.
            $dataTypeContent = $this->resolveRelations($dataTypeContent, $dataType);
        } else {
            // If Model doesn't exist, get data from table name
            $dataTypeContent = call_user_func([DB::table($dataType->name), $getter]);
            $model = false;
        }

        // Check if BREAD is Translatable
        if (($isModelTranslatable = is_bread_translatable($model))) {
            $dataTypeContent->load('translations');
        }

        // Check if server side pagination is enabled
        $isServerSide = isset($dataType->server_side) && $dataType->server_side;

        $view = 'voyager::bread.browse';

        if (view()->exists("voyager::$slug.browse")) {
            $view = "voyager::$slug.browse";
        }

        return Voyager::view($view, compact(
            'dataType',
            'dataTypeContent',
            'isModelTranslatable',
            'search',
            'orderBy',
            'sortOrder',
            'searchable',
            'isServerSide'
        ));
    }
}
