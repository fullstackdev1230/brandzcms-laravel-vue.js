<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Voyager\VoyagerBaseController as BaseVoyagerController;
use Illuminate\Http\Request;
use TCG\Voyager\Facades\Voyager;
use App\Repositories\Surveys\SurveysRepository;
use TCG\Voyager\Events\BreadDataAdded;
use TCG\Voyager\Events\BreadDataUpdated;
use Illuminate\Support\Facades\DB;

use App\Models\Survey;
use App\Models\SurveyQuestion;
use App\Models\SurveyQuestionAnswer;
use App\Models\Group;

class SurveysController extends BaseVoyagerController
{

	protected $surveys;

	public function __construct(SurveysRepository $surveys)
	{
		$this->surveys = $surveys;
	}

    public function show(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        $relationships = ['groups','questions','questions.answers','questions.answers.chosen','submissions'];

        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);
            $dataTypeContent = call_user_func([$model->with($relationships), 'findOrFail'], $id);
        } else {
            // If Model doest exist, get data from table name
            $dataTypeContent = DB::table($dataType->name)->where('id', $id)->first();
        }

        // Check permission
        $this->authorize('read', $dataTypeContent);

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        $view = 'voyager::bread.read';

        if (view()->exists("voyager::$slug.read")) {
            $view = "voyager::$slug.read";
        }
        
        return Voyager::view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable'));
    }

	public function store(Request $request){

		$request->validate([
		    'publish_date' => 'required',
		    'survey_title' => 'required',
		]);

		$slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('add', app($dataType->model_name));

        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->addRows);

        if ($val->fails()) {
            return response()->json(['errors' => $val->messages()]);
        }

        if (!$request->has('_validate')) {

            DB::beginTransaction();

            $data = $this->insertUpdateData($request, $slug, $dataType->addRows, new $dataType->model_name());

            event(new BreadDataAdded($dataType, $data));

            /**
             * Insert Survey Questions
             */
            $survey_id = $data->id;

            try {
			    // Validate, then create if valid
			    foreach ($request->question as $key => $question) {
	            	$question_type = $question["'question_type'"];
	            	$question_text = is_null($question["'question_text'"])?"":$question["'question_text'"];

	            	$survey_question = new SurveyQuestion;
	            	$survey_question->survey_id 		= $survey_id;
	            	$survey_question->question_type 	= $question_type;
	            	$survey_question->question_text 	= $question_text;
	            	$survey_question->question_order 	= 0;
	            	$survey_question->save();


	            	//Insert Options for each question except if type == text
	            	if($question_type != 'text'){

	            		$question_id = $survey_question->id;

	            		foreach ($question["'option_text'"] as $optionKey => $option) {
		            		$survey_question_answer = new SurveyQuestionAnswer;
		            		$survey_question_answer->survey_question_id = $question_id;
		            		$survey_question_answer->answer_text = is_null($option)?"":$option;
		            		$survey_question_answer->save();
		            	}
	            	}
	            }
			} catch(ValidationException $e){
			    // Rollback and then redirect
			    // back to form with errors
			    DB::rollback();
			    return redirect()
                ->route("voyager.{$dataType->slug}.index")
                ->with([
                        'message'    => $e->getErrors(),
                        'alert-type' => 'danger',
                    ]);
			} catch(\Exception $e){
			    DB::rollback();
			    throw $e;
			}


            if ($request->ajax()) {
                return response()->json(['success' => true, 'data' => $data]);
            }

            DB::commit();
            return redirect()
                ->route("voyager.{$dataType->slug}.index")
                ->with([
                        'message'    => __('voyager::generic.successfully_added_new')." {$dataType->display_name_singular}",
                        'alert-type' => 'success',
                    ]);
        }

	}


    public function edit(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
        $relationships = ['questions','questions.answers'];

        $dataTypeContent = (strlen($dataType->model_name) != 0)
            ? app($dataType->model_name)->with($relationships)->findOrFail($id)
            : DB::table($dataType->name)->where('id', $id)->first(); // If Model doest exist, get data from table name

        foreach ($dataType->editRows as $key => $row) {
            $details = $row->details;
            $dataType->editRows[$key]['col_width'] = isset($details->width) ? $details->width : 100;
        }

        // Check permission
        $this->authorize('edit', $dataTypeContent);

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        $view = 'voyager::bread.edit-add';

        if (view()->exists("voyager::$slug.edit-add")) {
            $view = "voyager::$slug.edit-add";
        }

        return Voyager::view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable'));
    }

    public function update(Request $request, $id)
    {

        $request->validate([
            'publish_date' => 'required',
            'survey_title' => 'required',
        ]);

        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Compatibility with Model binding.
        $id = $id instanceof Model ? $id->{$id->getKeyName()} : $id;

        $data = call_user_func([$dataType->model_name, 'findOrFail'], $id);

        // Check permission
        $this->authorize('edit', $data);

        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->editRows, $dataType->name, $id);

        if ($val->fails()) {
            return response()->json(['errors' => $val->messages()]);
        }

        if (!$request->ajax()) {

            DB::beginTransaction();

            $this->insertUpdateData($request, $slug, $dataType->editRows, $data);

            event(new BreadDataUpdated($dataType, $data));

            $relationships = ['questions','questions.answers'];
            $surveyContent = app($dataType->model_name)->with($relationships)->findOrFail($id);

            /**
             * Update Survey Questions
             */

            $questionIDs = [];
            $survey_id = $id;

            try {
                // Validate, then create if valid
                foreach ($request->question as $key => $question) {

                    $question_id   = $question["'question_id'"];
                    $question_type = $question["'question_type'"];
                    $question_text = is_null($question["'question_text'"])?"":$question["'question_text'"];

                    $survey_fetch = ['id' => $question_id,'survey_id' => $survey_id];
                    $survey_data  = ['question_type' => $question_type,
                                     'survey_id'     => $survey_id,
                                     'question_text' => $question_text,
                                     'question_order' => 0];

                    $survey_question = SurveyQuestion::updateOrCreate($survey_fetch,$survey_data);

                    //We get question ids to use later on to know which to remove
                    array_push($questionIDs,$survey_question->id);

                    // Insert Options for each question except if type == text
                    $optionIDs = [];
                    if($question_type != 'text'){
                        $question_id = $survey_question->id;

                        foreach ($question["'option_text'"] as $optionKey => $option) {

                            $option_id      = $question["'option_id'"][$optionKey];
                            $option_text    = is_null($option)?"":$option;
                            $option_fetch   = ['id' => $option_id,'survey_question_id' => $question_id];
                            $option_data    = ['survey_question_id' => $survey_question->id,
                                               'answer_text'     => $option_text];

                            $survey_question_answer = SurveyQuestionAnswer::updateOrCreate($option_fetch,$option_data);

                            //We get option ids to use later on to know which to remove
                            array_push($optionIDs,$survey_question_answer->id);

                        }

                    }

                    //We will delete removed options
                    $old_option_ids = SurveyQuestionAnswer::where('survey_question_id',$question_id)->pluck('id')->toArray();
                    $new_option_ids = $optionIDs;
                    $remove_option_ids = array_diff($old_option_ids,$new_option_ids);
                    SurveyQuestionAnswer::destroy($remove_option_ids);

                }

                //We will delete removed questions
                $old_question_ids = SurveyQuestion::where('survey_id',$survey_id)->pluck('id')->toArray();
                $new_question_ids = $questionIDs;
                $remove_question_ids = array_diff($old_question_ids,$new_question_ids);
                SurveyQuestion::destroy($remove_question_ids);



            } catch(ValidationException $e){
                // Rollback and then redirect
                // back to form with errors
                DB::rollback();
                return redirect()
                ->route("voyager.{$dataType->slug}.index")
                ->with([
                        'message'    => $e->getErrors(),
                        'alert-type' => 'danger',
                    ]);
            } catch(\Exception $e){
                DB::rollback();
                throw $e;
            }

            if ($request->ajax()) {
                return response()->json(['success' => true, 'data' => $data]);
            }

            DB::commit();

            return redirect()
                ->route("voyager.{$dataType->slug}.index")
                ->with([
                    'message'    => __('voyager::generic.successfully_updated')." {$dataType->display_name_singular}",
                    'alert-type' => 'success',
                ]);
        }
    }
}
