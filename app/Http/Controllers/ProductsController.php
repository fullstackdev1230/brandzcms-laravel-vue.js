<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Voyager\VoyagerBaseController as BaseVoyagerController;
use Illuminate\Http\Request;
use TCG\Voyager\Database\Schema\SchemaManager;
use TCG\Voyager\Facades\Voyager;
use Session;

class ProductsController extends BaseVoyagerController
{
    //
    protected $products;

    public function index(Request $request)
    {
        // GET THE SLUG, ex. 'posts', 'pages', etc.
        $slug = $this->getSlug($request);

        // GET THE DataType based on the slug
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('browse', app($dataType->model_name));

        $getter = $dataType->server_side ? 'paginate' : 'get';

        $pagination = $request->get('filter_per_page');
        //$search = (object) ['value' => $request->get('s'), 'key' => $request->get('key'), 'filter' => $request->get('filter')];
        $search = (object) ['value' => $request->get('s'),
            'supplier'                  => $request->get('filter_supplier'),
            'category'                  => $request->get('filter_category'),
            'paging'                    => $request->get('filter_per_page')];

        if (Session::has('currentPage') && ($dataType->slug != Session::get('currentPage'))) {
            Session::put('filter_category', '' );
            Session::put('filter_supplier', '' );
            Session::put('filter_per_page', '10' );
            Session::put('currentPage', $dataType->slug );
 

        } else {
            Session::put('currentPage', $dataType->slug );
        }



        if(isset($search->category)){
            Session::put('filter_category', $search->category);
        }else{
            Session::put('filter_category', Session::get('filter_category'));
        }
            
        if(isset($search->supplier)){
            Session::put('filter_supplier', $search->supplier);
        }else{
            Session::put('filter_supplier', Session::get('filter_supplier'));
        }

        if(isset($search->paging)){
            Session::put('filter_per_page', $search->paging);
        }else{
            Session::put('filter_per_page', Session::get('filter_per_page'));
        }


        // set search based on sessions
        $search->category = Session::get('filter_category');
        $search->supplier = Session::get('filter_supplier');
        $search->paging = Session::get('filter_per_page');
        

        Session::save();

        $searchable = $dataType->server_side ? array_keys(SchemaManager::describeTable(app($dataType->model_name)->getTable())->toArray()) : '';
        $orderBy = $request->get('order_by');
        $sortOrder = $request->get('sort_order', null);

        // Next Get or Paginate the actual content from the MODEL that corresponds to the slug DataType
        if (strlen($dataType->model_name) != 0) {

            $relationships = $this->getRelationships($dataType);
            $model = app($dataType->model_name);
            $query = $model::select($dataType->name . '.*')->with($relationships);
            // If a column has a relationship associated with it, we do not want to show that field

            $this->removeRelationshipField($dataType, 'browse');

            if ($search->value) {

                // $search_filter = ($search->filter == 'equals') ? '=' : 'LIKE';
                // $search_value = ($search->filter == 'equals') ? $search->value : '%'.$search->value.'%';

                $query->where(function ($query) use ($dataType, $search) {
                    $query->where($dataType->name . '.name', 'LIKE', '%' . $search->value . '%')
                        ->orWhere($dataType->name . '.barcode_unit', 'LIKE', '%' . $search->value . '%');
                });

            }

            if ($search->supplier) {

                $query->where($dataType->name . '.supplier_id', $search->supplier);
            }

            if ($search->category) {

                $query->join('product_category', 'product_category.product_id', '=', 'products.id');
                $query->join('pcategories', 'pcategories.id', '=', 'product_category.pcategory_id');
                $query->where('pcategories.id', $search->category);
            }

            if ($orderBy && in_array($orderBy, $dataType->fields())) {

                $querySortOrder = (!empty($sortOrder)) ? $sortOrder : 'DESC';

                if ($getter == 'paginate') {
                    $dataTypeContent = $query->orderBy($orderBy, $querySortOrder)->paginate($pagination);
                } else {
                    $dataTypeContent = call_user_func([
                        $query->orderBy($orderBy, $querySortOrder),
                        $getter,
                    ]);
                }
            } elseif ($model->timestamps) {

                if ($getter == 'paginate') {
                    $dataTypeContent = $query->latest($model::CREATED_AT)->paginate($pagination);

                } else {

                    $dataTypeContent = call_user_func([$query->latest($model::CREATED_AT), $getter]);
                }

            } else {

                if ($getter == 'paginate') {

                    $dataTypeContent = $query->orderBy($model->getKeyName(), 'DESC')->paginate($pagination);

                } else {

                    $dataTypeContent = call_user_func([$query->orderBy($model->getKeyName(), 'DESC'), $getter]);
                }

            }

            // Replace relationships' keys for labels and create READ links if a slug is provided.
            $dataTypeContent = $this->resolveRelations($dataTypeContent, $dataType);

        } else {

            // If Model doesn't exist, get data from table name
            $dataTypeContent = call_user_func([DB::table($dataType->name), $getter]);
            $model = false;
        }

        // Check if BREAD is Translatable
        if (($isModelTranslatable = is_bread_translatable($model))) {
            $dataTypeContent->load('translations');
        }

        // Check if server side pagination is enabled
        $isServerSide = isset($dataType->server_side) && $dataType->server_side;

        $view = 'voyager::bread.browse';

        if (view()->exists("voyager::$slug.browse")) {
            $view = "voyager::$slug.browse";
        }

        return Voyager::view($view, compact(
            'dataType',
            'dataTypeContent',
            'isModelTranslatable',
            'search',
            'orderBy',
            'sortOrder',
            'searchable',
            'isServerSide'
        ));
    }

}
