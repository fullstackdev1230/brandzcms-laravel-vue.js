<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Voyager\VoyagerBaseController as BaseVoyagerController;
use Illuminate\Http\Request;
use TCG\Voyager\Facades\Voyager;
use App\Repositories\Layouts\LayoutsRepository;

class LayoutsController extends BaseVoyagerController
{
    //
	protected $layouts;

	public function __construct(LayoutsRepository $layouts)
	{
		$this->layouts = $layouts;
	}


}
