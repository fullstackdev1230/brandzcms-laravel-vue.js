<?php

namespace App\Http\Controllers\Voyager;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use League\Flysystem\Plugin\ListWith;
use Illuminate\Support\Facades\DB;
use TCG\Voyager\Facades\Voyager;
use Spatie\MediaLibrary\Models\Media;
use TCG\Voyager\Http\Controllers\VoyagerMediaController as BaseVoyagerMediaController;

class VoyagerMediaController extends BaseVoyagerMediaController
{
    /** @var string */
    private $filesystem;

    /** @var string */
    private $directory = '';

    public function __construct()
    {
        $this->filesystem = config('voyager.storage.disk');
    }

    public function files(Request $request)
    {
        $folder = $request->folder;

        if ($folder == '/') {
            $folder = '';
        }

        $dir = $this->directory.$folder;

        return response()->json([
            'name'          => 'files',
            'type'          => 'folder',
            'path'          => $dir,
            'folder'        => $folder,
            'items'         => $this->getFiles($dir),
            'last_modified' => 'asdf',
        ]);
    }

    private function getFiles($dir)
    {
        $files = [];
        $storage = Storage::disk($this->filesystem)->addPlugin(new ListWith());
        $storageItems = $storage->listWith(['mimetype'], $dir);

        foreach ($storageItems as $item) {
            if ($item['type'] == 'dir') {
            
	 			$file_url = Storage::disk($this->filesystem)->url($item['basename']);

				$foldername =  $item['basename']; /*Set default name*/
				if(isset($file_url)){
					$id = basename($file_url);

					if(isset($id)){
						$query = DB::table('media')->select('name')->where('id',$id)->get();

						if(isset($query) && count($query) > 0){
							$foldername = $query[0]->name;
						}
					}	
				}

                $files[] = [
                    'name'          => $item['basename'],
                    'type'          => 'folder',
                    'path'          => Storage::disk($this->filesystem)->url($item['path']),
                    'items'         => '',
                    'last_modified' => '',
                    'filename'		=> $foldername,
                ];
            } else {
                if (empty(pathinfo($item['path'], PATHINFO_FILENAME)) && !config('voyager.hidden_files')) {
                    continue;
                }
                $files[] = [
                    'name'          => $item['basename'],
                    'type'          => isset($item['mimetype']) ? $item['mimetype'] : 'file',
                    'path'          => Storage::disk($this->filesystem)->url($item['path']),
                    'size'          => $item['size'],
                    'last_modified' => $item['timestamp'],
                    'filename'		=> $item['basename'],
                ];
            }
        }

        return $files;
    }

    public function remove(Request $request)
    {
        try {
            // GET THE SLUG, ex. 'posts', 'pages', etc.
            $slug = $request->get('slug');

            // GET image name
            $image = $request->get('image');

            $media_id = $request->get('media_id');

            // GET record id
            $id = $request->get('id');

            // GET field name
            $field = $request->get('field');

            // GET THE DataType based on the slug
            $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

            // Check permission
            Voyager::canOrFail('delete_'.$dataType->name);

            // Load model and find record
            $model = app($dataType->model_name);
            $data = $model::find([$id])->first();

            // Check if field exists
            if (!isset($data->{$field})) {
                throw new Exception(__('voyager::generic.field_does_not_exist'), 400);
            }

            // Check if valid json
            if (is_null(@json_decode($data->{$field}))) {
                throw new Exception(__('voyager::json.invalid'), 500);
            }

            // Decode field value
            $fieldData = @json_decode($data->{$field}, true);

            // Flip keys and values
            $fieldData = array_flip($fieldData);

            // Check if image exists in array
            if (!array_key_exists($image, $fieldData)) {
                throw new Exception(__('voyager::media.image_does_not_exist'), 400);
            }

            // Remove image from array
            unset($fieldData[$image]);

            // Generate json and update field
            $data->{$field} = json_encode(array_values(array_flip($fieldData)));
            $data->save();

            //We also remove the image from the media 
            $media = Media::find($media_id);
            $model_type = $media->model_type;
            $model = $model_type::find($media->model_id);
            $model->deleteMedia($media->id);

            return response()->json([
               'data' => [
                   'status'  => 200,
                   'message' => __('voyager::media.image_removed'),
               ],
            ]);
        } catch (Exception $e) {
            $code = 500;
            $message = __('voyager::generic.internal_error');

            if ($e->getCode()) {
                $code = $e->getCode();
            }

            if ($e->getMessage()) {
                $message = $e->getMessage();
            }

            return response()->json([
                'data' => [
                    'status'  => $code,
                    'message' => $message,
                ],
            ], $code);
        }
    }

}
