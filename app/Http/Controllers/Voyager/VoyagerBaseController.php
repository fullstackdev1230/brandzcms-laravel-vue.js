<?php

namespace App\Http\Controllers\Voyager;

use App\Exports\BreadExport;
use App\models\PCategory;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use TCG\Voyager\Facades\Voyager;
use App\Http\Controllers\Voyager\ContentTypes\SimpleMultipleImage;
use TCG\Voyager\Http\Controllers\ContentTypes\Checkbox;
use TCG\Voyager\Http\Controllers\ContentTypes\Coordinates;
use App\Http\Controllers\Voyager\ContentTypes\File;
use TCG\Voyager\Http\Controllers\ContentTypes\Image as ContentImage;
use TCG\Voyager\Http\Controllers\ContentTypes\MultipleImage;
use TCG\Voyager\Http\Controllers\ContentTypes\Password;
use TCG\Voyager\Http\Controllers\ContentTypes\Relationship;
use TCG\Voyager\Http\Controllers\ContentTypes\SelectMultiple;
use TCG\Voyager\Http\Controllers\ContentTypes\Text;
use TCG\Voyager\Http\Controllers\ContentTypes\Timestamp;


use TCG\Voyager\Events\BreadDataDeleted;

use TCG\Voyager\Http\Controllers\VoyagerBaseController as BaseVoyagerBaseController;

class VoyagerBaseController extends BaseVoyagerBaseController
{

	public function export(Request $request)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Export, get IDs from POST
        $ids = explode(',', $request->ids);
        ob_clean();
        return (new BreadExport($dataType->model_name))->whereIn($ids)->download($dataType->name.'.xlsx');
    }

    public function bulkEdit(Request $request)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        $ids = explode(',', $request->ids);

        $column = $request->column;

        switch ($column) {
            case 'pcategory_id':
                $categoryArray = $request->product_belongstomany_category_relationship;

                foreach ($ids as $key => $id) {
                    //Delete from product category first
                    DB::table('product_category')->where('product_id', $id)->delete();

                    //Insert all items to product category
                    if($categoryArray){
                        foreach ($categoryArray as $key => $pcategory_id) {
                                if($pcategory_id){
                                    DB::table('product_category')->insert([
                                        'product_id' => $id,
                                        'pcategory_id' => $pcategory_id,
                                        'updated_at' => Carbon::now()->toDateTimeString(),
                                        'created_at' => Carbon::now()->toDateTimeString()
                                    ]);
                                }
                        }
                    }
                }
                break;
            case 'bcategory_id':
                $categoryArray = $request->brochure_belongstomany_category_relationship;

                foreach ($ids as $key => $id) {
                    //Delete from product category first
                    DB::table('brochure_category')->where('brochure_id', $id)->delete();

                    //Insert all items to product category
                    if($categoryArray){
                        foreach ($categoryArray as $key => $bcategory_id) {
                                if($bcategory_id){
                                    DB::table('brochure_category')->insert([
                                        'brochure_id' => $id,
                                        'bcategory_id' => $bcategory_id,
                                        'updated_at' => Carbon::now()->toDateTimeString(),
                                        'created_at' => Carbon::now()->toDateTimeString()
                                    ]);
                                }
                        }
                    }
                }
                break;
            default:
                foreach ($ids as $key => $id) {
                    $dataType->model_name::where('id', $id)
                                  ->update([
                                  $column => $request->get($column)
                                  ]);
                }
                break;
        }

        return redirect()
            ->route("voyager.{$dataType->slug}.index")
            ->with([
                'message'    => __('voyager::generic.successfully_updated')." {$dataType->display_name_plural}",
                'alert-type' => 'success',
            ]);
    }

    /**
     * Overriding the existing getContentBasedOnType method of Voyager
     * @param  Request $request Request object
     * @param  String $slug    Slug
     * @param  String $rows    Rows
     * @param  Array $options Options
     * @return Object Content
     */
	public function getContentBasedOnType(Request $request, $slug, $row, $options = null)
	{
		switch ($row->type) {

		    /********** PASSWORD TYPE **********/
		    case 'password':
		        return (new Password($request, $slug, $row, $options))->handle();

		    /********** CHECKBOX TYPE **********/
		    case 'checkbox':
		        return (new Checkbox($request, $slug, $row, $options))->handle();

		    /********** FILE TYPE **********/
		    case 'file':
		        return (new File($request, $slug, $row, $options))->handle();

		    /********** MULTIPLE IMAGES TYPE **********/
		    case 'multiple_images':
		        return (new SimpleMultipleImage($request, $slug, $row, $options))->handle();

		    /********** SELECT MULTIPLE TYPE **********/
		    case 'select_multiple':
		        return (new SelectMultiple($request, $slug, $row, $options))->handle();

		    /********** IMAGE TYPE **********/
		    case 'image':
		        return (new ContentImage($request, $slug, $row, $options))->handle();

		    /********** TIMESTAMP TYPE **********/
		    case 'timestamp':
		        return (new Timestamp($request, $slug, $row, $options))->handle();

		    /********** COORDINATES TYPE **********/
		    case 'coordinates':
		        return (new Coordinates($request, $slug, $row, $options))->handle();

		    /********** RELATIONSHIPS TYPE **********/
		    case 'relationship':
		        return (new Relationship($request, $slug, $row, $options))->handle();

		    /********** ALL OTHER TEXT TYPE **********/
		    default:
		        return (new Text($request, $slug, $row, $options))->handle();
		}
	}


    /**
     * Overriding the insertUpdateData method of Voyager
     * @param  Request $request Request object
     * @param  String $slug    Slug
     * @param  String $rows    Rows
     * @param  String $data    Data
     * @return Object          Data
     */
    public function insertUpdateData($request, $slug, $rows, $data)
    {
        $multi_select = [];

        /*
         * Prepare Translations and Transform data
         */
        $translations = is_bread_translatable($data)
                        ? $data->prepareTranslations($request)
                        : [];

        foreach ($rows as $row) {
            $options = json_decode($row->details);

            // if the field for this row is absent from the request, continue
            // checkboxes will be absent when unchecked, thus they are the exception
            if (!$request->hasFile($row->field) && !$request->has($row->field) && $row->type !== 'checkbox') {
                // if the field is a belongsToMany relationship, don't remove it
                // if no content is provided, that means the relationships need to be removed
                if ((isset($options->type) && $options->type !== 'belongsToMany')) {
                    continue;
                }
            }

            $content = $this->getContentBasedOnType($request, $slug, $row, $options);
            
            if ($row->type == 'relationship' && $options->type != 'belongsToMany') {
                $row->field = @$options->column;
            }

            /*
             * merge existing images and upload images
             */
            if ($row->type == 'multiple_images' && !is_null($content)) {
                if (isset($data->{$row->field})) {
                    $ex_files = json_decode($data->{$row->field}, true);
                    if (!is_null($ex_files)) {
                        $content = json_encode(array_merge($ex_files, json_decode($content)));
                    }
                }
            }

            if (is_null($content)) {

                // If the image upload is null and it has a current image keep the current image
                if ($row->type == 'image' && is_null($request->input($row->field)) && isset($data->{$row->field})) {
                    $content = $data->{$row->field};
                }

                // If the multiple_images upload is null and it has a current image keep the current image
                if ($row->type == 'multiple_images' && is_null($request->input($row->field)) && isset($data->{$row->field})) {
                    $content = $data->{$row->field};
                }

                // If the file upload is null and it has a current file keep the current file
                if ($row->type == 'file') {
                    $content = $data->{$row->field};
                }

                if ($row->type == 'password') {
                    $content = $data->{$row->field};
                }
            }

            //Handle empty file return []
            if ($row->type == 'file') {
                $decoded_content = json_decode($content);
                if(empty($decoded_content)){
                    // If the file upload is null and it has a current file keep the current file
                    $content = $data->{$row->field};
                }
            }

            if ($row->type == 'relationship' && $options->type == 'belongsToMany') {
                // Only if select_multiple is working with a relationship
                $multi_select[] = ['model' => $options->model, 'content' => $content, 'table' => $options->pivot_table];
            } else {
                $data->{$row->field} = $content;
            }
        }

        $data->save();

        // Save Spatie Media
        foreach ($rows as $row) {

        	if($row->type == 'multiple_images') {
				if (!is_null($data->{$row->field}) && $request->hasFile($row->field) ) {

					$data->addMultipleMediaFromRequest([$row->field])
						->each( function ($fileAdder) use ($row) {
							$fileAdder->toMediaCollection($row->field);
						});

                    // Get Images and replace the data saved in the image field of the model

                    $image_array = [];
                    $images = $data->getMedia($row->field);

                    foreach ($images as $i) {
                        $image_array[] = $i->getFullUrl('thumb');
                    }

                    $data->{$row->field} = json_encode($image_array);
                    $data->save();
				}



        	} elseif($row->type == 'image') {
                if (!is_null($data->{$row->field}) && $request->hasFile($row->field) ) {

                    // Delete existing media collection related to this record
                    $data->clearMediaCollection($row->field);

                    // Add Media
                    $data->addMediaFromRequest($row->field)->toMediaCollection($row->field);

                    // Get Images and replace the data saved in the image field of the model
                    $image_array = [];
                    $images = $data->getMedia($row->field);

                    $data->{$row->field} = $images[0]->getFullUrl('thumb');
                    $data->save();
                }

            }
        }

        // Save translations
        if (count($translations) > 0) {
            $data->saveTranslations($translations);
        }

        foreach ($multi_select as $sync_data) {
            if(!is_null($sync_data['content'])){

                //for brochures only
                if($sync_data['table'] == 'brochure_group'  || $sync_data['table'] == 'group_survey'
                    || $sync_data['table'] == 'notification_group' || $sync_data['table'] == 'layout_group'){

                    if (in_array("all", $sync_data['content'])){
                        $group_ids = $sync_data['model']::pluck('id')->toArray();
                        $data->belongsToMany($sync_data['model'], $sync_data['table'])->sync($group_ids);
                    }else{
                        $group_types = $sync_data['model']::select('user_type')->distinct()->pluck('user_type')->toArray();
                        $added_id = [];

                        //We loop through all the distinct user types
                        foreach ($sync_data['content'] as $synckey => $syncval) {

                            //If curreny sync value an id or a user type
                            if(in_array($syncval,$group_types)){
                                //We will remove selected user type in the array
                                unset($sync_data['content'][$synckey]);

                                //We collect the IDs of the groups on the specific user type
                                $current_type_ids = $sync_data['model']::where('user_type',$syncval)->pluck('id')->toArray();

                                foreach ($current_type_ids as $id_val) {
                                    //We add all the groups with the user type name match
                                    array_push($added_id,$id_val);

                                    //We also remove selected ID individually if already part of a selected user type
                                    $id_del = array_search($id_val,$sync_data['content']);
                                    unset($sync_data['content'][$id_del]);
                                }
                            }else{
                                array_push($added_id,$syncval);
                            }

                        }

                       $data->belongsToMany($sync_data['model'], $sync_data['table'])->sync($added_id);
                    }

                }else{
                    if (in_array("all", $sync_data['content'])){
                        $current_model = $sync_data['model'];
                        $group_ids = $current_model::pluck('id')->toArray();
                        $data->belongsToMany($sync_data['model'], $sync_data['table'])->sync($group_ids);
                    }else{
                        $data->belongsToMany($sync_data['model'], $sync_data['table'])->sync($sync_data['content']);
                    }
                }



            }
        }

        return $data;
    }

    public function destroy(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('delete', app($dataType->model_name));

        // Init array of IDs
        $ids = [];
        if (empty($id)) {
            // Bulk delete, get IDs from POST
            $ids = explode(',', $request->ids);
        } else {
            // Single item delete, get ID from URL
            $ids[] = $id;
        }
        foreach ($ids as $id) {
            $data = call_user_func([$dataType->model_name, 'findOrFail'], $id);
            $this->cleanup($dataType, $data);
        }

        $displayName = count($ids) > 1 ? $dataType->display_name_plural : $dataType->display_name_singular;

        DB::beginTransaction();

        try {
            $res = $data->destroy($ids);
            $data = $res
                ? [
                    'message'    => __('voyager::generic.successfully_deleted')." {$displayName}",
                    'alert-type' => 'success',
                ]
                : [
                    'message'    => __('voyager::generic.error_deleting')." {$displayName}",
                    'alert-type' => 'error',
                ];

            if ($res) {
                event(new BreadDataDeleted($dataType, $data));
            }
        }catch (\Illuminate\Database\QueryException $e) {

            DB::rollback();
            //__('voyager::generic.error_deleting')." {$displayName}"
            $return_data = ['message'    => 'Cannot delete selected '.$displayName.' while there are records connected.',
                            'alert-type' => 'error'];

            return redirect()
            ->route("voyager.{$dataType->slug}.index")
            ->with($return_data);

        }catch(\Exception $e){
            DB::rollback();
            throw $e;
        }

        DB::commit();

        if($request->input('del_location') == 'dashboard'){
            return redirect('admin')->with($data);
        }else{
            return redirect()->route("voyager.{$dataType->slug}.index")->with($data);
        }


    }

}
