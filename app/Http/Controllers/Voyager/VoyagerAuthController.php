<?php

namespace App\Http\Controllers\Voyager;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use TCG\Voyager\Facades\Voyager;
use Illuminate\Support\Facades\DB;
use App\Models\Group;
use App\Models\User;
use Carbon\Carbon;


use TCG\Voyager\Http\Controllers\VoyagerAuthController as BaseVoyagerAuthController;

class VoyagerAuthController extends BaseVoyagerAuthController
{
    use AuthenticatesUsers;

    public function login()
    {
        if (Auth::user()) {
           // return redirect()->route('voyager.dashboard');
        }

        return Voyager::view('voyager::login');
    }

    public function postLogin(Request $request)
    {	
        $this->validateLogin($request);
        $errors = [];
        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        $credentials = $this->credentials($request);

        if ($this->guard()->attempt($credentials, $request->has('remember'))) {

        	//user subscription expiry validation
	        $today = date(Carbon::now()->format('Y-m-d'));

            $group_id = User::where('email', $request->email)->first()->group_id;
            $group_expiry = Group::where('id', $group_id)->first()->subscription_expiry;

            if($group_expiry!=null){
                if ($group_expiry <= $today){
                    Auth::logout();
                    throw ValidationException::withMessages([
                    $this->username() => 'Group Subscription Expired']);
                    //return Voyager::view('voyager::login')->with('errors' => $errors);
                }
            }


	        $expiry = date(User::where('email', $request->email)->first()->subscription_expiry);

	        if($expiry!=null){
		        if ($expiry <= $today){
                    Auth::logout();
		        	throw ValidationException::withMessages([
	            	$this->username() => 'User Subscription Expired']);
		        	//return Voyager::view('voyager::login')->with('errors' => $errors);
		        }
	        }

            return $this->sendLoginResponse($request);
        }


        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    /*
     * Preempts $redirectTo member variable (from RedirectsUsers trait)
     */
    public function redirectTo()
    {
        return config('voyager.user.redirect', route('voyager.dashboard'));
    }
}
