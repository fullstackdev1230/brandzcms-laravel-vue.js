<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use DB;

class BreadExport implements FromQuery, WithHeadings
{

	use Exportable;

	protected $model;
	protected $ids;

	public function __construct($model)
	{
		$this->model = $model;
	}

	public function headings(): array
	{
		return DB::connection()->getSchemaBuilder()->getColumnListing((new $this->model)->getTable());
	}

	public function whereIn(array $ids)
	{
		$this->ids = $ids;

		return $this;
	}

	/**
	 * From Query
	 * @return Query Query object
	 */
    public function query()
    {
        return $this->model::query()->whereIn('id',$this->ids);
    }
}
