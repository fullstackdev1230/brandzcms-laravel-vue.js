<?php

use Illuminate\Database\Seeder;

class MenusTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('menus')->delete();
        
        \DB::table('menus')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'admin',
                'created_at' => '2018-08-14 17:58:18',
                'updated_at' => '2018-08-14 17:58:18',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'badmin',
                'created_at' => '2018-09-19 05:33:59',
                'updated_at' => '2018-09-19 05:33:59',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'ww',
                'created_at' => '2018-11-09 03:11:19',
                'updated_at' => '2018-11-09 03:52:36',
            ),
        ));
        
        
    }
}