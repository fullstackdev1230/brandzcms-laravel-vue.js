<?php

use Illuminate\Database\Seeder;

class DataTypesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('data_types')->delete();
        
        \DB::table('data_types')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'users',
                'slug' => 'users',
                'display_name_singular' => 'User',
                'display_name_plural' => 'Users',
                'icon' => 'voyager-person',
                'model_name' => 'App\\Models\\User',
                'policy_name' => 'TCG\\Voyager\\Policies\\UserPolicy',
                'controller' => NULL,
                'description' => NULL,
                'generate_permissions' => 1,
                'server_side' => 0,
                'details' => '{"order_column":null,"order_display_column":null}',
                'created_at' => '2018-08-14 17:58:18',
                'updated_at' => '2018-10-11 04:40:59',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'menus',
                'slug' => 'menus',
                'display_name_singular' => 'Menu',
                'display_name_plural' => 'Menus',
                'icon' => 'voyager-list',
                'model_name' => 'TCG\\Voyager\\Models\\Menu',
                'policy_name' => NULL,
                'controller' => '',
                'description' => '',
                'generate_permissions' => 1,
                'server_side' => 0,
                'details' => NULL,
                'created_at' => '2018-08-14 17:58:18',
                'updated_at' => '2018-08-14 17:58:18',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'roles',
                'slug' => 'roles',
                'display_name_singular' => 'Role',
                'display_name_plural' => 'Roles',
                'icon' => 'voyager-lock',
                'model_name' => 'TCG\\Voyager\\Models\\Role',
                'policy_name' => NULL,
                'controller' => '',
                'description' => '',
                'generate_permissions' => 1,
                'server_side' => 0,
                'details' => NULL,
                'created_at' => '2018-08-14 17:58:18',
                'updated_at' => '2018-08-14 17:58:18',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'categories',
                'slug' => 'categories',
                'display_name_singular' => 'Category',
                'display_name_plural' => 'Categories',
                'icon' => 'voyager-categories',
                'model_name' => 'TCG\\Voyager\\Models\\Category',
                'policy_name' => NULL,
                'controller' => NULL,
                'description' => NULL,
                'generate_permissions' => 1,
                'server_side' => 0,
                'details' => '{"order_column":null,"order_display_column":null}',
                'created_at' => '2018-08-14 17:58:19',
                'updated_at' => '2018-10-17 15:09:13',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'posts',
                'slug' => 'posts',
                'display_name_singular' => 'Post',
                'display_name_plural' => 'Posts',
                'icon' => 'voyager-news',
                'model_name' => 'TCG\\Voyager\\Models\\Post',
                'policy_name' => 'TCG\\Voyager\\Policies\\PostPolicy',
                'controller' => NULL,
                'description' => NULL,
                'generate_permissions' => 1,
                'server_side' => 0,
                'details' => '{"order_column":null,"order_display_column":null}',
                'created_at' => '2018-08-14 17:58:20',
                'updated_at' => '2018-08-19 19:44:39',
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'pages',
                'slug' => 'pages',
                'display_name_singular' => 'Page',
                'display_name_plural' => 'Pages',
                'icon' => 'voyager-file-text',
                'model_name' => 'TCG\\Voyager\\Models\\Page',
                'policy_name' => NULL,
                'controller' => NULL,
                'description' => NULL,
                'generate_permissions' => 1,
                'server_side' => 0,
                'details' => '{"order_column":null,"order_display_column":null}',
                'created_at' => '2018-08-14 17:58:20',
                'updated_at' => '2018-10-15 20:20:41',
            ),
            6 => 
            array (
                'id' => 11,
                'name' => 'products',
                'slug' => 'products',
                'display_name_singular' => 'Product',
                'display_name_plural' => 'Products',
                'icon' => 'voyager-gift',
                'model_name' => 'App\\Models\\Product',
                'policy_name' => NULL,
                'controller' => 'ProductsController',
                'description' => NULL,
                'generate_permissions' => 1,
                'server_side' => 1,
                'details' => '{"order_column":"id","order_display_column":null}',
                'created_at' => '2018-08-19 15:40:02',
                'updated_at' => '2018-10-31 14:51:24',
            ),
            7 => 
            array (
                'id' => 12,
                'name' => 'suppliers',
                'slug' => 'suppliers',
                'display_name_singular' => 'Supplier',
                'display_name_plural' => 'Suppliers',
                'icon' => 'voyager-group',
                'model_name' => 'App\\Models\\Supplier',
                'policy_name' => NULL,
                'controller' => NULL,
                'description' => NULL,
                'generate_permissions' => 1,
                'server_side' => 0,
                'details' => '{"order_column":"name","order_display_column":null}',
                'created_at' => '2018-08-19 18:34:57',
                'updated_at' => '2018-10-23 22:26:34',
            ),
            8 => 
            array (
                'id' => 13,
                'name' => 'brochures',
                'slug' => 'brochures',
                'display_name_singular' => 'Brochure',
                'display_name_plural' => 'Brochures',
                'icon' => 'voyager-logbook',
                'model_name' => 'App\\Models\\Brochure',
                'policy_name' => NULL,
                'controller' => 'BrochuresController',
                'description' => NULL,
                'generate_permissions' => 1,
                'server_side' => 0,
                'details' => '{"order_column":null,"order_display_column":null}',
                'created_at' => '2018-08-20 04:54:23',
                'updated_at' => '2018-09-27 12:26:00',
            ),
            9 => 
            array (
                'id' => 14,
                'name' => 'groups',
                'slug' => 'groups',
                'display_name_singular' => 'Group',
                'display_name_plural' => 'Groups',
                'icon' => NULL,
                'model_name' => 'App\\Models\\Group',
                'policy_name' => NULL,
                'controller' => NULL,
                'description' => 'User Groups',
                'generate_permissions' => 1,
                'server_side' => 0,
                'details' => '{"order_column":null,"order_display_column":"id"}',
                'created_at' => '2018-08-25 16:12:38',
                'updated_at' => '2018-09-19 05:25:30',
            ),
            10 => 
            array (
                'id' => 15,
                'name' => 'notifications',
                'slug' => 'notifications',
                'display_name_singular' => 'Notification',
                'display_name_plural' => 'Notifications',
                'icon' => 'voyager-chat',
                'model_name' => 'App\\Models\\Notification',
                'policy_name' => NULL,
                'controller' => NULL,
                'description' => 'Notifications',
                'generate_permissions' => 1,
                'server_side' => 0,
                'details' => '{"order_column":null,"order_display_column":null}',
                'created_at' => '2018-08-26 01:11:48',
                'updated_at' => '2018-09-16 03:15:11',
            ),
            11 => 
            array (
                'id' => 17,
                'name' => 'pcategories',
                'slug' => 'pcategories',
                'display_name_singular' => 'Product Category',
                'display_name_plural' => 'Product Categories',
                'icon' => 'voyager-categories',
                'model_name' => 'App\\Models\\Pcategory',
                'policy_name' => NULL,
                'controller' => NULL,
                'description' => NULL,
                'generate_permissions' => 1,
                'server_side' => 0,
                'details' => '{"order_column":null,"order_display_column":null}',
                'created_at' => '2018-09-18 23:05:38',
                'updated_at' => '2018-09-19 04:17:38',
            ),
            12 => 
            array (
                'id' => 18,
                'name' => 'bcategories',
                'slug' => 'bcategories',
                'display_name_singular' => 'Brochure Category',
                'display_name_plural' => 'Brochure Categories',
                'icon' => 'voyager-categories',
                'model_name' => 'App\\Models\\Bcategory',
                'policy_name' => NULL,
                'controller' => NULL,
                'description' => NULL,
                'generate_permissions' => 1,
                'server_side' => 0,
                'details' => '{"order_column":null,"order_display_column":null}',
                'created_at' => '2018-09-19 03:41:22',
                'updated_at' => '2018-09-19 03:42:11',
            ),
            13 => 
            array (
                'id' => 21,
                'name' => 'surveys',
                'slug' => 'surveys',
                'display_name_singular' => 'Survey',
                'display_name_plural' => 'Surveys',
                'icon' => 'voyager-pie-chart',
                'model_name' => 'App\\Models\\Survey',
                'policy_name' => NULL,
                'controller' => 'SurveysController',
                'description' => NULL,
                'generate_permissions' => 1,
                'server_side' => 0,
                'details' => '{"order_column":null,"order_display_column":null}',
                'created_at' => '2018-10-14 13:01:55',
                'updated_at' => '2018-10-14 13:10:56',
            ),
            14 => 
            array (
                'id' => 22,
                'name' => 'layouts',
                'slug' => 'layouts',
                'display_name_singular' => 'Brandz App Home Screen Layout',
                'display_name_plural' => 'Layouts',
                'icon' => 'voyager-phone',
                'model_name' => 'App\\Models\\Layout',
                'policy_name' => NULL,
                'controller' => NULL,
                'description' => NULL,
                'generate_permissions' => 1,
                'server_side' => 0,
                'details' => '{"order_column":null,"order_display_column":null}',
                'created_at' => '2018-10-15 13:23:56',
                'updated_at' => '2018-10-15 15:27:36',
            ),
            15 => 
            array (
                'id' => 23,
                'name' => 'layout_image_links',
                'slug' => 'layout-image-links',
                'display_name_singular' => 'Home Screen Widget',
                'display_name_plural' => 'Home Screen Widget',
                'icon' => NULL,
                'model_name' => 'App\\Models\\LayoutImageLink',
                'policy_name' => NULL,
                'controller' => 'LayoutImageLinksController',
                'description' => NULL,
                'generate_permissions' => 1,
                'server_side' => 0,
                'details' => '{"order_column":null,"order_display_column":null}',
                'created_at' => '2018-10-17 20:03:28',
                'updated_at' => '2018-10-17 20:30:20',
            ),
        ));
        
        
    }
}