<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        
        \DB::statement('SET FOREIGN_KEY_CHECKS=0');

        \DB::table('roles')->delete();
        
        \DB::table('roles')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'admin',
                'display_name' => 'Administrator',
                'created_at' => '2018-08-14 07:58:19',
                'updated_at' => '2018-08-14 07:58:19',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'user',
                'display_name' => 'Normal User',
                'created_at' => '2018-08-14 07:58:19',
                'updated_at' => '2018-08-14 07:58:19',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'badmin',
                'display_name' => 'Brandz Administrator',
                'created_at' => '2018-09-18 19:32:19',
                'updated_at' => '2018-09-18 19:32:42',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'ww',
                'display_name' => 'Widget World',
                'created_at' => '2018-11-09 02:12:49',
                'updated_at' => '2018-11-09 02:48:02',
            ),
        ));
        
        
    }
}