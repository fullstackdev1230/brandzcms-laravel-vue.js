<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('permissions')->delete();
        
        \DB::table('permissions')->insert(array (
            0 => 
            array (
                'id' => 1,
                'key' => 'browse_admin',
                'table_name' => NULL,
                'created_at' => '2018-08-14 17:58:19',
                'updated_at' => '2018-08-14 17:58:19',
            ),
            1 => 
            array (
                'id' => 2,
                'key' => 'browse_bread',
                'table_name' => NULL,
                'created_at' => '2018-08-14 17:58:19',
                'updated_at' => '2018-08-14 17:58:19',
            ),
            2 => 
            array (
                'id' => 3,
                'key' => 'browse_database',
                'table_name' => NULL,
                'created_at' => '2018-08-14 17:58:19',
                'updated_at' => '2018-08-14 17:58:19',
            ),
            3 => 
            array (
                'id' => 4,
                'key' => 'browse_media',
                'table_name' => NULL,
                'created_at' => '2018-08-14 17:58:19',
                'updated_at' => '2018-08-14 17:58:19',
            ),
            4 => 
            array (
                'id' => 5,
                'key' => 'browse_compass',
                'table_name' => NULL,
                'created_at' => '2018-08-14 17:58:19',
                'updated_at' => '2018-08-14 17:58:19',
            ),
            5 => 
            array (
                'id' => 6,
                'key' => 'browse_menus',
                'table_name' => 'menus',
                'created_at' => '2018-08-14 17:58:19',
                'updated_at' => '2018-08-14 17:58:19',
            ),
            6 => 
            array (
                'id' => 7,
                'key' => 'read_menus',
                'table_name' => 'menus',
                'created_at' => '2018-08-14 17:58:19',
                'updated_at' => '2018-08-14 17:58:19',
            ),
            7 => 
            array (
                'id' => 8,
                'key' => 'edit_menus',
                'table_name' => 'menus',
                'created_at' => '2018-08-14 17:58:19',
                'updated_at' => '2018-08-14 17:58:19',
            ),
            8 => 
            array (
                'id' => 9,
                'key' => 'add_menus',
                'table_name' => 'menus',
                'created_at' => '2018-08-14 17:58:19',
                'updated_at' => '2018-08-14 17:58:19',
            ),
            9 => 
            array (
                'id' => 10,
                'key' => 'delete_menus',
                'table_name' => 'menus',
                'created_at' => '2018-08-14 17:58:19',
                'updated_at' => '2018-08-14 17:58:19',
            ),
            10 => 
            array (
                'id' => 11,
                'key' => 'browse_roles',
                'table_name' => 'roles',
                'created_at' => '2018-08-14 17:58:19',
                'updated_at' => '2018-08-14 17:58:19',
            ),
            11 => 
            array (
                'id' => 12,
                'key' => 'read_roles',
                'table_name' => 'roles',
                'created_at' => '2018-08-14 17:58:19',
                'updated_at' => '2018-08-14 17:58:19',
            ),
            12 => 
            array (
                'id' => 13,
                'key' => 'edit_roles',
                'table_name' => 'roles',
                'created_at' => '2018-08-14 17:58:19',
                'updated_at' => '2018-08-14 17:58:19',
            ),
            13 => 
            array (
                'id' => 14,
                'key' => 'add_roles',
                'table_name' => 'roles',
                'created_at' => '2018-08-14 17:58:19',
                'updated_at' => '2018-08-14 17:58:19',
            ),
            14 => 
            array (
                'id' => 15,
                'key' => 'delete_roles',
                'table_name' => 'roles',
                'created_at' => '2018-08-14 17:58:19',
                'updated_at' => '2018-08-14 17:58:19',
            ),
            15 => 
            array (
                'id' => 16,
                'key' => 'browse_users',
                'table_name' => 'users',
                'created_at' => '2018-08-14 17:58:19',
                'updated_at' => '2018-08-14 17:58:19',
            ),
            16 => 
            array (
                'id' => 17,
                'key' => 'read_users',
                'table_name' => 'users',
                'created_at' => '2018-08-14 17:58:19',
                'updated_at' => '2018-08-14 17:58:19',
            ),
            17 => 
            array (
                'id' => 18,
                'key' => 'edit_users',
                'table_name' => 'users',
                'created_at' => '2018-08-14 17:58:19',
                'updated_at' => '2018-08-14 17:58:19',
            ),
            18 => 
            array (
                'id' => 19,
                'key' => 'add_users',
                'table_name' => 'users',
                'created_at' => '2018-08-14 17:58:19',
                'updated_at' => '2018-08-14 17:58:19',
            ),
            19 => 
            array (
                'id' => 20,
                'key' => 'delete_users',
                'table_name' => 'users',
                'created_at' => '2018-08-14 17:58:19',
                'updated_at' => '2018-08-14 17:58:19',
            ),
            20 => 
            array (
                'id' => 21,
                'key' => 'browse_settings',
                'table_name' => 'settings',
                'created_at' => '2018-08-14 17:58:19',
                'updated_at' => '2018-08-14 17:58:19',
            ),
            21 => 
            array (
                'id' => 22,
                'key' => 'read_settings',
                'table_name' => 'settings',
                'created_at' => '2018-08-14 17:58:19',
                'updated_at' => '2018-08-14 17:58:19',
            ),
            22 => 
            array (
                'id' => 23,
                'key' => 'edit_settings',
                'table_name' => 'settings',
                'created_at' => '2018-08-14 17:58:19',
                'updated_at' => '2018-08-14 17:58:19',
            ),
            23 => 
            array (
                'id' => 24,
                'key' => 'add_settings',
                'table_name' => 'settings',
                'created_at' => '2018-08-14 17:58:19',
                'updated_at' => '2018-08-14 17:58:19',
            ),
            24 => 
            array (
                'id' => 25,
                'key' => 'delete_settings',
                'table_name' => 'settings',
                'created_at' => '2018-08-14 17:58:19',
                'updated_at' => '2018-08-14 17:58:19',
            ),
            25 => 
            array (
                'id' => 26,
                'key' => 'browse_categories',
                'table_name' => 'categories',
                'created_at' => '2018-08-14 17:58:20',
                'updated_at' => '2018-08-14 17:58:20',
            ),
            26 => 
            array (
                'id' => 27,
                'key' => 'read_categories',
                'table_name' => 'categories',
                'created_at' => '2018-08-14 17:58:20',
                'updated_at' => '2018-08-14 17:58:20',
            ),
            27 => 
            array (
                'id' => 28,
                'key' => 'edit_categories',
                'table_name' => 'categories',
                'created_at' => '2018-08-14 17:58:20',
                'updated_at' => '2018-08-14 17:58:20',
            ),
            28 => 
            array (
                'id' => 29,
                'key' => 'add_categories',
                'table_name' => 'categories',
                'created_at' => '2018-08-14 17:58:20',
                'updated_at' => '2018-08-14 17:58:20',
            ),
            29 => 
            array (
                'id' => 30,
                'key' => 'delete_categories',
                'table_name' => 'categories',
                'created_at' => '2018-08-14 17:58:20',
                'updated_at' => '2018-08-14 17:58:20',
            ),
            30 => 
            array (
                'id' => 31,
                'key' => 'browse_posts',
                'table_name' => 'posts',
                'created_at' => '2018-08-14 17:58:20',
                'updated_at' => '2018-08-14 17:58:20',
            ),
            31 => 
            array (
                'id' => 32,
                'key' => 'read_posts',
                'table_name' => 'posts',
                'created_at' => '2018-08-14 17:58:20',
                'updated_at' => '2018-08-14 17:58:20',
            ),
            32 => 
            array (
                'id' => 33,
                'key' => 'edit_posts',
                'table_name' => 'posts',
                'created_at' => '2018-08-14 17:58:20',
                'updated_at' => '2018-08-14 17:58:20',
            ),
            33 => 
            array (
                'id' => 34,
                'key' => 'add_posts',
                'table_name' => 'posts',
                'created_at' => '2018-08-14 17:58:20',
                'updated_at' => '2018-08-14 17:58:20',
            ),
            34 => 
            array (
                'id' => 35,
                'key' => 'delete_posts',
                'table_name' => 'posts',
                'created_at' => '2018-08-14 17:58:20',
                'updated_at' => '2018-08-14 17:58:20',
            ),
            35 => 
            array (
                'id' => 36,
                'key' => 'browse_pages',
                'table_name' => 'pages',
                'created_at' => '2018-08-14 17:58:20',
                'updated_at' => '2018-08-14 17:58:20',
            ),
            36 => 
            array (
                'id' => 37,
                'key' => 'read_pages',
                'table_name' => 'pages',
                'created_at' => '2018-08-14 17:58:20',
                'updated_at' => '2018-08-14 17:58:20',
            ),
            37 => 
            array (
                'id' => 38,
                'key' => 'edit_pages',
                'table_name' => 'pages',
                'created_at' => '2018-08-14 17:58:20',
                'updated_at' => '2018-08-14 17:58:20',
            ),
            38 => 
            array (
                'id' => 39,
                'key' => 'add_pages',
                'table_name' => 'pages',
                'created_at' => '2018-08-14 17:58:20',
                'updated_at' => '2018-08-14 17:58:20',
            ),
            39 => 
            array (
                'id' => 40,
                'key' => 'delete_pages',
                'table_name' => 'pages',
                'created_at' => '2018-08-14 17:58:20',
                'updated_at' => '2018-08-14 17:58:20',
            ),
            40 => 
            array (
                'id' => 41,
                'key' => 'browse_hooks',
                'table_name' => NULL,
                'created_at' => '2018-08-14 17:58:20',
                'updated_at' => '2018-08-14 17:58:20',
            ),
            41 => 
            array (
                'id' => 42,
                'key' => 'browse_products',
                'table_name' => 'products',
                'created_at' => '2018-08-19 15:35:35',
                'updated_at' => '2018-08-19 15:35:35',
            ),
            42 => 
            array (
                'id' => 43,
                'key' => 'read_products',
                'table_name' => 'products',
                'created_at' => '2018-08-19 15:35:35',
                'updated_at' => '2018-08-19 15:35:35',
            ),
            43 => 
            array (
                'id' => 44,
                'key' => 'edit_products',
                'table_name' => 'products',
                'created_at' => '2018-08-19 15:35:35',
                'updated_at' => '2018-08-19 15:35:35',
            ),
            44 => 
            array (
                'id' => 45,
                'key' => 'add_products',
                'table_name' => 'products',
                'created_at' => '2018-08-19 15:35:35',
                'updated_at' => '2018-08-19 15:35:35',
            ),
            45 => 
            array (
                'id' => 46,
                'key' => 'delete_products',
                'table_name' => 'products',
                'created_at' => '2018-08-19 15:35:35',
                'updated_at' => '2018-08-19 15:35:35',
            ),
            46 => 
            array (
                'id' => 47,
                'key' => 'browse_suppliers',
                'table_name' => 'suppliers',
                'created_at' => '2018-08-19 18:34:57',
                'updated_at' => '2018-08-19 18:34:57',
            ),
            47 => 
            array (
                'id' => 48,
                'key' => 'read_suppliers',
                'table_name' => 'suppliers',
                'created_at' => '2018-08-19 18:34:57',
                'updated_at' => '2018-08-19 18:34:57',
            ),
            48 => 
            array (
                'id' => 49,
                'key' => 'edit_suppliers',
                'table_name' => 'suppliers',
                'created_at' => '2018-08-19 18:34:57',
                'updated_at' => '2018-08-19 18:34:57',
            ),
            49 => 
            array (
                'id' => 50,
                'key' => 'add_suppliers',
                'table_name' => 'suppliers',
                'created_at' => '2018-08-19 18:34:57',
                'updated_at' => '2018-08-19 18:34:57',
            ),
            50 => 
            array (
                'id' => 51,
                'key' => 'delete_suppliers',
                'table_name' => 'suppliers',
                'created_at' => '2018-08-19 18:34:57',
                'updated_at' => '2018-08-19 18:34:57',
            ),
            51 => 
            array (
                'id' => 52,
                'key' => 'browse_brochures',
                'table_name' => 'brochures',
                'created_at' => '2018-08-20 04:54:23',
                'updated_at' => '2018-08-20 04:54:23',
            ),
            52 => 
            array (
                'id' => 53,
                'key' => 'read_brochures',
                'table_name' => 'brochures',
                'created_at' => '2018-08-20 04:54:23',
                'updated_at' => '2018-08-20 04:54:23',
            ),
            53 => 
            array (
                'id' => 54,
                'key' => 'edit_brochures',
                'table_name' => 'brochures',
                'created_at' => '2018-08-20 04:54:23',
                'updated_at' => '2018-08-20 04:54:23',
            ),
            54 => 
            array (
                'id' => 55,
                'key' => 'add_brochures',
                'table_name' => 'brochures',
                'created_at' => '2018-08-20 04:54:23',
                'updated_at' => '2018-08-20 04:54:23',
            ),
            55 => 
            array (
                'id' => 56,
                'key' => 'delete_brochures',
                'table_name' => 'brochures',
                'created_at' => '2018-08-20 04:54:23',
                'updated_at' => '2018-08-20 04:54:23',
            ),
            56 => 
            array (
                'id' => 57,
                'key' => 'browse_polls',
                'table_name' => 'polls',
                'created_at' => '2018-08-20 05:19:38',
                'updated_at' => '2018-08-20 05:19:38',
            ),
            57 => 
            array (
                'id' => 58,
                'key' => 'read_polls',
                'table_name' => 'polls',
                'created_at' => '2018-08-20 05:19:38',
                'updated_at' => '2018-08-20 05:19:38',
            ),
            58 => 
            array (
                'id' => 59,
                'key' => 'edit_polls',
                'table_name' => 'polls',
                'created_at' => '2018-08-20 05:19:38',
                'updated_at' => '2018-08-20 05:19:38',
            ),
            59 => 
            array (
                'id' => 60,
                'key' => 'add_polls',
                'table_name' => 'polls',
                'created_at' => '2018-08-20 05:19:38',
                'updated_at' => '2018-08-20 05:19:38',
            ),
            60 => 
            array (
                'id' => 61,
                'key' => 'delete_polls',
                'table_name' => 'polls',
                'created_at' => '2018-08-20 05:19:38',
                'updated_at' => '2018-08-20 05:19:38',
            ),
            61 => 
            array (
                'id' => 62,
                'key' => 'browse_groups',
                'table_name' => 'groups',
                'created_at' => '2018-08-25 16:12:38',
                'updated_at' => '2018-08-25 16:12:38',
            ),
            62 => 
            array (
                'id' => 63,
                'key' => 'read_groups',
                'table_name' => 'groups',
                'created_at' => '2018-08-25 16:12:38',
                'updated_at' => '2018-08-25 16:12:38',
            ),
            63 => 
            array (
                'id' => 64,
                'key' => 'edit_groups',
                'table_name' => 'groups',
                'created_at' => '2018-08-25 16:12:38',
                'updated_at' => '2018-08-25 16:12:38',
            ),
            64 => 
            array (
                'id' => 65,
                'key' => 'add_groups',
                'table_name' => 'groups',
                'created_at' => '2018-08-25 16:12:38',
                'updated_at' => '2018-08-25 16:12:38',
            ),
            65 => 
            array (
                'id' => 66,
                'key' => 'delete_groups',
                'table_name' => 'groups',
                'created_at' => '2018-08-25 16:12:38',
                'updated_at' => '2018-08-25 16:12:38',
            ),
            66 => 
            array (
                'id' => 67,
                'key' => 'browse_notifications',
                'table_name' => 'notifications',
                'created_at' => '2018-08-26 01:11:48',
                'updated_at' => '2018-08-26 01:11:48',
            ),
            67 => 
            array (
                'id' => 68,
                'key' => 'read_notifications',
                'table_name' => 'notifications',
                'created_at' => '2018-08-26 01:11:48',
                'updated_at' => '2018-08-26 01:11:48',
            ),
            68 => 
            array (
                'id' => 69,
                'key' => 'edit_notifications',
                'table_name' => 'notifications',
                'created_at' => '2018-08-26 01:11:48',
                'updated_at' => '2018-08-26 01:11:48',
            ),
            69 => 
            array (
                'id' => 70,
                'key' => 'add_notifications',
                'table_name' => 'notifications',
                'created_at' => '2018-08-26 01:11:48',
                'updated_at' => '2018-08-26 01:11:48',
            ),
            70 => 
            array (
                'id' => 71,
                'key' => 'delete_notifications',
                'table_name' => 'notifications',
                'created_at' => '2018-08-26 01:11:48',
                'updated_at' => '2018-08-26 01:11:48',
            ),
            71 => 
            array (
                'id' => 72,
                'key' => 'browse_pcategories',
                'table_name' => 'pcategories',
                'created_at' => '2018-09-18 23:05:38',
                'updated_at' => '2018-09-18 23:05:38',
            ),
            72 => 
            array (
                'id' => 73,
                'key' => 'read_pcategories',
                'table_name' => 'pcategories',
                'created_at' => '2018-09-18 23:05:38',
                'updated_at' => '2018-09-18 23:05:38',
            ),
            73 => 
            array (
                'id' => 74,
                'key' => 'edit_pcategories',
                'table_name' => 'pcategories',
                'created_at' => '2018-09-18 23:05:38',
                'updated_at' => '2018-09-18 23:05:38',
            ),
            74 => 
            array (
                'id' => 75,
                'key' => 'add_pcategories',
                'table_name' => 'pcategories',
                'created_at' => '2018-09-18 23:05:38',
                'updated_at' => '2018-09-18 23:05:38',
            ),
            75 => 
            array (
                'id' => 76,
                'key' => 'delete_pcategories',
                'table_name' => 'pcategories',
                'created_at' => '2018-09-18 23:05:38',
                'updated_at' => '2018-09-18 23:05:38',
            ),
            76 => 
            array (
                'id' => 77,
                'key' => 'browse_bcategories',
                'table_name' => 'bcategories',
                'created_at' => '2018-09-19 03:41:22',
                'updated_at' => '2018-09-19 03:41:22',
            ),
            77 => 
            array (
                'id' => 78,
                'key' => 'read_bcategories',
                'table_name' => 'bcategories',
                'created_at' => '2018-09-19 03:41:22',
                'updated_at' => '2018-09-19 03:41:22',
            ),
            78 => 
            array (
                'id' => 79,
                'key' => 'edit_bcategories',
                'table_name' => 'bcategories',
                'created_at' => '2018-09-19 03:41:22',
                'updated_at' => '2018-09-19 03:41:22',
            ),
            79 => 
            array (
                'id' => 80,
                'key' => 'add_bcategories',
                'table_name' => 'bcategories',
                'created_at' => '2018-09-19 03:41:22',
                'updated_at' => '2018-09-19 03:41:22',
            ),
            80 => 
            array (
                'id' => 81,
                'key' => 'delete_bcategories',
                'table_name' => 'bcategories',
                'created_at' => '2018-09-19 03:41:22',
                'updated_at' => '2018-09-19 03:41:22',
            ),
            81 => 
            array (
                'id' => 87,
                'key' => 'browse_surveys',
                'table_name' => 'surveys',
                'created_at' => '2018-10-14 13:01:55',
                'updated_at' => '2018-10-14 13:01:55',
            ),
            82 => 
            array (
                'id' => 88,
                'key' => 'read_surveys',
                'table_name' => 'surveys',
                'created_at' => '2018-10-14 13:01:55',
                'updated_at' => '2018-10-14 13:01:55',
            ),
            83 => 
            array (
                'id' => 89,
                'key' => 'edit_surveys',
                'table_name' => 'surveys',
                'created_at' => '2018-10-14 13:01:55',
                'updated_at' => '2018-10-14 13:01:55',
            ),
            84 => 
            array (
                'id' => 90,
                'key' => 'add_surveys',
                'table_name' => 'surveys',
                'created_at' => '2018-10-14 13:01:55',
                'updated_at' => '2018-10-14 13:01:55',
            ),
            85 => 
            array (
                'id' => 91,
                'key' => 'delete_surveys',
                'table_name' => 'surveys',
                'created_at' => '2018-10-14 13:01:55',
                'updated_at' => '2018-10-14 13:01:55',
            ),
            86 => 
            array (
                'id' => 92,
                'key' => 'browse_layouts',
                'table_name' => 'layouts',
                'created_at' => '2018-10-15 13:23:56',
                'updated_at' => '2018-10-15 13:23:56',
            ),
            87 => 
            array (
                'id' => 93,
                'key' => 'read_layouts',
                'table_name' => 'layouts',
                'created_at' => '2018-10-15 13:23:56',
                'updated_at' => '2018-10-15 13:23:56',
            ),
            88 => 
            array (
                'id' => 94,
                'key' => 'edit_layouts',
                'table_name' => 'layouts',
                'created_at' => '2018-10-15 13:23:56',
                'updated_at' => '2018-10-15 13:23:56',
            ),
            89 => 
            array (
                'id' => 95,
                'key' => 'add_layouts',
                'table_name' => 'layouts',
                'created_at' => '2018-10-15 13:23:56',
                'updated_at' => '2018-10-15 13:23:56',
            ),
            90 => 
            array (
                'id' => 96,
                'key' => 'delete_layouts',
                'table_name' => 'layouts',
                'created_at' => '2018-10-15 13:23:56',
                'updated_at' => '2018-10-15 13:23:56',
            ),
            91 => 
            array (
                'id' => 97,
                'key' => 'browse_layout_image_links',
                'table_name' => 'layout_image_links',
                'created_at' => '2018-10-17 20:03:28',
                'updated_at' => '2018-10-17 20:03:28',
            ),
            92 => 
            array (
                'id' => 98,
                'key' => 'read_layout_image_links',
                'table_name' => 'layout_image_links',
                'created_at' => '2018-10-17 20:03:28',
                'updated_at' => '2018-10-17 20:03:28',
            ),
            93 => 
            array (
                'id' => 99,
                'key' => 'edit_layout_image_links',
                'table_name' => 'layout_image_links',
                'created_at' => '2018-10-17 20:03:28',
                'updated_at' => '2018-10-17 20:03:28',
            ),
            94 => 
            array (
                'id' => 100,
                'key' => 'add_layout_image_links',
                'table_name' => 'layout_image_links',
                'created_at' => '2018-10-17 20:03:28',
                'updated_at' => '2018-10-17 20:03:28',
            ),
            95 => 
            array (
                'id' => 101,
                'key' => 'delete_layout_image_links',
                'table_name' => 'layout_image_links',
                'created_at' => '2018-10-17 20:03:28',
                'updated_at' => '2018-10-17 20:03:28',
            ),
        ));
        
        
    }
}