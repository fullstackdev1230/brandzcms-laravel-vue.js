<?php

use Illuminate\Database\Seeder;

class PermissionRoleTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('permission_role')->delete();
        
        \DB::table('permission_role')->insert(array (
            0 => 
            array (
                'permission_id' => 1,
                'role_id' => 1,
            ),
            1 => 
            array (
                'permission_id' => 1,
                'role_id' => 3,
            ),
            2 => 
            array (
                'permission_id' => 1,
                'role_id' => 4,
            ),
            3 => 
            array (
                'permission_id' => 2,
                'role_id' => 1,
            ),
            4 => 
            array (
                'permission_id' => 2,
                'role_id' => 3,
            ),
            5 => 
            array (
                'permission_id' => 2,
                'role_id' => 4,
            ),
            6 => 
            array (
                'permission_id' => 3,
                'role_id' => 1,
            ),
            7 => 
            array (
                'permission_id' => 3,
                'role_id' => 3,
            ),
            8 => 
            array (
                'permission_id' => 3,
                'role_id' => 4,
            ),
            9 => 
            array (
                'permission_id' => 4,
                'role_id' => 1,
            ),
            10 => 
            array (
                'permission_id' => 4,
                'role_id' => 3,
            ),
            11 => 
            array (
                'permission_id' => 4,
                'role_id' => 4,
            ),
            12 => 
            array (
                'permission_id' => 5,
                'role_id' => 1,
            ),
            13 => 
            array (
                'permission_id' => 5,
                'role_id' => 3,
            ),
            14 => 
            array (
                'permission_id' => 5,
                'role_id' => 4,
            ),
            15 => 
            array (
                'permission_id' => 6,
                'role_id' => 1,
            ),
            16 => 
            array (
                'permission_id' => 6,
                'role_id' => 3,
            ),
            17 => 
            array (
                'permission_id' => 6,
                'role_id' => 4,
            ),
            18 => 
            array (
                'permission_id' => 7,
                'role_id' => 1,
            ),
            19 => 
            array (
                'permission_id' => 7,
                'role_id' => 3,
            ),
            20 => 
            array (
                'permission_id' => 7,
                'role_id' => 4,
            ),
            21 => 
            array (
                'permission_id' => 8,
                'role_id' => 1,
            ),
            22 => 
            array (
                'permission_id' => 8,
                'role_id' => 3,
            ),
            23 => 
            array (
                'permission_id' => 8,
                'role_id' => 4,
            ),
            24 => 
            array (
                'permission_id' => 9,
                'role_id' => 1,
            ),
            25 => 
            array (
                'permission_id' => 9,
                'role_id' => 3,
            ),
            26 => 
            array (
                'permission_id' => 9,
                'role_id' => 4,
            ),
            27 => 
            array (
                'permission_id' => 10,
                'role_id' => 1,
            ),
            28 => 
            array (
                'permission_id' => 10,
                'role_id' => 3,
            ),
            29 => 
            array (
                'permission_id' => 10,
                'role_id' => 4,
            ),
            30 => 
            array (
                'permission_id' => 11,
                'role_id' => 1,
            ),
            31 => 
            array (
                'permission_id' => 11,
                'role_id' => 3,
            ),
            32 => 
            array (
                'permission_id' => 12,
                'role_id' => 1,
            ),
            33 => 
            array (
                'permission_id' => 12,
                'role_id' => 3,
            ),
            34 => 
            array (
                'permission_id' => 13,
                'role_id' => 1,
            ),
            35 => 
            array (
                'permission_id' => 13,
                'role_id' => 3,
            ),
            36 => 
            array (
                'permission_id' => 14,
                'role_id' => 1,
            ),
            37 => 
            array (
                'permission_id' => 14,
                'role_id' => 3,
            ),
            38 => 
            array (
                'permission_id' => 15,
                'role_id' => 1,
            ),
            39 => 
            array (
                'permission_id' => 15,
                'role_id' => 3,
            ),
            40 => 
            array (
                'permission_id' => 16,
                'role_id' => 1,
            ),
            41 => 
            array (
                'permission_id' => 16,
                'role_id' => 3,
            ),
            42 => 
            array (
                'permission_id' => 16,
                'role_id' => 4,
            ),
            43 => 
            array (
                'permission_id' => 17,
                'role_id' => 1,
            ),
            44 => 
            array (
                'permission_id' => 17,
                'role_id' => 3,
            ),
            45 => 
            array (
                'permission_id' => 17,
                'role_id' => 4,
            ),
            46 => 
            array (
                'permission_id' => 18,
                'role_id' => 1,
            ),
            47 => 
            array (
                'permission_id' => 18,
                'role_id' => 3,
            ),
            48 => 
            array (
                'permission_id' => 18,
                'role_id' => 4,
            ),
            49 => 
            array (
                'permission_id' => 19,
                'role_id' => 1,
            ),
            50 => 
            array (
                'permission_id' => 19,
                'role_id' => 3,
            ),
            51 => 
            array (
                'permission_id' => 19,
                'role_id' => 4,
            ),
            52 => 
            array (
                'permission_id' => 20,
                'role_id' => 1,
            ),
            53 => 
            array (
                'permission_id' => 20,
                'role_id' => 3,
            ),
            54 => 
            array (
                'permission_id' => 20,
                'role_id' => 4,
            ),
            55 => 
            array (
                'permission_id' => 21,
                'role_id' => 1,
            ),
            56 => 
            array (
                'permission_id' => 21,
                'role_id' => 3,
            ),
            57 => 
            array (
                'permission_id' => 22,
                'role_id' => 1,
            ),
            58 => 
            array (
                'permission_id' => 22,
                'role_id' => 3,
            ),
            59 => 
            array (
                'permission_id' => 23,
                'role_id' => 1,
            ),
            60 => 
            array (
                'permission_id' => 23,
                'role_id' => 3,
            ),
            61 => 
            array (
                'permission_id' => 24,
                'role_id' => 1,
            ),
            62 => 
            array (
                'permission_id' => 24,
                'role_id' => 3,
            ),
            63 => 
            array (
                'permission_id' => 25,
                'role_id' => 1,
            ),
            64 => 
            array (
                'permission_id' => 25,
                'role_id' => 3,
            ),
            65 => 
            array (
                'permission_id' => 26,
                'role_id' => 1,
            ),
            66 => 
            array (
                'permission_id' => 26,
                'role_id' => 3,
            ),
            67 => 
            array (
                'permission_id' => 26,
                'role_id' => 4,
            ),
            68 => 
            array (
                'permission_id' => 27,
                'role_id' => 1,
            ),
            69 => 
            array (
                'permission_id' => 27,
                'role_id' => 3,
            ),
            70 => 
            array (
                'permission_id' => 27,
                'role_id' => 4,
            ),
            71 => 
            array (
                'permission_id' => 28,
                'role_id' => 1,
            ),
            72 => 
            array (
                'permission_id' => 28,
                'role_id' => 3,
            ),
            73 => 
            array (
                'permission_id' => 28,
                'role_id' => 4,
            ),
            74 => 
            array (
                'permission_id' => 29,
                'role_id' => 1,
            ),
            75 => 
            array (
                'permission_id' => 29,
                'role_id' => 3,
            ),
            76 => 
            array (
                'permission_id' => 29,
                'role_id' => 4,
            ),
            77 => 
            array (
                'permission_id' => 30,
                'role_id' => 1,
            ),
            78 => 
            array (
                'permission_id' => 30,
                'role_id' => 3,
            ),
            79 => 
            array (
                'permission_id' => 30,
                'role_id' => 4,
            ),
            80 => 
            array (
                'permission_id' => 31,
                'role_id' => 1,
            ),
            81 => 
            array (
                'permission_id' => 31,
                'role_id' => 3,
            ),
            82 => 
            array (
                'permission_id' => 32,
                'role_id' => 1,
            ),
            83 => 
            array (
                'permission_id' => 32,
                'role_id' => 3,
            ),
            84 => 
            array (
                'permission_id' => 33,
                'role_id' => 1,
            ),
            85 => 
            array (
                'permission_id' => 33,
                'role_id' => 3,
            ),
            86 => 
            array (
                'permission_id' => 34,
                'role_id' => 1,
            ),
            87 => 
            array (
                'permission_id' => 34,
                'role_id' => 3,
            ),
            88 => 
            array (
                'permission_id' => 35,
                'role_id' => 1,
            ),
            89 => 
            array (
                'permission_id' => 35,
                'role_id' => 3,
            ),
            90 => 
            array (
                'permission_id' => 36,
                'role_id' => 1,
            ),
            91 => 
            array (
                'permission_id' => 36,
                'role_id' => 3,
            ),
            92 => 
            array (
                'permission_id' => 37,
                'role_id' => 1,
            ),
            93 => 
            array (
                'permission_id' => 37,
                'role_id' => 3,
            ),
            94 => 
            array (
                'permission_id' => 38,
                'role_id' => 1,
            ),
            95 => 
            array (
                'permission_id' => 38,
                'role_id' => 3,
            ),
            96 => 
            array (
                'permission_id' => 39,
                'role_id' => 1,
            ),
            97 => 
            array (
                'permission_id' => 39,
                'role_id' => 3,
            ),
            98 => 
            array (
                'permission_id' => 40,
                'role_id' => 1,
            ),
            99 => 
            array (
                'permission_id' => 40,
                'role_id' => 3,
            ),
            100 => 
            array (
                'permission_id' => 41,
                'role_id' => 1,
            ),
            101 => 
            array (
                'permission_id' => 41,
                'role_id' => 3,
            ),
            102 => 
            array (
                'permission_id' => 41,
                'role_id' => 4,
            ),
            103 => 
            array (
                'permission_id' => 42,
                'role_id' => 1,
            ),
            104 => 
            array (
                'permission_id' => 42,
                'role_id' => 3,
            ),
            105 => 
            array (
                'permission_id' => 42,
                'role_id' => 4,
            ),
            106 => 
            array (
                'permission_id' => 43,
                'role_id' => 1,
            ),
            107 => 
            array (
                'permission_id' => 43,
                'role_id' => 3,
            ),
            108 => 
            array (
                'permission_id' => 43,
                'role_id' => 4,
            ),
            109 => 
            array (
                'permission_id' => 44,
                'role_id' => 1,
            ),
            110 => 
            array (
                'permission_id' => 44,
                'role_id' => 3,
            ),
            111 => 
            array (
                'permission_id' => 44,
                'role_id' => 4,
            ),
            112 => 
            array (
                'permission_id' => 45,
                'role_id' => 1,
            ),
            113 => 
            array (
                'permission_id' => 45,
                'role_id' => 3,
            ),
            114 => 
            array (
                'permission_id' => 45,
                'role_id' => 4,
            ),
            115 => 
            array (
                'permission_id' => 46,
                'role_id' => 1,
            ),
            116 => 
            array (
                'permission_id' => 46,
                'role_id' => 3,
            ),
            117 => 
            array (
                'permission_id' => 46,
                'role_id' => 4,
            ),
            118 => 
            array (
                'permission_id' => 47,
                'role_id' => 1,
            ),
            119 => 
            array (
                'permission_id' => 47,
                'role_id' => 3,
            ),
            120 => 
            array (
                'permission_id' => 47,
                'role_id' => 4,
            ),
            121 => 
            array (
                'permission_id' => 48,
                'role_id' => 1,
            ),
            122 => 
            array (
                'permission_id' => 48,
                'role_id' => 3,
            ),
            123 => 
            array (
                'permission_id' => 48,
                'role_id' => 4,
            ),
            124 => 
            array (
                'permission_id' => 49,
                'role_id' => 1,
            ),
            125 => 
            array (
                'permission_id' => 49,
                'role_id' => 3,
            ),
            126 => 
            array (
                'permission_id' => 49,
                'role_id' => 4,
            ),
            127 => 
            array (
                'permission_id' => 50,
                'role_id' => 1,
            ),
            128 => 
            array (
                'permission_id' => 50,
                'role_id' => 3,
            ),
            129 => 
            array (
                'permission_id' => 50,
                'role_id' => 4,
            ),
            130 => 
            array (
                'permission_id' => 51,
                'role_id' => 1,
            ),
            131 => 
            array (
                'permission_id' => 51,
                'role_id' => 3,
            ),
            132 => 
            array (
                'permission_id' => 51,
                'role_id' => 4,
            ),
            133 => 
            array (
                'permission_id' => 52,
                'role_id' => 1,
            ),
            134 => 
            array (
                'permission_id' => 52,
                'role_id' => 3,
            ),
            135 => 
            array (
                'permission_id' => 52,
                'role_id' => 4,
            ),
            136 => 
            array (
                'permission_id' => 53,
                'role_id' => 1,
            ),
            137 => 
            array (
                'permission_id' => 53,
                'role_id' => 3,
            ),
            138 => 
            array (
                'permission_id' => 53,
                'role_id' => 4,
            ),
            139 => 
            array (
                'permission_id' => 54,
                'role_id' => 1,
            ),
            140 => 
            array (
                'permission_id' => 54,
                'role_id' => 3,
            ),
            141 => 
            array (
                'permission_id' => 54,
                'role_id' => 4,
            ),
            142 => 
            array (
                'permission_id' => 55,
                'role_id' => 1,
            ),
            143 => 
            array (
                'permission_id' => 55,
                'role_id' => 3,
            ),
            144 => 
            array (
                'permission_id' => 55,
                'role_id' => 4,
            ),
            145 => 
            array (
                'permission_id' => 56,
                'role_id' => 1,
            ),
            146 => 
            array (
                'permission_id' => 56,
                'role_id' => 3,
            ),
            147 => 
            array (
                'permission_id' => 56,
                'role_id' => 4,
            ),
            148 => 
            array (
                'permission_id' => 57,
                'role_id' => 1,
            ),
            149 => 
            array (
                'permission_id' => 57,
                'role_id' => 3,
            ),
            150 => 
            array (
                'permission_id' => 58,
                'role_id' => 1,
            ),
            151 => 
            array (
                'permission_id' => 58,
                'role_id' => 3,
            ),
            152 => 
            array (
                'permission_id' => 59,
                'role_id' => 1,
            ),
            153 => 
            array (
                'permission_id' => 59,
                'role_id' => 3,
            ),
            154 => 
            array (
                'permission_id' => 60,
                'role_id' => 1,
            ),
            155 => 
            array (
                'permission_id' => 60,
                'role_id' => 3,
            ),
            156 => 
            array (
                'permission_id' => 61,
                'role_id' => 1,
            ),
            157 => 
            array (
                'permission_id' => 61,
                'role_id' => 3,
            ),
            158 => 
            array (
                'permission_id' => 62,
                'role_id' => 1,
            ),
            159 => 
            array (
                'permission_id' => 62,
                'role_id' => 3,
            ),
            160 => 
            array (
                'permission_id' => 62,
                'role_id' => 4,
            ),
            161 => 
            array (
                'permission_id' => 63,
                'role_id' => 1,
            ),
            162 => 
            array (
                'permission_id' => 63,
                'role_id' => 3,
            ),
            163 => 
            array (
                'permission_id' => 63,
                'role_id' => 4,
            ),
            164 => 
            array (
                'permission_id' => 64,
                'role_id' => 1,
            ),
            165 => 
            array (
                'permission_id' => 64,
                'role_id' => 3,
            ),
            166 => 
            array (
                'permission_id' => 64,
                'role_id' => 4,
            ),
            167 => 
            array (
                'permission_id' => 65,
                'role_id' => 1,
            ),
            168 => 
            array (
                'permission_id' => 65,
                'role_id' => 3,
            ),
            169 => 
            array (
                'permission_id' => 65,
                'role_id' => 4,
            ),
            170 => 
            array (
                'permission_id' => 66,
                'role_id' => 1,
            ),
            171 => 
            array (
                'permission_id' => 66,
                'role_id' => 3,
            ),
            172 => 
            array (
                'permission_id' => 66,
                'role_id' => 4,
            ),
            173 => 
            array (
                'permission_id' => 67,
                'role_id' => 1,
            ),
            174 => 
            array (
                'permission_id' => 67,
                'role_id' => 3,
            ),
            175 => 
            array (
                'permission_id' => 67,
                'role_id' => 4,
            ),
            176 => 
            array (
                'permission_id' => 68,
                'role_id' => 1,
            ),
            177 => 
            array (
                'permission_id' => 68,
                'role_id' => 3,
            ),
            178 => 
            array (
                'permission_id' => 68,
                'role_id' => 4,
            ),
            179 => 
            array (
                'permission_id' => 69,
                'role_id' => 1,
            ),
            180 => 
            array (
                'permission_id' => 69,
                'role_id' => 3,
            ),
            181 => 
            array (
                'permission_id' => 69,
                'role_id' => 4,
            ),
            182 => 
            array (
                'permission_id' => 70,
                'role_id' => 1,
            ),
            183 => 
            array (
                'permission_id' => 70,
                'role_id' => 3,
            ),
            184 => 
            array (
                'permission_id' => 70,
                'role_id' => 4,
            ),
            185 => 
            array (
                'permission_id' => 71,
                'role_id' => 1,
            ),
            186 => 
            array (
                'permission_id' => 71,
                'role_id' => 3,
            ),
            187 => 
            array (
                'permission_id' => 71,
                'role_id' => 4,
            ),
            188 => 
            array (
                'permission_id' => 72,
                'role_id' => 1,
            ),
            189 => 
            array (
                'permission_id' => 72,
                'role_id' => 3,
            ),
            190 => 
            array (
                'permission_id' => 72,
                'role_id' => 4,
            ),
            191 => 
            array (
                'permission_id' => 73,
                'role_id' => 1,
            ),
            192 => 
            array (
                'permission_id' => 73,
                'role_id' => 3,
            ),
            193 => 
            array (
                'permission_id' => 73,
                'role_id' => 4,
            ),
            194 => 
            array (
                'permission_id' => 74,
                'role_id' => 1,
            ),
            195 => 
            array (
                'permission_id' => 74,
                'role_id' => 3,
            ),
            196 => 
            array (
                'permission_id' => 74,
                'role_id' => 4,
            ),
            197 => 
            array (
                'permission_id' => 75,
                'role_id' => 1,
            ),
            198 => 
            array (
                'permission_id' => 75,
                'role_id' => 3,
            ),
            199 => 
            array (
                'permission_id' => 75,
                'role_id' => 4,
            ),
            200 => 
            array (
                'permission_id' => 76,
                'role_id' => 1,
            ),
            201 => 
            array (
                'permission_id' => 76,
                'role_id' => 3,
            ),
            202 => 
            array (
                'permission_id' => 76,
                'role_id' => 4,
            ),
            203 => 
            array (
                'permission_id' => 77,
                'role_id' => 1,
            ),
            204 => 
            array (
                'permission_id' => 77,
                'role_id' => 3,
            ),
            205 => 
            array (
                'permission_id' => 77,
                'role_id' => 4,
            ),
            206 => 
            array (
                'permission_id' => 78,
                'role_id' => 1,
            ),
            207 => 
            array (
                'permission_id' => 78,
                'role_id' => 3,
            ),
            208 => 
            array (
                'permission_id' => 78,
                'role_id' => 4,
            ),
            209 => 
            array (
                'permission_id' => 79,
                'role_id' => 1,
            ),
            210 => 
            array (
                'permission_id' => 79,
                'role_id' => 3,
            ),
            211 => 
            array (
                'permission_id' => 79,
                'role_id' => 4,
            ),
            212 => 
            array (
                'permission_id' => 80,
                'role_id' => 1,
            ),
            213 => 
            array (
                'permission_id' => 80,
                'role_id' => 3,
            ),
            214 => 
            array (
                'permission_id' => 80,
                'role_id' => 4,
            ),
            215 => 
            array (
                'permission_id' => 81,
                'role_id' => 1,
            ),
            216 => 
            array (
                'permission_id' => 81,
                'role_id' => 3,
            ),
            217 => 
            array (
                'permission_id' => 81,
                'role_id' => 4,
            ),
            218 => 
            array (
                'permission_id' => 87,
                'role_id' => 1,
            ),
            219 => 
            array (
                'permission_id' => 88,
                'role_id' => 1,
            ),
            220 => 
            array (
                'permission_id' => 89,
                'role_id' => 1,
            ),
            221 => 
            array (
                'permission_id' => 90,
                'role_id' => 1,
            ),
            222 => 
            array (
                'permission_id' => 91,
                'role_id' => 1,
            ),
            223 => 
            array (
                'permission_id' => 92,
                'role_id' => 1,
            ),
            224 => 
            array (
                'permission_id' => 93,
                'role_id' => 1,
            ),
            225 => 
            array (
                'permission_id' => 94,
                'role_id' => 1,
            ),
            226 => 
            array (
                'permission_id' => 95,
                'role_id' => 1,
            ),
            227 => 
            array (
                'permission_id' => 96,
                'role_id' => 1,
            ),
            228 => 
            array (
                'permission_id' => 97,
                'role_id' => 1,
            ),
            229 => 
            array (
                'permission_id' => 98,
                'role_id' => 1,
            ),
            230 => 
            array (
                'permission_id' => 99,
                'role_id' => 1,
            ),
            231 => 
            array (
                'permission_id' => 100,
                'role_id' => 1,
            ),
            232 => 
            array (
                'permission_id' => 101,
                'role_id' => 1,
            ),
        ));
        
        
    }
}