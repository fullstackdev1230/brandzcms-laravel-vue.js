<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterBrochureCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('brochure_category', function(Blueprint $table){

            $table->dropForeign('brochure_category_category_id_foreign');
            $table->renameColumn('category_id', 'bcategory_id');

            $table->foreign('bcategory_id')->references('id')->on('bcategories')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('brochure_category', function(Blueprint $table){

            $table->dropForeign('brochure_category_bcategory_id_foreign');
            $table->renameColumn('bcategory_id', 'category_id');

            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');

        });
    }
}
