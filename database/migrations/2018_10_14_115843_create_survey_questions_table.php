<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSurveyQuestionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('survey_questions', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('survey_id')->unsigned()->index('survey_id');
			$table->text('question_text', 65535);
			$table->string('question_type', 64);
			$table->smallInteger('question_order');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('survey_questions');
	}

}
