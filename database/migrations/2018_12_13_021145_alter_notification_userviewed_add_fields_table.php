<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterNotificationUserviewedAddFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notification_userviewed', function (Blueprint $table) {
            $table->boolean('ww')->after('user_id')->default(0);
            $table->boolean('app')->after('ww')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('notification_userviewed', function (Blueprint $table) {
            $table->dropColumn(['ww']);
            $table->dropColumn(['app']);
        });
    }
}
