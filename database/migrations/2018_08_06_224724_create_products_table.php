<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('description');
            $table->unsignedInteger('supplier_id');
            $table->foreign('supplier_id')->references('id')->on('suppliers');
            $table->string('barcode_unit')->nullable();
            $table->string('barcode_shipper')->nullable();
            $table->string('barcode_outer')->nullable();
            $table->string('barcode_default')->nullable();
            $table->float('rrp',8,2);
            $table->smallInteger('items_per_outer')->nullable();
            $table->smallInteger('height')->nullable();
            $table->smallInteger('width')->nullable();
            $table->smallInteger('depth')->nullable();
            $table->string('dimension_unit','16')->nullable();
            $table->smallInteger('weight')->nullable();
            $table->string('weight_unit','16')->nullable();
            $table->date('visible_from')->nullable();
            $table->date('visible_to')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
