<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterProductsAddMissingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->boolean('add_to_website')->after('visible_to')->nullable();
            $table->boolean('add_to_app')->after('visible_to')->nullable();
            $table->boolean('add_to_widget')->after('visible_to')->nullable();
            $table->boolean('product_is_new')->after('visible_to')->nullable();
            $table->boolean('product_is_featured')->after('visible_to')->nullable();
            $table->boolean('product_is_merchandising')->after('visible_to')->nullable();
            $table->text('images')->after('visible_to')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {

            $table->dropColumn(['add_to_website',
                                'add_to_app',
                                'add_to_widget',
                                'product_is_new',
                                'product_is_featured',
                                'product_is_merchandising',
                                'images']);
        });
    }
}
