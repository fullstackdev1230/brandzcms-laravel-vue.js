<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterGroupsAddSubscriptionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('groups', function (Blueprint $table) {
            $table->date('subscription_start')->after('max_users')->nullable();
            $table->date('subscription_expiry')->after('subscription_start')->nullable();
            $table->unsignedInteger('auto_renew')->after('subscription_expiry')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('groups', function (Blueprint $table) {
            $table->dropColumn(['subscription_start']);
            $table->dropColumn(['subscription_expiry']);
            $table->dropColumn(['auto_renew']);
        });
    }
}
