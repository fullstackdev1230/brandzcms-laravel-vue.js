<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterProductCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_category', function(Blueprint $table){

            $table->dropForeign('product_category_category_id_foreign');
            $table->renameColumn('category_id', 'pcategory_id');

            $table->foreign('pcategory_id')->references('id')->on('pcategories')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_category', function(Blueprint $table){

            $table->dropForeign('product_category_pcategory_id_foreign');
            $table->renameColumn('pcategory_id', 'category_id');

            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');

        });
    }
}
