<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterLayoutsImageLinksAddDataTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('layout_image_links', function (Blueprint $table) {
            $table->string('data_type')->after('image');
            $table->renameColumn('link', 'data_type_value')->after('data_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('layout_image_links', function (Blueprint $table) {
            $table->renameColumn('data_type_value', 'link');
            $table->dropColumn(['data_type']);
        });
    }
}
