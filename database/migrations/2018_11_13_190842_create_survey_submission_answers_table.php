<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSurveySubmissionAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('survey_submission_answers', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('survey_submission_id');
            $table->foreign('survey_submission_id')->references('id')->on('survey_submissions')->onDelete('cascade');
            $table->unsignedInteger('survey_question_id');
            $table->foreign('survey_question_id')->references('id')->on('survey_questions')->onDelete('cascade');
            $table->unsignedInteger('survey_answer_id')->nullable();
            $table->foreign('survey_answer_id')->references('id')->on('survey_answers')->onDelete('cascade');
            $table->string('answer_text')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('survey_submission_answers');
    }
}
