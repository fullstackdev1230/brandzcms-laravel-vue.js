<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSurveyAnswersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('survey_answers', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('survey_question_id')->unsigned()->index('survey_question_id');
			$table->integer('user_id')->unsigned()->index('user_id');
			$table->string('answer_text');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('survey_answers');
	}

}
