<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterBrochuresNullableFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('brochures', function (Blueprint $table) {
            $table->text('display_image')->nullable(false)->change();
            $table->text('brochure_files')->nullable(false)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('brochures', function (Blueprint $table) {
            $table->text('display_image')->nullable()->change();
            $table->text('brochure_files')->nullable()->change();
        });
    }
}
