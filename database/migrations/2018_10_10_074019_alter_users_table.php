<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('users', function (Blueprint $table) {
            $table->integer('group_id')->after('id')->nullable();
            $table->date('subscription_expiry')->after('password')->nullable();
            $table->tinyInteger('access_to_app')->after('subscription_expiry');
            $table->tinyInteger('access_to_widget')->after('access_to_app');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('users', function (Blueprint $table) {

            $table->dropColumn('group_id');
            $table->dropColumn('subscription_expiry');
            $table->dropColumn('access_to_app');
            $table->dropColumn('access_to_widget');
        });
    }
}
