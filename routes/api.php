
<?php

use TCG\Voyager\Models\DataType;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('getExpiryDate', 'Api\ActionsController@getExpiryDate')->name('getExpiryDate');

Route::get('generatePassword', 'Api\ActionsController@generatePassword')->name('generatePassword');

Route::get('getTermsByGroupId', 'Api\ActionsController@getTermsByGroupId')->name('getTermsByGroupId');

Route::get('updateImageLink', 'Api\ActionsController@updateImageLink')->name('updateImageLink');

Route::middleware('auth:api')->group(function () {

	// Products API
	Route::get('/products/get/{id?}','Api\ProductsController@get');
	Route::get('/products/getAll/{supplier_id?}/{category_id?}/{product_is?}/{user_id?}','Api\ProductsController@getAll');
	Route::get('/products/search/{keyword?}','Api\ProductsController@search');

	// Brochures API
	Route::get('/brochures/get/{id?}','Api\BrochuresController@get');
	Route::get('/brochures/getAll/{category?}/{user_id?}','Api\BrochuresController@getAll');

	// Suppliers API
	Route::get('/suppliers/get/{id?}','Api\SuppliersController@get');
	Route::get('/suppliers/getAll','Api\SuppliersController@getAll');
	Route::get('/suppliers/categories/{supplier_id?}','Api\SuppliersController@getCategories');

	// Users API
	Route::get('/users/get/{user_id?}','Api\UsersController@get');
	Route::get('/users/getAll','Api\UsersController@getAll');
	Route::patch('/users/edit/{user_id?}/{email?}/{password?}/{accent_colour?}/{device_id?}','Api\UsersController@edit');

	//Users Authentication API
	Route::post('/users/login/{email?}/{password?}/{device_id?}','Api\UsersController@login');
	Route::post('/users/register/{email?}/{name?}/{password?}','Api\UsersController@register');
	Route::post('/users/update/{email?}/{password?}/{name?}','Api\UsersController@update');

	// Groups API
	Route::get('/user_groups/get/{id?}','Api\GroupsController@get');

	// Pcategories API
	Route::get('/product_categories/get/{id?}','Api\PcategoriesController@get');
	Route::get('/product_categories/getAll','Api\PcategoriesController@getAll');

	// ProductUserFavorites API
	Route::get('/product_user_favorites/{user_id?}/{product_id?}','Api\ProductUserFavoritesController@get');
	Route::post('/product_user_favorites/{user_id?}/{product_id?}','Api\ProductUserFavoritesController@save');
	Route::delete('/product_user_favorites/{user_id?}/{product_id?}','Api\ProductUserFavoritesController@delete');

	// Categories API
	Route::get('/categories/getAll/{user_id?}/{parent_id?}','Api\CategoriesController@getAll');
	Route::get('/categories/get/{id?}','Api\CategoriesController@get');

	// Orders API
	Route::get('/orders/history/{user_id?}','Api\OrdersController@getHistory');
	Route::delete('/orders/{order_id?}/{user_id?}','Api\OrdersController@deleteHistory');
	Route::post('/orders/add/{user_id?}/{subject?}/{to_email?}/{notes?}','Api\OrdersController@add');
	Route::post('/orders/saveOrders/{orders?}','Api\OrdersController@saveOrders');

	//Emails API
	Route::post('/email/send/{email_address?}/{email_name?}/{subject?}/{attachment?}/{notes?}/{brochure_id?}','Api\EmailsController@send');

	//Notifications API
	Route::get('/notifications/getAll/{notification_type?}/{user_id?}','Api\NotificationsController@getAll');
	Route::post('/notifications/viewed/{notification_id?}/{user_id?}','Api\NotificationsController@addNotificationUserViewed');
	Route::delete('/notifications/deleted/{notification_id?}/{user_id?}','Api\NotificationsController@addNotificationUserDeleted');

	//Surveys
	Route::get('/surveys/getAll/{user_id?}','Api\SurveysController@getAll');
	Route::post('/surveys/submit/{survey_id?}/{user_id?}','Api\SurveysController@submitSurvey');
	Route::post('/surveys/viewed/{survey_id?}/{user_id?}','Api\SurveysController@addSurveyUserViewed');
	Route::delete('/surveys/deleted/{survey_id?}/{user_id?}','Api\SurveysController@addSurveyUserDeleted');
});
