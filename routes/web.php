<?php

use TCG\Voyager\Models\DataType;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('admin');
});




Route::group(['prefix' => 'admin'], function () {

    Voyager::routes();

    Route::group(['as' => 'voyager.'], function () {

	    $namespacePrefix = '\\'.config('voyager.controllers.namespace').'\\';

	    Route::group(['middleware' => 'admin.user'], function () use ($namespacePrefix) {

    	// Brochure Diplay Order Route
    	Route::get('brochures/order/display/{categoryId?}', 'BrochuresController@displayOrder')->name('brochures.order.display');
    	Route::post('brochures/order/change', 'BrochuresController@changeOrder')->name('brochures.order.change');

		Route::get('/passport', function () {
		    return view('passport');
		});

	        try {
	            foreach (DataType::all() as $dataType) {

	                $breadController = $dataType->controller
	                                 ? $dataType->controller
	                                 : $namespacePrefix.'VoyagerBaseController';

	                ///////////////////
	               	// Custom Routes //
	                ///////////////////

	                Route::post($dataType->slug.'/export', $breadController.'@export')->name($dataType->slug.'.export');

	                Route::post($dataType->slug.'/bulk-edit', $breadController.'@bulkEdit')->name($dataType->slug.'.bulkEdit');

	            }

	        } catch (\InvalidArgumentException $e) {

	            throw new \InvalidArgumentException("Custom routes hasn't been configured because: ".$e->getMessage(), 1);

	        } catch (\Exception $e) {

	            // do nothing, might just be because table not yet migrated.
	        }
	    });


	  });
});

Auth::routes();

