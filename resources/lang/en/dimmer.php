<?php

return [
    'product'           => 'Product|Products',
    'product_link_text' => 'View All Products',
    'product_text'      => 'You have :count :string in your database. Click on button below to view all products.',

    'brochure'           => 'Brochure|Brochures',
    'brochure_link_text' => 'View All Brochures',
    'brochure_text'      => 'You have :count :string in your database. Click on button below to view all brochures.',

    'notification'           => 'Notification|Notifications',
    'notification_link_text' => 'View All Notifications',
    'notification_text'      => 'You have :count :string in your database. Click on button below to view all notifications.',

    'survey'           => 'Survey|Surveys',
    'survey_link_text' => 'View All Surveys',
    'survey_text'      => 'You have :count :string in your database. Click on button below to view all surveys.',

    'user'           => 'User|Users',
    'user_link_text' => 'View All Users',
    'user_text'      => 'You have :count :string in your database. Click on button below to view all users.',
];
