@if(isset($emailContent->notes))
<p>{{ $emailContent->notes }}</p>
@endif

@if(isset($emailContent->attachments))
<p>Please download attachments from: </p>
	@foreach($emailContent->attachments as $attachment)
		<a href="{{ $attachment->url }}">{{ $attachment->file_name }}</a>
	@endforeach
@endif