{{ $objOrder->notes }}


Order Info:

Product Name|Supplier|RRP|Units per Outer|Quantity

@foreach ($objOrder->products as $product)
{{ $product->name }}|{{ $product->supplier }}|{{ $product->rrp }}|{{ $product->units_per_outer }}|$product->quantity }}

@endforeach
