<p>{{ $objOrder->notes }}</p>
<br>

<p>Order Info: <p>
<br>
<table>
	<thead style="text-align: left;">
		<th>Product Name</th>
		<th>Supplier</th>
		<th>RRP</th>
		<th>Units per Outer</th>
		<th>Quantity</th>
	</thead>
	<tbody>
		@foreach ($objOrder->products as $product)
    		<tr>
    			<td style="text-align: left;">{{ $product->name }}</td>
    			<td style="text-align: left;">{{ $product->supplier }}</td>
    			<td style="text-align: center;">{{ $product->rrp }}</td>
    			<td style="text-align: center;">{{ $product->units_per_outer }}</td>
    			<td style="text-align: center;">{{ $product->quantity }}</td>
    		</tr>
		@endforeach
	</tbody>
</table>
 
<br>