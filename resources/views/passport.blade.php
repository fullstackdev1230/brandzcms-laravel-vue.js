@extends('voyager::master')


@section('page_title', __('voyager::generic.viewing').' '.'Passport')

@section('page_header')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-9 title-total">
                <h1 class="page-title">
                    <i class=""></i>
                    Passport
                </h1>
            </div>
        </div>
    </div>
@stop

@section('content')
<div class="page-content container-fluid" id="app">
	<passport-clients></passport-clients>
	<br>
	<passport-authorized-clients></passport-authorized-clients>
	<br>
	<passport-personal-access-tokens></passport-personal-access-tokens>
	<br>
</div>
@stop

