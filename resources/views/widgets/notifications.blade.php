<div class="panel widget center bgimage" style="margin-bottom:0;overflow:hidden;background-image:url('{{ $image }}');">
    <div class="dimmer"></div>
    <div class="panel-content">
    	<h4 class="icon voyager-chat"></h4>
        <h4>
    		{!! $title !!}
    	</h4>
        <table class="table">
        	<thead></thead>
        	<tbody>

                @if($notifications->isEmpty())
                    <tr>
                        <td class="no-result-display">No notifications at the moment.</td>
                    </tr>
                @else
                    @foreach ($notifications as $notification)
                        <tr class="notif-row">
                            <td width="10%">
                                <span class="notif-icon icon voyager-bell"></span>
                            </td>
                            <td width="70%" class="notif-content">
                                <div class="notif-content-data">{{ strip_tags($notification->notification_content) }}</div>
                                <div class="notif-date">{{ Carbon\Carbon::parse($notification->publish_date)->format('d/m/Y') }}</div>
                            </td>
                            <td width="10%">
                                @if(!empty($notification->link))
                                    <a href="{{ $notification->link }}" class="btn btn-view-all" style="text-decoration: none">View</a><br />
                                @else
                                    <a href="{{ route('voyager.notifications.show',['id' => $notification->id]) }}" class="btn btn-view-all" style="text-decoration: none">View</a><br />
                                @endif
                            </td>
                            <td width="10%">
                                <a href="javascript:;" title="Delete" class="btn btn-sm btn-danger delete-notif" data-id="{{ $notification->id }}" id="delete-{{ $notification->id }}">
                                    <span class="voyager-x"></span>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                @endif
        		
        	</tbody>
        </table>
    </div>
</div>

 {{-- Single delete modal --}}
    <div class="modal modal-danger fade" tabindex="-1" id="delete_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('voyager::generic.close') }}"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><i class="voyager-trash"></i> {{ __('voyager::generic.delete_question') }} {{ strtolower('notification') }}?</h4>
                </div>
                <div class="modal-footer">
                    <form action="#" id="delete_form" method="POST">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <input type="hidden" value="dashboard" name="del_location">
                        <input type="submit" class="btn btn-danger pull-right delete-confirm" value="{{ __('voyager::generic.delete_confirm') }}">
                    </form>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

@section('javascript')
    <script>
        var deleteFormAction;
        $('td').on('click', '.delete-notif', function (e) {
            $('#delete_form')[0].action = '{{ route('voyager.notifications.destroy', ['id' => '__id']) }}'.replace('__id', $(this).data('id'));
            $('#delete_modal').modal('show');
        });
    </script>
@stop

