@if(isset($options->model) && isset($options->type))

	@if(class_exists($options->model))

		@php $relationshipField = $row->field; @endphp

		@if($options->type == 'belongsTo')

			@if(isset($view) && ($view == 'browse' || $view == 'read'))

				@php
					$relationshipData = (isset($data)) ? $data : $dataTypeContent;
					$model = app($options->model);
					if (method_exists($model, 'getRelationship')) {
						$query = $model::getRelationship($relationshipData->{$options->column});
					} else {
						$query = $model::find($relationshipData->{$options->column});
					}
            	@endphp

            	@if(isset($query))
					<p>{{ $query->{$options->label} }}</p>
				@else
					<p>No results</p>
				@endif

			@else

				<select class="form-control select2" name="{{ $options->column }}">
					@php
						$model = app($options->model);
						$query = $model::all();
					@endphp

					@if($row->required === 0)
						{{-- <option value="">{{__('voyager::generic.none')}}</option> --}}
					@endif

					@if($dataType->name == 'users')
						@foreach($query as $relationshipData)
							@if(is_null($dataTypeContent->{$options->column}))
								<option value="{{ $relationshipData->{$options->key} }}" 
								@if(2 == $relationshipData->{$options->key}){{ 'selected="selected"' }}
								@endif>{{ $relationshipData->{$options->label} }}</option>
							@else
								<option value="{{ $relationshipData->{$options->key} }}" 
								@if($dataTypeContent->{$options->column} == $relationshipData->{$options->key}){{ 'selected="selected"' }}
								@endif>{{ $relationshipData->{$options->label} }}</option>
							@endif
						@endforeach
					@else
						@foreach($query as $relationshipData)
							<option value="{{ $relationshipData->{$options->key} }}" @if($dataTypeContent->{$options->column} == $relationshipData->{$options->key}){{ 'selected="selected"' }}@endif>{{ $relationshipData->{$options->label} }}</option>
						@endforeach
					@endif

					
				</select>

			@endif

		@elseif($options->type == 'hasOne')

			@php

				$relationshipData = (isset($data)) ? $data : $dataTypeContent;

				$model = app($options->model);
        		$query = $model::where($options->column, '=', $relationshipData->id)->first();

			@endphp

			@if(isset($query))
				<p>{{ $query->{$options->label} }}</p>
			@else
				<p>None results</p>
			@endif

		@elseif($options->type == 'hasMany')

			@if(isset($view) && ($view == 'browse' || $view == 'read'))

				@php
					$relationshipData = (isset($data)) ? $data : $dataTypeContent;
					$model = app($options->model);
            		$selected_values = $model::where($options->column, '=', $relationshipData->id)->pluck($options->label)->all();
				@endphp

	            @if($view == 'browse')
	            	@php
	            		$string_values = implode(", ", $selected_values);
	            		if(strlen($string_values) > 25){ $string_values = substr($string_values, 0, 25) . '...'; }
	            	@endphp

	            	@if(empty($selected_values))
	            		@if(isset($show_count) && $show_count)
	            			<p>0</p>
	            		@else
			            	<p>No results</p>
			            @endif
		            @else
	            		@if(isset($show_count) && $show_count)
		            		{{ count($selected_values) }}
		            	@else
		            		<p>{{ $string_values }}</p>
		            	@endif
	            	@endif
	            @else
	            	@if(empty($selected_values))
		            	@if(isset($show_count) && $show_count)
	            			<p>0</p>
	            		@else
			            	<p>No results</p>
			            @endif
		            @else
		            	@if(isset($show_count) && $show_count)
		            		<p>{{ count($selected_values) }}</p>
		            	@else
		            	<ul>
			            	@foreach($selected_values as $selected_value)
			            		<li>{{ $selected_value }}</li>
			            	@endforeach
			            </ul>
		            	@endif
			        @endif
	            @endif

			@else

				@php
					$model = app($options->model);
            		$query = $model::where($options->column, '=', $dataTypeContent->id)->get();
				@endphp

				@if(isset($query))
					<ul>
						@foreach($query as $query_res)
							<li>{{ $query_res->{$options->label} }}</li>
						@endforeach
					</ul>

				@else
					<p>No results</p>
				@endif

			@endif

		@elseif($options->type == 'belongsToMany')

			@if(isset($view) && ($view == 'browse' || $view == 'read'))

				@php
					$relationshipData = (isset($data)) ? $data : $dataTypeContent;
	            	$selected_values = isset($relationshipData) ? $relationshipData->belongsToMany($options->model, $options->pivot_table)->pluck($options->label)->all() : array();
	            @endphp

	            @if($view == 'browse')
	            	@php
	            		$string_values = implode(", ", $selected_values);
	            		if(strlen($string_values) > 25){ $string_values = substr($string_values, 0, 25) . '...'; }
	            	@endphp

	            	@if(empty($selected_values))
	            		@if(isset($show_count) && $show_count)
	            			<p>0</p>
	            		@else
			            	<p>No results</p>
			            @endif
		            @else
	            		@if(isset($show_count) && $show_count)
		            		{{ count($selected_values) }}
		            	@else
		            		<p>{{ $string_values }}</p>
		            	@endif
	            	@endif

	            @else
	            	@if(empty($selected_values))
		            	<p>No results</p>
		            @else
		            	<ul>
		            		@if(isset($show_count) && $show_count)
			            		{{ count($selected_values) }}
			            	@else
				            	@foreach($selected_values as $selected_value)
				            		<li>{{ $selected_value }}</li>
				            	@endforeach
				            @endif
			            </ul>
			        @endif
	            @endif

			@else
				<select
					class="form-control @if(isset($options->taggable) && $options->taggable == 'on') select2-taggable @else select2 @endif"
					name="{{ $relationshipField }}[]" multiple
					@if(isset($options->taggable) && $options->taggable == 'on')
						data-route="{{ route('voyager.'.str_slug($options->table).'.store') }}"
						data-label="{{$options->label}}"
						data-error-message="{{__('voyager::bread.error_tagging')}}"
					@endif
				>

			            @php
							$selected_values = isset($dataTypeContent) ? $dataTypeContent->belongsToMany($options->model, $options->pivot_table)->pluck($options->table.'.'.$options->key)->all() : array();

							if($dataType->name == 'products'){
								$root_categories = app($options->model)->whereNull('parent_id')
																	   ->with(['children' => function($query) {
																						        $query->orderBy('name','ASC');
																						    }])
																	   ->orderBy('name','ASC')->get();

								$relationshipOptions = list_categories($root_categories);

							}else{
			                	$relationshipOptions = app($options->model)->all();
							}
			                //$relationshipOptions = app($options->model)->with('children')->whereNull('parent_id')->get(); 
						@endphp

						<option value="all">{{__('generic.all')}}</option>

						@if($row->required === 0)
							{{--<option value="">{{__('voyager::generic.none')}}</option>--}}
						@endif

			            @foreach($relationshipOptions as $relkey => $relationshipOption)

			            	@if($dataType->name == 'brochures' || $dataType->name == 'surveys' || $dataType->name == 'notifications' || $dataType->name == 'layouts')
								
								@if($relkey == 0 || $relationshipOption->user_type != $relationshipOptions[$relkey-1]->user_type)
									<option value="{{ $relationshipOption->user_type }}">{{ ucfirst($relationshipOption->user_type) }}</option>
									<option value="{{ $relationshipOption->{$options->key} }}" @if(in_array($relationshipOption->{$options->key}, $selected_values)){{ 'selected="selected"' }}@endif>-- {{ $relationshipOption->{$options->label} }}</option>
								@else
									<option value="{{ $relationshipOption->{$options->key} }}" @if(in_array($relationshipOption->{$options->key}, $selected_values)){{ 'selected="selected"' }}@endif>-- {{ $relationshipOption->{$options->label} }}</option>
								@endif
			            		
							@elseif($dataType->name == 'products')
								<option value="{{ $relationshipOption['id'] }}" @if(in_array($relationshipOption['id'], $selected_values)){{ 'selected="selected"' }}@endif>{{ $relationshipOption['name'] }}</option>
								
								@php
									$option_loop = $relationshipOption['children'];
								@endphp
								
								@if(!empty($relationshipOption['children']))
									@foreach($relationshipOption['children'] as $childkey => $child)
										<option value="{{ $child['id'] }}" @if(in_array($child['id'], $selected_values)){{ 'selected="selected"' }}@endif>--{{ $child['name'] }}</option>

										@if(!empty($child['children']))
											@foreach($child['children'] as $c2key => $c2)
												<option value="{{ $c2['id'] }}" @if(in_array($c2['id'], $selected_values)){{ 'selected="selected"' }}@endif>----{{ $c2['name'] }}</option>
												
												@if(!empty($c2['children']))
													@foreach($c2['children'] as $c3key => $c3)
														<option value="{{ $c3['id'] }}" @if(in_array($c3['id'], $selected_values)){{ 'selected="selected"' }}@endif>------{{ $c3['name'] }}</option>
													@endforeach
												@endif

											@endforeach
										@endif

									@endforeach
								@endif
								


			            	@else
								<option value="{{ $relationshipOption->{$options->key} }}" @if(in_array($relationshipOption->{$options->key}, $selected_values)){{ 'selected="selected"' }}@endif>{{ $relationshipOption->{$options->label} }}</option>
			            	@endif
			                
			            	
							{{-- @if(!is_null($relationshipOption->children))
								@foreach($relationshipOption->children as $children)
									<option value="{{ $relationshipOption->{$options->key} }}">-{{ $children->{$options->label} }}</option>
								@endforeach
							@endif --}}


			            @endforeach

				</select>

			@endif

		@endif

	@else

		cannot make relationship because {{ $options->model }} does not exist.

	@endif

@endif
