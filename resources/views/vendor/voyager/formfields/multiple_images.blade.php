<br>
@if(isset($dataTypeContent->{$row->field}))
    <?php $images = json_decode($dataTypeContent->{$row->field}); ?>
    @if($images != null)
        @foreach($images as $image)

			@php
                $path = Storage::url('');
				$image_id = explode($path, $image);
                $image_id = explode('/conversions', $image_id[1]);
                $media_id = $image_id[0];
			@endphp

            <div class="img_settings_container" data-field-name="{{ $row->field }}" style="float:left;padding-right:15px;">
                <img src="{{ $image }}" data-image="{{ $image }}" data-media-id={{ $media_id }} data-id="{{ $dataTypeContent->getKey() }}" style="max-width:200px; height:auto; clear:both; display:block; padding:2px; border:1px solid #ddd; margin-bottom:5px;">
                <a href="#" class="voyager-x remove-multi-image"></a>
            </div>
        @endforeach
    @endif
@endif
<div class="clearfix"></div>
<input @if($row->required == 1) required @endif type="file" name="{{ $row->field }}[]" multiple="multiple" accept="image/*">
