@extends('voyager::master')

@section('page_title', __('voyager::generic.'.(isset($dataTypeContent->id) ? 'edit' : 'add')).' '.$dataType->display_name_singular)

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i>
        {{ __('voyager::generic.'.(isset($dataTypeContent->id) ? 'edit' : 'add')).' '.$dataType->display_name_singular }}
    </h1>
@stop

@section('content')
    <div class="page-content container-fluid">
        <form class="form-edit-add form-horizontal"
              role="form"
              action="{{ (isset($dataTypeContent->id)) ? route('voyager.'.$dataType->slug.'.update', $dataTypeContent->id) : route('voyager.'.$dataType->slug.'.store') }}"
              method="POST" enctype="multipart/form-data" autocomplete="off">
            <!-- PUT Method if we are editing -->
            @if(isset($dataTypeContent->id))
                {{ method_field("PUT") }}
            @endif
            {{ csrf_field() }}

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <!-- Adding / Editing -->
            @php
                $dataTypeRows = $dataType->{(!is_null($dataTypeContent->getKey()) ? 'editRows' : 'addRows' )};

                foreach($dataTypeRows as $row)
                {
                    $fields[$row->field] = $row;
                }

            @endphp

            <div class="row">
                <div class="col-md-8">
                    <div class="panel panel-bordered">
                    {{-- <div class="panel"> --}}
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <div class="panel-body">
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="name">{{ __('voyager::generic.name') }}:</label>
                                <div class="col-sm-10">
                                    {!! app('voyager')->formField($fields['name'], $dataType, $dataTypeContent) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-2" for="email">{{ __('voyager::generic.email') }}:</label>
                                <div class="col-sm-10">
                                    {!! app('voyager')->formField($fields['email'], $dataType, $dataTypeContent) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                @php
                                    $row = $fields['user_belongsto_group_relationship'];
                                    $options = json_decode($row->details);
                                    $display_options = isset($options->display) ? $options->display : NULL;
                                @endphp

                                <label class="control-label col-sm-2" for="relationship">{{ $row->display_name }}:</label>
                                <div class="col-sm-10">

                                    @include('voyager::formfields.relationship')

                                    @foreach (app('voyager')->afterFormFields($row, $dataType, $dataTypeContent) as $after)
                                        {!! $after->handle($row, $dataType, $dataTypeContent) !!}
                                    @endforeach

                                    @include('voyager::multilingual.input-hidden-bread-edit-add')
                                </div>
                            </div>

                            @can('editRoles', $dataTypeContent)
                                <div class="form-group">
                                    @php
                                        $row = $fields['user_belongsto_role_relationship'];
                                        $options = json_decode($row->details);
                                        $display_options = isset($options->display) ? $options->display : NULL;
                                    @endphp

                                    <label class="control-label col-sm-2" for="default_role">{{ __('voyager::profile.role_default') }}:</label>
                                    <div class="col-sm-10">

                                        @include('voyager::formfields.relationship')

                                        @foreach (app('voyager')->afterFormFields($row, $dataType, $dataTypeContent) as $after)
                                            {!! $after->handle($row, $dataType, $dataTypeContent) !!}
                                        @endforeach

                                        @include('voyager::multilingual.input-hidden-bread-edit-add')
                                    </div>
                                </div>

                                <div class="form-group">
                                    @php
                                        $row = $fields['user_belongstomany_role_relationship'];
                                        $options = json_decode($row->details);
                                        $display_options = isset($options->display) ? $options->display : NULL;
                                    @endphp

                                    <label class="control-label col-sm-2" for="additional_roles">{{ __('voyager::profile.roles_additional') }}:</label>
                                    <div class="col-sm-10">

                                        @include('voyager::formfields.relationship')

                                        @foreach (app('voyager')->afterFormFields($row, $dataType, $dataTypeContent) as $after)
                                            {!! $after->handle($row, $dataType, $dataTypeContent) !!}
                                        @endforeach

                                        @include('voyager::multilingual.input-hidden-bread-edit-add')
                                    </div>
                                </div>
                            @endcan

                            <div class="form-group">
                                <label class="control-label col-sm-2" for="email">Access To:</label>
                                <div class="col-sm-10">
                                    <div class="checkbox">
                                        <label>
                                            {!! app('voyager')->formField($fields['access_to_app'], $dataType, $dataTypeContent) !!}
                                            {{ $fields['access_to_app']->display_name }}
                                        </label>

                                        &nbsp;

                                        <label>
                                            {!! app('voyager')->formField($fields['access_to_widget'], $dataType, $dataTypeContent) !!}
                                            {{ $fields['access_to_widget']->display_name }}
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-2" for="subscription_start">{{ $fields['subscription_start']->display_name }}:</label>
                                <div class="col-sm-10">
                                    {!! app('voyager')->formField($fields['subscription_start'], $dataType, $dataTypeContent) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-2" for="subscription_expiry">{{ $fields['subscription_expiry']->display_name }}:</label>
                                <div class="col-sm-10">
                                    {!! app('voyager')->formField($fields['subscription_expiry'], $dataType, $dataTypeContent) !!}
                                </div>
                            </div>

                            @php
                                if (isset($dataTypeContent->locale)) {
                                    $selected_locale = $dataTypeContent->locale;
                                } else {
                                    $selected_locale = config('app.locale', 'en');
                                }
                            @endphp

                            <div class="form-group">
                                <label class="control-label col-sm-2" for="password">{{__('voyager::generic.password') }}:</label>
                                <div class="col-sm-10">
                                    @if(isset($dataTypeContent->password))
                                        <br/>
                                        <small>{{ __('voyager::profile.password_hint') }}</small>
                                    @endif

                                    <input type="text" class="form-control" id="password" name="password" autocomplete="new-password">
                                    <button class="btn btn-primary" type="button" id="generatePw_btn">Generate</button>
                                </div>
                            </div>
                            <input type="hidden" name="locale" id="locale" value="en">
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="panel panel panel-bordered panel-warning">
                        <div class="panel-body">
                            <div class="form-group">
                                @if(isset($dataTypeContent->avatar))
                                    <img src="{{ filter_var($dataTypeContent->avatar, FILTER_VALIDATE_URL) ? $dataTypeContent->avatar : Voyager::image( $dataTypeContent->avatar ) }}" style="width:200px; height:auto; clear:both; display:block; padding:2px; border:1px solid #ddd; margin-bottom:10px;" />
                                @endif
                                <input type="file" data-name="avatar" name="avatar">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <button type="submit" class="btn btn-primary pull-right save">
                {{ __('voyager::generic.save') }}
            </button>
        </form>

        <iframe id="form_target" name="form_target" style="display:none"></iframe>
        <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post" enctype="multipart/form-data" style="width:0px;height:0;overflow:hidden">
            {{ csrf_field() }}
            <input name="image" id="upload_file" type="file" onchange="$('#my_form').submit();this.value='';">
            <input type="hidden" name="type_slug" id="type_slug" value="{{ $dataType->slug }}">
        </form>
    </div>

<?php
    if(isset($dataTypeContent->password))
        $hasOldPassword = "true";
    else
        $hasOldPassword = "false";
?>
@stop

@section('javascript')
    <script>
        $('document').ready(function () {
            updateSubscriptionExpiry();
            $('.toggleswitch').bootstrapToggle();
        });
    </script>

<script>
$(document).ready(function(){

    $('#generatePw_btn').on('click', function (event) {
        event.preventDefault();
        $.ajax({
          type: "GET",
          url: '/api/generatePassword',
          data:'',
          success: function(data) {
            $('#password').val(data);
          },
          error: function(data) {
            alert('Failed to Generate Password');
          }
        });
    });
});

</script>
<script type="text/javascript">
    $(document).ready(function(){
            var hasPw = <?php echo $hasOldPassword ?>;
            if(hasPw){
                $('#password').removeAttr('required');
            }else{
                $('#password').removeAttr('required');
                $('#password').attr('required', 'required');
            }      
    });

    $('select[name=group_id]').change(function() {
        updateSubscriptionExpiry();
    });
</script>
<script>
    function updateSubscriptionExpiry(){
        var default_subscription_expiry = new Date();
        //get term by group id
        var group_id = $('select[name=group_id]').val();;
        var months_added = 0;

        $.ajax({
          type: "GET",
          url: '/api/getTermsByGroupId',
          data:{ id: group_id },
          success: function(data) {
            months_added = data;
            default_subscription_expiry.setMonth(default_subscription_expiry.getMonth() + parseInt(months_added));
            default_subscription_expiry = $.datepicker.formatDate('yy-mm-dd', default_subscription_expiry)

            $('input[name=subscription_expiry]').attr('value', default_subscription_expiry);
          }
        });


    }
</script>

<script>
    $('document').ready(function () {
        var pageTitle = $('.page-title').text();
        $('.breadcrumb li:last-child').text(pageTitle);
    });
</script>
@stop
