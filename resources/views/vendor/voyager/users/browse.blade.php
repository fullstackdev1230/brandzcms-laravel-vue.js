@extends('vendor.voyager.bread.browse')

@php

	$roles = DB::table('roles')->get();
	$groups = DB::table('groups')->get();
	$states = json_decode(setting('site.state'),true);

	$elements = [
		View::make('elements.filters.user_role',['roles' => $roles]),
		View::make('elements.filters.user_group',['groups' => $groups]),
		View::make('elements.filters.state',['states' => $states['options']]),
	];

@endphp

@section('search_bar')
    @include('elements.extra_search_bar',['elements' => $elements])
@stop