<div class="panel widget center bgimage" style="margin-bottom:0;overflow:hidden;background-image:url('{{ $image }}');">
    <div class="dimmer"></div>
    <div class="panel-content">
        @if (isset($icon))<i class='{{ $icon }}'></i>@endif
        <h4>{!! $title !!}</h4>
        <a href="{{ isset($button['add_new'])?$button['add_new']:'' }}" class="btn btn-add-new">Add New</a><br />
        <a href="{{ $button['link'] }}" class="btn btn-view-all">{!! $button['text'] !!}</a>
    </div>
</div>
