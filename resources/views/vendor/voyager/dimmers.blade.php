@php
$dimmers = Voyager::dimmers();
$count = $dimmers->count();
$classes = [
    'col-xs-12',
    'col-sm-'.($count >= 2 ? '12' : '12'),
    'col-md-'.($count >= 3 ? '6' : ($count >= 2 ? '6' : '6')),
];
$class = implode(' ', $classes);
$prefix = "<div class='{$class}'>";
$suffix = '</div>';
@endphp

<div class="row dashboard-widgets">
	<div class="col-md-8">
    <div class="row">
    	<div class="col-md-6">
    		{{ Widget::productDimmer() }}
        {{ Widget::userDimmer() }}
    	</div>
    	<div class="col-md-6">
        {{ Widget::brochureDimmer() }}
        {{ Widget::surveyDimmer() }}
    	</div>
    </div>
  </div>
	<div class="col-md-4">
		{{ Widget::notifications() }}
	</div>
</div>
