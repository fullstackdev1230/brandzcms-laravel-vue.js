@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{ asset('css/redips.css') }}" type="text/css" />
@stop

@section('page_title', __('voyager::generic.'.(!is_null($dataTypeContent->getKey()) ? 'edit' : 'add')).' '.$dataType->display_name_singular)

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i>
        {{ __('voyager::generic.'.(!is_null($dataTypeContent->getKey()) ? 'edit' : 'add')).' '.$dataType->display_name_singular }}
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-sm-12">

                <div class="panel panel-bordered">
                    <!-- form start -->
                    <form role="form"
                            class="form-edit-add form-horizontal"
                            action="@if(!is_null($dataTypeContent->getKey())){{ route('voyager.'.$dataType->slug.'.update', $dataTypeContent->getKey()) }}@else{{ route('voyager.'.$dataType->slug.'.store') }}@endif"
                            method="POST" enctype="multipart/form-data">

                        <!-- PUT Method if we are editing -->
                        @if(!is_null($dataTypeContent->getKey()))
                            {{ method_field("PUT") }}
                        @endif

                        <!-- CSRF TOKEN -->
                        {{ csrf_field() }}

                        <div class="panel-body">

                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <!-- Adding / Editing -->
                            @php
                                $dataTypeRows = $dataType->{(!is_null($dataTypeContent->getKey()) ? 'editRows' : 'addRows' )};

                                foreach($dataTypeRows as $row)
                                {
                                    $fields[$row->field] = $row;
                                }

                            @endphp

                            {!! app('voyager')->formField($fields['table_html'], $dataType, $dataTypeContent) !!}
                            {!! app('voyager')->formField($fields['cells'], $dataType, $dataTypeContent) !!}

                            <div class="form-group">
                                <label class="control-label col-sm-2" for="name">{{ $fields['name']->display_name }}:</label>
                                <div class="col-sm-10">
                                    {!! app('voyager')->formField($fields['name'], $dataType, $dataTypeContent) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                @php
                                    $row = $fields['layout_belongstomany_group_relationship'];
                                    $options = json_decode($row->details);
                                    $display_options = isset($options->display) ? $options->display : NULL;
                                @endphp

                                <label class="control-label col-sm-2" for="groups">{{ $row->display_name }}:</label>
                                <div class="col-sm-10">

                                    @include('voyager::formfields.relationship')

                                    @foreach (app('voyager')->afterFormFields($row, $dataType, $dataTypeContent) as $after)
                                        {!! $after->handle($row, $dataType, $dataTypeContent) !!}
                                    @endforeach

                                    @include('voyager::multilingual.input-hidden-bread-edit-add')
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-2" for="email">{{ $fields['set_as_default']->display_name }}: </label>
                                <div class="col-sm-10">
                                    <div class="checkbox">
                                        <label>
                                            {!! app('voyager')->formField($fields['set_as_default'], $dataType, $dataTypeContent) !!}
                                        </label>
                                    </div>
                                </div>
                            </div>

                            @include('elements.layout.editor')

                        </div><!-- panel-body -->

                        <div class="panel-footer">
                            <button type="submit" class="btn btn-primary btn-lg save" onclick="setDefaultWidth(); return getCellValues();">{{ __('voyager::generic.save') }}</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-danger" id="confirm_delete_modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager::generic.are_you_sure') }}</h4>
                </div>

                <div class="modal-body">
                    <h4>{{ __('voyager::generic.are_you_sure_delete') }} '<span class="confirm_delete_name"></span>'</h4>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                    <button type="button" class="btn btn-danger" id="confirm_delete">{{ __('voyager::generic.delete_confirm') }}
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Delete File Modal -->
@stop

@section('javascript')

    <script type="text/javascript" src="{{ asset('js/redips-table-min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/redips.js') }}"></script>

    <script>

        function getCellValues() {

            var table = document.getElementById('mainTable');
            var layoutCells = [];

            $("#mainTable").find('td').each( function() {

                layoutCells.push($(this).text());

                $(this).attr('id',$(this).text());
            });

            $("input[name=cells]").val(JSON.stringify(layoutCells));
            $("input[name=table_html]").val($("#mainTable").prop('outerHTML'));

            updateImageLink(layoutCells);

            return true;
        }

        var params = {}
        var $image

        $('document').ready(function () {

            // Load table_html
            var tableHtml = $("input[name=table_html]").val();

            if(tableHtml.length > 0){
                $("#mainTableHolder").html(tableHtml);
                redips.init();
            }

            $('.toggleswitch').bootstrapToggle();

            //Init datepicker for date fields if data-datepicker attribute defined
            //or if browser does not handle date inputs
            $('.form-group input[type=date]').each(function (idx, elt) {
                if (elt.type != 'date' || elt.hasAttribute('data-datepicker')) {
                    elt.type = 'text';
                    $(elt).datetimepicker($(elt).data('datepicker'));
                }
            });

            @if ($isModelTranslatable)
                $('.side-body').multilingual({"editing": true});
            @endif

            $('.side-body input[data-slug-origin]').each(function(i, el) {
                $(el).slugify();
            });

            $('.form-group').on('click', '.remove-multi-image', function (e) {
                e.preventDefault();
                $image = $(this).siblings('img');

                params = {
                    slug:   '{{ $dataType->slug }}',
                    image:  $image.data('image'),
                    id:     $image.data('id'),
                    field:  $image.parent().data('field-name'),
                    _token: '{{ csrf_token() }}'
                }

                $('.confirm_delete_name').text($image.data('image'));
                $('#confirm_delete_modal').modal('show');
            });

            $('#confirm_delete').on('click', function(){
                $.post('{{ route('voyager.media.remove') }}', params, function (response) {
                    if ( response
                        && response.data
                        && response.data.status
                        && response.data.status == 200 ) {

                        toastr.success(response.data.message);
                        $image.parent().fadeOut(300, function() { $(this).remove(); })
                    } else {
                        toastr.error("Error removing image.");
                    }
                });

                $('#confirm_delete_modal').modal('hide');
            });
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
    <script>
    $(document).ready(function () {
        setDefaultWidth();
    });


    function setDefaultWidth(){
        $('#mainTable td').each(function() {
            $(this).removeAttr('style').css("background-color");
            $(this).css('width','132px');
            $(this).css('height','120px');
         });
        $('#mainTable tr').each(function() {
            $(this).css('height','120px');
         });

    }

    function updateImageLink(layoutCells){

        $.ajaxSetup({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var layout_id = "{{ $dataTypeContent->getKey() }}";

        $.ajax({
            type:"GET",
            data: { "layoutCells" : layoutCells, "layout_id" : layout_id },
            url: "{{ route('updateImageLink') }}",

            success:function(data){
                console.log(data);
            },
            error: function (data) {
                console.log(data);
            }
        });

    }
</script>
@stop

