@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_title', __('voyager::generic.'.(!is_null($dataTypeContent->getKey()) ? 'edit' : 'add')).' '.$dataType->display_name_singular)

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i>
        {{ __('voyager::generic.'.(!is_null($dataTypeContent->getKey()) ? 'edit' : 'add')).' '.$dataType->display_name_singular }}
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-sm-12">

                <div class="panel panel-bordered">
                    <!-- form start -->
                    <form role="form"
                            class="form-edit-add form-horizontal"
                            action="@if(!is_null($dataTypeContent->getKey())){{ route('voyager.'.$dataType->slug.'.update', $dataTypeContent->getKey()) }}@else{{ route('voyager.'.$dataType->slug.'.store') }}@endif"
                            method="POST" enctype="multipart/form-data">
                        <!-- PUT Method if we are editing -->
                        @if(!is_null($dataTypeContent->getKey()))
                            {{ method_field("PUT") }}
                        @endif

                        <!-- CSRF TOKEN -->
                        {{ csrf_field() }}

                        <div class="panel-body">

                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <!-- Adding / Editing -->
                            @php
                                $dataTypeRows = $dataType->{(!is_null($dataTypeContent->getKey()) ? 'editRows' : 'addRows' )};

                                foreach($dataTypeRows as $row)
                                {
                                    $fields[$row->field] = $row;
                                }

                            @endphp

                            <div class="form-group">
                                <label class="control-label col-sm-2" for="visible_from">
                                    {{ $fields['publish_date']->display_name }}:
                                </label>
                                <div class="col-sm-10">
                                    <div class="form-row">
                                        <div class="form-group col-sm-2">
                                            {!! app('voyager')->formField($fields['publish_date'], $dataType, $dataTypeContent) !!}
                                        </div>
                                        <div class="form-group col-sm-4">
                                            <label class="control-label col-sm-4" for="visible_to">
                                                {{ $fields['expiry_date']->display_name }}:
                                            </label>
                                            <div class="col-sm-8">
                                                {!! app('voyager')->formField($fields['expiry_date'], $dataType, $dataTypeContent) !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-2" for="visible_to_app">Display On:</label>
                                <div class="col-sm-10">
                                    <div class="checkbox">
                                        <label>
                                            {!! app('voyager')->formField($fields['visible_to_app'], $dataType, $dataTypeContent) !!}
                                            {{ $fields['visible_to_app']->display_name }}
                                        </label>

                                        &nbsp;

                                        <label>
                                            {!! app('voyager')->formField($fields['visible_to_widget'], $dataType, $dataTypeContent) !!}
                                            {{ $fields['visible_to_widget']->display_name }}
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-2" for="groups">{{ $fields['notification_belongstomany_group_relationship']->display_name }}:</label>
                                <div class="col-sm-10">

                                    @php
                                        $row = $fields['notification_belongstomany_group_relationship'];
                                        $options = json_decode($row->details);
                                        $display_options = isset($options->display) ? $options->display : NULL;
                                    @endphp

                                    @include('voyager::formfields.relationship')
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-2" for="notification_title">{{ $fields['notification_title']->display_name }}:</label>
                                <div class="col-sm-10">
                                    {!! app('voyager')->formField($fields['notification_title'], $dataType, $dataTypeContent) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-2" for="notification_type">{{ $fields['notification_type']->display_name }}:</label>
                                <div class="col-sm-10">
                                    <div class="form-group col-sm-3">
                                        {!! app('voyager')->formField($fields['notification_type'], $dataType, $dataTypeContent) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-2" for="notification_content">{{ $fields['notification_content']->display_name }}:</label>
                                <div class="col-sm-10">
                                    {!! app('voyager')->formField($fields['notification_content'], $dataType, $dataTypeContent) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-2" for="images">{{ $fields['images']->display_name }}:</label>
                                <div class="col-sm-10">
                                    {!! app('voyager')->formField($fields['images'], $dataType, $dataTypeContent) !!}
                                </div>
                            </div>

                        </div><!-- panel-body -->

                        <div class="panel-footer">
                            <button type="submit" class="btn btn-primary btn-lg save">{{ __('voyager::generic.save') }}</button>
                        </div>
                    </form>

                    <iframe id="form_target" name="form_target" style="display:none"></iframe>
                    <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post"
                            enctype="multipart/form-data" style="width:0;height:0;overflow:hidden">
                        <input name="image" id="upload_file" type="file"
                                 onchange="$('#my_form').submit();this.value='';">
                        <input type="hidden" name="type_slug" id="type_slug" value="{{ $dataType->slug }}">
                        {{ csrf_field() }}
                    </form>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-danger" id="confirm_delete_modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager::generic.are_you_sure') }}</h4>
                </div>

                <div class="modal-body">
                    <h4>{{ __('voyager::generic.are_you_sure_delete') }} '<span class="confirm_delete_name"></span>'</h4>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                    <button type="button" class="btn btn-danger" id="confirm_delete">{{ __('voyager::generic.delete_confirm') }}
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Delete File Modal -->
@stop

@section('javascript')
    <script>
        var params = {}
        var $image

        $('document').ready(function () {
            $('.toggleswitch').bootstrapToggle();

            //Init datepicker for date fields if data-datepicker attribute defined
            //or if browser does not handle date inputs
            $('.form-group input[type=date]').each(function (idx, elt) {
                if (elt.type != 'date' || elt.hasAttribute('data-datepicker')) {
                    elt.type = 'text';
                    $(elt).datetimepicker($(elt).data('datepicker'));
                }
            });

            @if ($isModelTranslatable)
                $('.side-body').multilingual({"editing": true});
            @endif

            $('.side-body input[data-slug-origin]').each(function(i, el) {
                $(el).slugify();
            });

            $('.form-group').on('click', '.remove-multi-image', function (e) {
                e.preventDefault();
                $image = $(this).siblings('img');

                params = {
                    slug:   '{{ $dataType->slug }}',
                    image:  $image.data('image'),
                    id:     $image.data('id'),
                    field:  $image.parent().data('field-name'),
                    _token: '{{ csrf_token() }}'
                }

                $('.confirm_delete_name').text($image.data('image'));
                $('#confirm_delete_modal').modal('show');
            });

            $('#confirm_delete').on('click', function(){
                $.post('{{ route('voyager.media.remove') }}', params, function (response) {
                    if ( response
                        && response.data
                        && response.data.status
                        && response.data.status == 200 ) {

                        toastr.success(response.data.message);
                        $image.parent().fadeOut(300, function() { $(this).remove(); })
                    } else {
                        toastr.error("Error removing image.");
                    }
                });

                $('#confirm_delete_modal').modal('hide');
            });
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
@stop
