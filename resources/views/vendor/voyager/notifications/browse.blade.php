@extends('vendor.voyager.bread.browse')

@php
	$dateColumn = 13;

    $elements = [
        View::make('elements.filters.date_range')
    ];

	// For the Bulk Edit
	$bulk_actions =[
		'publish_date' => 'Edit Publish Date',
		'expiry_date' => 'Edit Expiry Date',
		'displayed_on' => 'Edit Displayed On',
	];

	$bulk_actions_modal = [

		View::make('elements.bulk_actions.edit_publish',
			[
				'field_to_edit' => 'Publish Date',
				'column' => 'publish_date',
				'field_type' => 'PublishDate',
				'dataType' => $dataType,
			]),

		View::make('elements.bulk_actions.edit_expiry',
			[
				'field_to_edit' => 'Expiry Date',
				'column' => 'expiry_date',
				'field_type' => 'ExpiryDate',
				'dataType' => $dataType,
			]),

			View::make('elements.bulk_actions.edit_displayed_on',
			[
				'field_to_edit' => 'Displayed On',
				'column' => 'visible_to_app',
				'column2' => 'visible_to_widget',
				'field_type' => 'DisplayedOn',
				'dataType' => $dataType,
			]),

	];

@endphp

@section('search_bar')
    @include('elements.extra_search_bar',['elements' => $elements])
@stop