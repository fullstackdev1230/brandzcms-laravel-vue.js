@extends('voyager::master')

@section('page_title', __('voyager::generic.media'))

@section('content')
    <div class="page-content container-fluid">
        @include('voyager::alerts')
        <div class="row">
            <div class="col-md-12">

                <div class="admin-section-title">
                    <h3><i class="voyager-images"></i> {{ __('voyager::generic.media') }}</h3>
                </div>
                <div class="clear"></div>

                <div id="filemanager">

                    <div id="toolbar">

                        <button type="button" class="btn btn-default" id="refresh"><i class="voyager-refresh"></i>
                        </button>
                        <div class="btn-group offset-right">
                                <div class="">
                                    <input type="text" name="filter_search" id="filter_search" placeholder="Search">
                                    <button type="button" class="btn btn-primary" id="filter_search_btn"><i class="voyager-search"></i>
                                        {{ __('voyager::generic.search') }}
                                    </button>
                                </div>
                           
                            <!-- INSERT SEARCH BAR -->
                        </div>
                    </div>

                    <div id="uploadPreview" style="display:none;"></div>

                    <div id="uploadProgress" class="progress active progress-striped">
                        <div class="progress-bar progress-bar-success" style="width: 0"></div>
                    </div>

                    <div id="content">


                        <div class="breadcrumb-container">
                            <ol class="breadcrumb filemanager">
                                <li class="media_breadcrumb" data-folder="/" data-index="0"><span class="arrow"></span><strong>{{ __('voyager::media.library') }}</strong></li>
                                <template v-for="(folder, index) in folders">
                                    <li v-bind:data-folder="folder" v-bind:data-index="index+1"
                                    v-bind:class="{media_breadcrumb: index !== folders.length - 1}"><span
                                                class="arrow"></span>@{{ folder }}</li>

                                </template>
                            </ol>

                            <div class="toggle"><span>{{ __('voyager::generic.close') }}</span><i class="voyager-double-right"></i></div>
                        </div>
                        <div class="flex">

                            <div id="left">

                                <ul id="files">

                                    <li v-for="(file,index) in files.items">
                                        <div class="file_link" :data-folder="file.name" :data-index="index">
                                            <div class="link_icon">
                                                <template v-if="file.type.includes('image')">
                                                    <div class="img_icon" :style="imgIcon(file.path)"></div>
                                                </template>
                                                <template v-if="file.type.includes('video')">
                                                    <i class="icon voyager-video"></i>
                                                </template>
                                                <template v-if="file.type.includes('audio')">
                                                    <i class="icon voyager-music"></i>
                                                </template>
                                                <template v-if="file.type.includes('zip')">
                                                    <i class="icon voyager-archive"></i>
                                                </template>
                                                <template v-if="file.type == 'folder'">
                                                    <i class="icon voyager-folder"></i>
                                                </template>
                                                <template
                                                        v-if="file.type != 'folder' && !file.type.includes('image') && !file.type.includes('video') && !file.type.includes('audio') && !file.type.includes('zip')">
                                                    <i class="icon voyager-file-text"></i>
                                                </template>

                                            </div>
                                            <div class="details" :data-type="file.type">
                                                <div :class="file.type">
                                                    <h4>@{{ file.filename }}</h4>
                                                    <small>
                                                        <template v-if="file.type == 'folder'">
                                                        <!--span class="num_items">@{{ file.items }} file(s)</span-->
                                                        </template>
                                                        <template v-else>
                                                            <span class="file_size">@{{ file.size }}</span>
                                                        </template>
                                                    </small>
                                                </div>
                                            </div>
                                        </div>
                                    </li>

                                </ul>

                                <div id="file_loader">
                                    <?php $admin_loader_img = Voyager::setting('admin.loader', ''); ?>
                                    @if($admin_loader_img == '')
                                        <img src="{{ voyager_asset('images/logo-icon.png') }}" alt="Voyager Loader">
                                    @else
                                        <img src="{{ Voyager::image($admin_loader_img) }}" alt="Voyager Loader">
                                    @endif
                                    <p>{{ __('voyager::media.loading') }}</p>
                                </div>

                                <div id="no_files">
                                    <h3><i class="voyager-meh"></i> {{ __('voyager::media.no_files_in_folder') }}</h3>
                                </div>

                            </div>

                            <div id="right">
                                <div class="right_none_selected">
                                    <i class="voyager-cursor"></i>
                                    <p>{{ __('voyager::media.nothing_selected') }}</p>
                                </div>
                                <div class="right_details">
                                    <div class="detail_img">
                                        <div :class="selected_file.type">
                                            <template v-if="selectedFileIs('image')">
                                                <img :src="selected_file.path"/>
                                            </template>
                                            <template v-if="selectedFileIs('video')">
                                                <video width="100%" height="auto" controls>
                                                    <source :src="selected_file.path" type="video/mp4">
                                                    <source :src="selected_file.path" type="video/ogg">
                                                    <source :src="selected_file.path" type="video/webm">
                                                    {{ __('voyager::media.browser_video_support') }}
                                                </video>
                                            </template>
                                            <template v-if="selectedFileIs('audio')">
                                                <i class="voyager-music"></i>
                                                <audio controls style="width:100%; margin-top:5px;">
                                                    <source :src="selected_file.path" type="audio/ogg">
                                                    <source :src="selected_file.path" type="audio/mpeg">
                                                    {{ __('voyager::media.browser_audio_support') }}
                                                </audio>
                                            </template>
                                            <template v-if="selectedFileIs('zip')">
                                                <i class="voyager-archive"></i>
                                            </template>
                                            <template v-if="selected_file.type == 'folder'">
                                                <i class="voyager-folder"></i>
                                            </template>
                                            <!--template
                                                    v-if="selected_file.type != 'folder' && !selectedFileIs('audio') && !selectedFileIs('video') && !selectedFileIs('image')">
                                                <i class="voyager-file-text-o"></i>
                                            </template>-->
                                        </div>

                                    </div>
                                    <div class="detail_info">
                                        <div :class="selected_file.type">
                                            <span><h4>{{ __('voyager::media.title') }}:</h4>
                                            <p>@{{selected_file.name}}</p></span>
                                            <span><h4>{{ __('voyager::media.type') }}:</h4>
                                            <p>@{{selected_file.type}}</p></span>

                                            <template v-if="selected_file.type != 'folder'">
                                                <span><h4>{{ __('voyager::media.size') }}:</h4>
                                                <p><span class="selected_file_count">@{{ selected_file.items }} item(s)</span><span
                                                    class="selected_file_size">@{{selected_file.size}}</span></p></span>
                                                <span><h4>{{ __('voyager::media.public_url') }}:</h4>
                                                <p><a :href="selected_file.path" target="_blank">Click Here</a></p></span>
                                                <span><h4>{{ __('voyager::media.last_modified') }}:</h4>
                                                <p>@{{ dateFilter(selected_file.last_modified) }}</p></span>
                                            </template>
                                        </div>
                                    </div>
                                </div>

                            </div><!-- #right -->

                        </div>

                        <div class="nothingfound">
                            <div class="nofiles"></div>
                            <span>{{ __('voyager::media.no_files_here') }}</span>
                        </div>

                    </div>

           
          

                    <!-- Image Modal -->
                    <div class="modal fade" id="imagemodal">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                </div>
                                <div class="modal-body">
                                    <img :src="selected_file.path" class="img img-responsive" style="margin: 0 auto;">
                                </div>

                                <div class="modal-footer text-left">
                                    <small class="image-title">@{{ selected_file.name }}</small>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- End Image Modal -->

                </div><!-- #filemanager -->

            </div><!-- .row -->
        </div><!-- .col-md-12 -->
    </div><!-- .page-content container-fluid -->

    <input type="hidden" id="storage_path" value="{{ storage_path() }}">
    <input type="hidden" id="base_url" value="{{ route('voyager.dashboard') }}">

@stop

@section('javascript')

<script>

    //Listen to backspace press to avoid reloading
    $(document).on("keydown", processKeyEvents);
    $(document).on("keypress", processKeyEvents);
    function processKeyEvents(event) {
        // Backspace
        if (event.keyCode == 8) {
            if ($("*:focus") != $("text")) {
                e.stopPropagation();
                event.preventDefault();
            }
        }
    }
    
    MediaManager();

    $(document).ready(function() {

        $("body").on("click","#filter_search_btn",function() {
            var search_txt = $('#filter_search').val();

            $("ul#files > li").each(function(index){
                var parent_li = $(this);
                var folder_name = parent_li.find('h4').text();

                if(folder_name.indexOf(search_txt) >= 0){
                    parent_li.show();
                }else{
                    parent_li.hide();
                }
            });

        });

        //We need to check if the ul#files changed and show again the li
        var targetNode = document.querySelector("ul#files");
        var observerOptions = {
          childList: true,
        }

        var files_observer = new MutationObserver(fileschanged);
        files_observer.observe(targetNode, observerOptions);

        function fileschanged(mutationList, observer) {
          mutationList.forEach((mutation) => {
            switch(mutation.type) {
              case 'childList':
                $("ul#files > li").show();
                /* One or more children have been added to and/or removed
                   from the tree; see mutation.addedNodes and
                   mutation.removedNodes */
                break;
            }
          });
        }

    });
</script>

@endsection