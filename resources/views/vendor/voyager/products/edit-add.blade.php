@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_title', __('voyager::generic.'.(!is_null($dataTypeContent->getKey()) ? 'edit' : 'add')).' '.$dataType->display_name_singular)

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i>
        {{ __('voyager::generic.'.(!is_null($dataTypeContent->getKey()) ? 'edit' : 'add')).' '.$dataType->display_name_singular }}
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-sm-12">

                <div class="panel panel-bordered">
                    <!-- form start -->
                    <form role="form"
                            class="form-edit-add form-horizontal"
                            action="@if(!is_null($dataTypeContent->getKey())){{ route('voyager.'.$dataType->slug.'.update', $dataTypeContent->getKey()) }}@else{{ route('voyager.'.$dataType->slug.'.store') }}@endif"
                            method="POST" enctype="multipart/form-data">
                        <!-- PUT Method if we are editing -->
                        @if(!is_null($dataTypeContent->getKey()))
                            {{ method_field("PUT") }}
                        @endif

                        <!-- CSRF TOKEN -->
                        {{ csrf_field() }}

                        <div class="panel-body">

                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <!-- Adding / Editing -->
                            @php
                                $dataTypeRows = $dataType->{(!is_null($dataTypeContent->getKey()) ? 'editRows' : 'addRows' )};

                                foreach($dataTypeRows as $row)
                                {
                                    $fields[$row->field] = $row;
                                }

                            @endphp

                            <div class="form-group">
                                <label class="control-label col-sm-2" for="add_to_website">Add To:</label>
                                <div class="col-sm-10">
                                    <div class="checkbox">
                                        <label>
                                            {!! app('voyager')->formField($fields['add_to_website'], $dataType, $dataTypeContent) !!}
                                            {{ $fields['add_to_website']->display_name }}
                                        </label>

                                        &nbsp;

                                        <label>
                                            {!! app('voyager')->formField($fields['add_to_app'], $dataType, $dataTypeContent) !!}
                                            {{ $fields['add_to_app']->display_name }}
                                        </label>

                                        &nbsp;

                                        <label>
                                            {!! app('voyager')->formField($fields['add_to_widget'], $dataType, $dataTypeContent) !!}
                                            {{ $fields['add_to_widget']->display_name }}
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-2" for="images">{{ $fields['images']->display_name }}:</label>
                                <div class="col-sm-10">
                                    {!! app('voyager')->formField($fields['images'], $dataType, $dataTypeContent) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-2" for="name">{{ $fields['name']->display_name }}:</label>
                                <div class="col-sm-10">
                                    {!! app('voyager')->formField($fields['name'], $dataType, $dataTypeContent) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-2" for="description">{{ $fields['description']->display_name }}:</label>
                                <div class="col-sm-10">
                                    {!! app('voyager')->formField($fields['description'], $dataType, $dataTypeContent) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-2" for="supplier">{{ $fields['product_hasone_supplier_relationship']->display_name }}:</label>
                                <div class="col-sm-10">

                                    @php
                                        $row = $fields['product_hasone_supplier_relationship'];
                                        $options = json_decode($row->details);
                                        $display_options = isset($options->display) ? $options->display : NULL;
                                    @endphp

                                    @include('voyager::formfields.relationship')

                                    @foreach (app('voyager')->afterFormFields($row, $dataType, $dataTypeContent) as $after)
                                        {!! $after->handle($row, $dataType, $dataTypeContent) !!}
                                    @endforeach

                                    @include('voyager::multilingual.input-hidden-bread-edit-add')

                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-2" for="category">{{ $fields['product_belongstomany_category_relationship']->display_name }}:</label>
                                <div class="col-sm-10">

                                    @php
                                        $row = $fields['product_belongstomany_category_relationship'];
                                        $options = json_decode($row->details);
                                        $display_options = isset($options->display) ? $options->display : NULL;
                                    @endphp

                                    @include('voyager::formfields.relationship')

                                    @foreach (app('voyager')->afterFormFields($row, $dataType, $dataTypeContent) as $after)
                                        {!! $after->handle($row, $dataType, $dataTypeContent) !!}
                                    @endforeach

                                    @include('voyager::multilingual.input-hidden-bread-edit-add')
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-2" for="barcode_image">{{ $fields['barcode_image']->display_name }}:</label>
                                <div class="col-sm-10">
                                    {!! app('voyager')->formField($fields['barcode_image'], $dataType, $dataTypeContent) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-2" for="barcode_unit">Barcode(s):</label>
                                <div class="col-sm-10">
                                    <div class="form-row">
                                        <div class="form-group col-sm-3">
                                            {!! app('voyager')->formField($fields['barcode_unit'], $dataType, $dataTypeContent) !!}
                                        </div>
                                        <div class="form-group col-sm-1">
                                        </div>
                                        <div class="form-group col-sm-3">
                                            {!! app('voyager')->formField($fields['barcode_outer'], $dataType, $dataTypeContent) !!}
                                        </div>
                                        <div class="form-group col-sm-1">
                                        </div>
                                        <div class="form-group col-sm-3">
                                            {!! app('voyager')->formField($fields['barcode_shipper'], $dataType, $dataTypeContent) !!}
                                        </div>
                                        <div class="form-group col-sm-1">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-2" for="barcode_default">{{ $fields['barcode_default']->display_name }}:</label>
                                <div class="col-sm-10">
                                    <div class="form-group col-sm-3">
                                        {!! app('voyager')->formField($fields['barcode_default'], $dataType, $dataTypeContent) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-2" for="rrp">{{ $fields['rrp']->display_name }}:</label>
                                <div class="col-sm-10">
                                    <div class="form-group col-sm-3">
                                        {!! app('voyager')->formField($fields['rrp'], $dataType, $dataTypeContent) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-2" for="items_per_outer">{{ $fields['items_per_outer']->display_name }}:</label>
                                <div class="col-sm-10">
                                    <div class="form-group col-sm-3">
                                        {!! app('voyager')->formField($fields['items_per_outer'], $dataType, $dataTypeContent) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-2" for="height">Dimensions:</label>
                                <div class="col-sm-10">
                                    <div class="form-row">
                                        <div class="form-group col-sm-3">
                                            {!! app('voyager')->formField($fields['height'], $dataType, $dataTypeContent) !!}
                                        </div>
                                        <div class="form-group col-sm-1">
                                        </div>
                                        <div class="form-group col-sm-3">
                                            {!! app('voyager')->formField($fields['width'], $dataType, $dataTypeContent) !!}
                                        </div>
                                        <div class="form-group col-sm-1">
                                        </div>
                                        <div class="form-group col-sm-3">
                                            {!! app('voyager')->formField($fields['depth'], $dataType, $dataTypeContent) !!}
                                        </div>
                                        <div class="form-group col-sm-1">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-2" for="weight">{{ $fields['weight']->display_name }}:</label>
                                <div class="col-sm-10">
                                    <div class="form-group col-sm-3">
                                        {!! app('voyager')->formField($fields['weight'], $dataType, $dataTypeContent) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-2" for="visible_from">Visible From: </label>
                                <div class="col-sm-10">
                                    <div class="form-row">
                                        <div class="form-group col-sm-2">
                                            {!! app('voyager')->formField($fields['visible_from'], $dataType, $dataTypeContent) !!}
                                        </div>
                                        <div class="form-group col-sm-3">
                                            <label class="control-label col-sm-2" for="visible_to">To: </label>
                                            <div class="col-sm-10">
                                                {!! app('voyager')->formField($fields['visible_to'], $dataType, $dataTypeContent) !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-2" for="email">Product Is:</label>
                                <div class="col-sm-10">
                                    <div class="checkbox">
                                        <label>
                                            {!! app('voyager')->formField($fields['product_is_new'], $dataType, $dataTypeContent) !!}
                                            {{ $fields['product_is_new']->display_name }}
                                        </label>

                                        &nbsp;

                                        <label>
                                            {!! app('voyager')->formField($fields['product_is_featured'], $dataType, $dataTypeContent) !!}
                                            {{ $fields['product_is_featured']->display_name }}
                                        </label>

                                        &nbsp;

                                        <label>
                                            {!! app('voyager')->formField($fields['product_is_merchandising'], $dataType, $dataTypeContent) !!}
                                            {{ $fields['product_is_merchandising']->display_name }}
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div><!-- panel-body -->

                        <div class="panel-footer">
                            <button type="submit" class="btn btn-primary btn-lg save">{{ __('voyager::generic.save') }}</button>
                        </div>
                    </form>

                    <iframe id="form_target" name="form_target" style="display:none"></iframe>
                    <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post"
                            enctype="multipart/form-data" style="width:0;height:0;overflow:hidden">
                        <input name="image" id="upload_file" type="file"
                                 onchange="$('#my_form').submit();this.value='';">
                        <input type="hidden" name="type_slug" id="type_slug" value="{{ $dataType->slug }}">
                        {{ csrf_field() }}
                    </form>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-danger" id="confirm_delete_modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager::generic.are_you_sure') }}</h4>
                </div>

                <div class="modal-body">
                    <h4>{{ __('voyager::generic.are_you_sure_delete') }} '<span class="confirm_delete_name"></span>'</h4>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                    <button type="button" class="btn btn-danger" id="confirm_delete">{{ __('voyager::generic.delete_confirm') }}
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Delete File Modal -->
@stop

@section('javascript')
    <script>

        // Validation for Visible from and To
        function validateExpiryDate(){

            var publish_date = $('.form-group input[name=visible_from]').val();
            var expiry_date = $('.form-group input[name=visible_to]').val();

            if(new Date(expiry_date) < new Date(publish_date))
            {
                return false;

            } else {
                return true;
            }
        }

        var params = {}
        var $image

        $('document').ready(function () {

            $('.toggleswitch').bootstrapToggle();

            //Init datepicker for date fields if data-datepicker attribute defined
            //or if browser does not handle date inputs
            $('.form-group input[type=date]').each(function (idx, elt) {
                if (elt.type != 'date' || elt.hasAttribute('data-datepicker')) {
                    elt.type = 'text';
                    $(elt).datetimepicker($(elt).data('datepicker'));
                }
            });

            $('.form-group input[type=date]').change(function() {
                if(!validateExpiryDate()) {
                    toastr.error("Expiry Date should be before Publish Date.");
                    $(this).val('');
                    return false;
                }
            });

            @if ($isModelTranslatable)
                $('.side-body').multilingual({"editing": true});
            @endif

            $('.side-body input[data-slug-origin]').each(function(i, el) {
                $(el).slugify();
            });

            $('.form-group').on('click', '.remove-multi-image', function (e) {
                e.preventDefault();
                $image = $(this).siblings('img');

                params = {
                    slug:   '{{ $dataType->slug }}',
                    image:  $image.data('image'),
                    media_id:  $image.data('media-id'),
                    id:     $image.data('id'),
                    field:  $image.parent().data('field-name'),
                    _token: '{{ csrf_token() }}'
                }

                $('.confirm_delete_name').text($image.data('image'));
                $('#confirm_delete_modal').modal('show');
            });

            $('#confirm_delete').on('click', function(){
                $.post('{{ route('voyager.media.remove') }}', params, function (response) {
                    if ( response
                        && response.data
                        && response.data.status
                        && response.data.status == 200 ) {

                        toastr.success(response.data.message);
                        $image.parent().fadeOut(300, function() { $(this).remove(); })
                    } else {
                        toastr.error("Error removing image.");
                    }
                });

                $('#confirm_delete_modal').modal('hide');
            });
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
    <script>
        $('document').ready(function () {
            var pageTitle = $('.page-title').text();
            $('.breadcrumb li:last-child').text(pageTitle);

            $('select[name="barcode_default"]').change(function (){
            });
        });
    </script>

    <!-- ajax call -->
    <script>
        $(document).ready(function () {
            $.ajaxSetup({
                headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var product_id = "<?php echo $dataTypeContent->getKey() ?>";

            $.ajax({
                type:"GET",
                data: { "product_id" : product_id },
                url: "{{ route('getExpiryDate') }}",

                success:function(data){
                    if($.trim(data)=="not found"){
                        $("input[type=date][name=visible_to]").val('');
                    }
                },
                error: function (data) {
                    console.log(data);
                }
            });
        });
    </script>
@stop
