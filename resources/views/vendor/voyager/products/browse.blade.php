@extends('vendor.voyager.bread.browse') 

@php

	// For the Search Bar
	$suppliers = \App\Models\Supplier::ordered()->get();
	$categories = \App\Models\Pcategory::ordered()->get();
	$elements = [
		View::make('elements.filters.supplier',['suppliers' => $suppliers,'use_id' => true]),
		View::make('elements.filters.category',['categories' => $categories,'use_id' => true])
	];

	// For the Bulk Edit
	$bulk_actions =[
		'delete' => 'Delete',
		'categories' => 'Edit Categories',
		'supplier' => 'Edit Suppliers',
		'expiry_date' => 'Edit Expiry Date',
		'export' => 'Export',
	];

	$bulk_actions_modal = [

		View::make('elements.bulk_actions.delete',
			[
				'field_to_edit' => null,
				'column' => null,
				'field_type' => null,
				'dataType' => $dataType,
			]),

		View::make('elements.bulk_actions.edit_supplier',
			[
				'field_to_edit' => 'Supplier',
				'column' => 'supplier_id',
				'field_type' => 'product_hasone_supplier_relationship',
				'dataType' => $dataType,
			]),

		View::make('elements.bulk_actions.edit_category',
			[
				'field_to_edit' => 'Category',
				'column' => 'pcategory_id',
				'field_type' => 'product_belongstomany_category_relationship',
				'dataType' => $dataType,
			]),

		View::make('elements.bulk_actions.edit_expiry',
			[
				'field_to_edit' => 'Expiry Date',
				'column' => 'visible_to',
				'field_type' => 'ExpiryDate',
				'dataType' => $dataType,
			]),

		View::make('elements.bulk_actions.export',
			[
				'field_to_edit' => null,
				'column' => null,
				'field_type' => null,
				'dataType' => $dataType,
			]),
	];

@endphp

@section('search_bar')
    @include('elements.extra_search_bar',['elements' => $elements])
@stop

