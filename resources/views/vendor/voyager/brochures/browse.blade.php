@extends('vendor.voyager.bread.browse')

@php
	$suppliers = \App\Models\Supplier::ordered()->get();
	$categories = DB::table('bcategories')->get();
	$elements = [
		View::make('elements.filters.supplier',['suppliers' => $suppliers]),
		View::make('elements.filters.category',['categories' => $categories])
	];

	// For the Bulk Edit
	$bulk_actions =[
		'delete' => 'Delete',
		'categories' => 'Edit Categories',
		'supplier' => 'Edit Suppliers',
		'expiry_date' => 'Edit Expiry Date',
		'export' => 'Export',
	];

	$bulk_actions_modal = [

		View::make('elements.bulk_actions.delete',
			[
				'field_to_edit' => null,
				'column' => null,
				'field_type' => null,
				'dataType' => $dataType,
			]),

		View::make('elements.bulk_actions.edit_supplier',
			[
				'field_to_edit' => 'Supplier',
				'column' => 'supplier_id',
				'field_type' => 'brochure_belongsto_supplier_relationship',
				'dataType' => $dataType,
			]),

		View::make('elements.bulk_actions.edit_category',
			[
				'field_to_edit' => 'Category',
				'column' => 'bcategory_id',
				'field_type' => 'brochure_belongstomany_category_relationship',
				'dataType' => $dataType,
			]),

		View::make('elements.bulk_actions.edit_expiry',
			[
				'field_to_edit' => 'Expiry Date',
				'column' => 'visible_to',
				'field_type' => 'ExpiryDate',
				'dataType' => $dataType,
			]),

		View::make('elements.bulk_actions.export',
			[
				'field_to_edit' => null,
				'column' => null,
				'field_type' => null,
				'dataType' => $dataType,
			]),
	];

@endphp

@section('search_bar')
    @include('elements.extra_search_bar',['elements' => $elements])
@stop