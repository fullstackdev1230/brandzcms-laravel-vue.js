@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_title', 'Brochure')

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i>
        Brochure Display Order
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-sm-12">

                <div class="panel panel-bordered">

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-4">
                                <h4>
                                    Categories:
                                </h4>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <td>Category Name</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($bcategories as $bc)
                                                <tr class="clickable-row {{ $bc->id == $brochureId ?'active':'' }}" data-href="{{ route('voyager.brochures.order.display') }}/{{ $bc->id }}">
                                                    <td>
                                                        {{ $bc->name }}
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="col-sm-8">
                                <h4>
                                    Brochures:
                                </h4>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <td>Image</td>
                                                <td>Name</td>
                                                <td>Filename</td>
                                                <td>Date Added</td>
                                            </tr>
                                        </thead>
                                        <tbody class="brochure-sortable">
                                            @foreach($brochures as $b)
                                                <tr data-display_order="{{ $b->display_order+1 }}" data-id="{{ $b->id }}">
                                                    <td>
                                                        <img src="{{ $b->display_image }}" height="100px" />
                                                    </td>
                                                    <td>
                                                        {{ $b->name }}
                                                    </td>
                                                    <td>
                                                    </td>
                                                    <td>
                                                        {{ \Carbon\Carbon::parse($b->created_at)->format('d/m/Y')}}
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript')
    <script>
        $(document).ready(function(){

            // Sortable
            $('.brochure-sortable').sortable({

                stop: function( event, ui ) {

                    var sortedList = [];

                    $(this).children().each(function(index) {
                        sortedList.push($(this).attr('data-id'));
                    });

                    $.ajax({
                        method: "POST",
                        url: "{{ route('voyager.brochures.order.change') }}",
                        data: {
                                order: sortedList,
                                _token: "{{ csrf_token() }}"
                            }
                    }).done(
                        function( msg ) {
                            toastr.success("Brochure Reordered");
                        }
                    );

                },
            });

            // Clicking the Brochure row
            $('.clickable-row').on('click',function(e){
                 window.location = $(this).data('href');
            });
        });
    </script>
@stop
