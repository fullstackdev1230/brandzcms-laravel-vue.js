@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_title', __('voyager::generic.'.(!is_null($dataTypeContent->getKey()) ? 'edit' : 'add')).' '.$dataType->display_name_singular)

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i>
        {{ __('voyager::generic.'.(!is_null($dataTypeContent->getKey()) ? 'edit' : 'add')).' '.$dataType->display_name_singular }}
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-sm-12">

                <div class="panel panel-bordered">
                    <!-- form start -->
                    <form role="form"
                            class="form-edit-add form-horizontal"
                            action="@if(!is_null($dataTypeContent->getKey())){{ route('voyager.'.$dataType->slug.'.update', $dataTypeContent->getKey()) }}@else{{  route('voyager.'.$dataType->slug.'.store') }}@endif"
                            method="POST" enctype="multipart/form-data">
                        <!-- PUT Method if we are editing -->
                        @if(!is_null($dataTypeContent->getKey()))
                            {{ method_field("PUT") }}
                        @endif

                        <!-- CSRF TOKEN -->
                        {{ csrf_field() }}

                        <div class="panel-body">

                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <!-- Adding / Editing -->
                            @php
                                $dataTypeRows = $dataType->{(!is_null($dataTypeContent->getKey()) ? 'editRows' : 'addRows' )};

                                foreach($dataTypeRows as $row)
                                {
                                    $fields[$row->field] = $row;
                                }
                            @endphp

                            <div class="form-group">
                                <label class="control-label col-sm-2" for="publish_date">Publish Date: </label>
                                <div class="col-sm-10">
                                    <div class="form-row">
                                        <div class="form-group col-sm-2">
                                            {!! app('voyager')->formField($fields['publish_date'], $dataType, $dataTypeContent) !!}
                                        </div>
                                        <div class="form-group col-sm-5">
                                            <label class="control-label col-sm-3" for="expiry_date">Expiry Date: </label>
                                            <div class="col-sm-6">
                                                {!! app('voyager')->formField($fields['expiry_date'], $dataType, $dataTypeContent) !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                @php
                                    $row = $fields['survey_belongstomany_group_relationship'];
                                    $options = json_decode($row->details);
                                    $display_options = isset($options->display) ? $options->display : NULL;
                                @endphp

                                <label class="control-label col-sm-2" for="supplier">{{ $row->display_name }}:</label>
                                <div class="col-sm-10">

                                    @include('voyager::formfields.relationship')

                                    @foreach (app('voyager')->afterFormFields($row, $dataType, $dataTypeContent) as $after)
                                        {!! $after->handle($row, $dataType, $dataTypeContent) !!}
                                    @endforeach

                                    @include('voyager::multilingual.input-hidden-bread-edit-add')
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-2" for="name">{{ $fields['survey_title']->display_name }}:</label>
                                <div class="col-sm-10">
                                    {!! app('voyager')->formField($fields['survey_title'], $dataType, $dataTypeContent) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-2" for="description">{{ $fields['survey_description']->display_name }}:</label>
                                <div class="col-sm-10">
                                    {!! app('voyager')->formField($fields['survey_description'], $dataType, $dataTypeContent) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-2" for="builder">Survey Builder:</label>
                                <div class="col-sm-10">

                                    <div id="added-questions">
                                        
                                        @if(!is_null($dataTypeContent->getKey()))
                                            @foreach($dataTypeContent->questions as $qkey => $question)
                                                
                                                <div class="added-question-wrapper">
                                                    <button type="button" class="btn btn-danger del-question"><span class="voyager-x icon"></span></button>
                                                   <div class="row">
                                                        <div class="col-md-6 question-divider">
                                                            <div class="row">
                                                                <div class="col-md-4">
                                                                    <input type="hidden" name="question[{{$qkey}}]['question_id']" value="{{$question->id}}">
                                                                    <select name="question[{{$qkey}}]['question_type']" class="question-type form-control" id="">
                                                                      <option {{ $question->question_type=='radio' ? 'selected' : '' }} value="radio">Radio Box</option>
                                                                      <option {{ $question->question_type=='checkbox' ? 'selected' : '' }} value="checkbox">Check Box</option>
                                                                      <option {{ $question->question_type=='text' ? 'selected' : '' }} value="text">Text</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-8">
                                                                    <input name="question[{{$qkey}}]['question_text']" type="text" class="form-control" value="{{ $question->question_text }}" placeholder="Enter Question?">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 options-wrapper question-divider" 
                                                        {{ $question->question_type == 'text' ? 'style=display:none;' : '' }}>
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="added-options-box">
                                                                        @if($question->question_type == 'text')
                                                                            <div class="row added-option">
                                                                                <div class="col-md-10 col-sm-10">
                                                                                    <input type="hidden" name="question[{{$qkey}}]['option_id'][]" value="">
                                                                                    <input name="question[{{$qkey}}]['option_text'][]" type="text" class="form-control" placeholder="Enter Options here">
                                                                                </div>
                                                                                <div class="col-md-2">
                                                                                    <button type="button" class="btn btn-warning del-option"><span class="voyager-trash icon"></span></button>
                                                                                </div>
                                                                            </div>
                                                                        @else
                                                                            @foreach($question->answers as $akey => $answer)
                                                                                    <div class="row added-option">
                                                                                        <div class="col-md-10 col-sm-10">
                                                                                            <input type="hidden" name="question[{{$qkey}}]['option_id'][]" value="{{$answer->id}}">
                                                                                            <input name="question[{{$qkey}}]['option_text'][]" type="text" class="form-control" value="{{ $answer->answer_text }}" placeholder="Enter Options here">
                                                                                        </div>
                                                                                        <div class="col-md-2">
                                                                                            <button type="button" class="btn btn-warning del-option"><span class="voyager-trash icon"></span></button>
                                                                                        </div>
                                                                                    </div>
                                                                            @endforeach
                                                                        @endif
                                                                        
                                                                    </div>
                                                                    <div class="add-option-wrapper">
                                                                        <button type="button" class="btn btn-success add-option"><span class="voyager-plus icon"></span></button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                   </div>
                                               </div><!-- added-question-wrapper -->
                                            @endforeach
                                        @else
                                            <div class="added-question-wrapper">
                                                <button type="button" class="btn btn-danger del-question"><span class="voyager-x icon"></span></button>
                                               <div class="row">
                                                    <div class="col-md-6 question-divider">
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <select name="question[0]['question_type']" class="question-type form-control" id="">
                                                                  <option value="radio">Radio Box</option>
                                                                  <option value="checkbox">Check Box</option>
                                                                  <option value="text">Text</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-8">
                                                                <input name="question[0]['question_text']" type="text" class="form-control" placeholder="Enter Question?">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 options-wrapper question-divider">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                
                                                                <div class="added-options-box">
                                                                    <div class="row added-option">
                                                                        <div class="col-md-10 col-sm-10">
                                                                            <input name="question[0]['option_text'][]" type="text" class="form-control" placeholder="Enter Options here">
                                                                        </div>
                                                                        <div class="col-md-2">
                                                                            <button type="button" class="btn btn-warning del-option"><span class="voyager-trash icon"></span></button>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="add-option-wrapper">
                                                                    <button type="button" class="btn btn-success add-option"><span class="voyager-plus icon"></span></button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                               </div>
                                           </div><!-- added-question-wrapper -->
                                        @endif
                                        
                                       


                                    </div><!-- #added-questions -->

                                    <div class="add-question-wrapper col-md-12">
                                        <button type="button" class="btn btn-success add-question">Add Question</button>
                                    </div>
                                    
                                </div>
                            </div>

                        </div><!-- panel-body -->

                        <div class="panel-footer">
                            <button type="submit" class="btn btn-primary btn-lg save"><span class="icon voyager-people"></span>{{ __('voyager::generic.save') }}</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-danger" id="confirm_delete_modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager::generic.are_you_sure') }}</h4>
                </div>

                <div class="modal-body">
                    <h4>{{ __('voyager::generic.are_you_sure_delete') }} '<span class="confirm_delete_name"></span>'</h4>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                    <button type="button" class="btn btn-danger" id="confirm_delete">{{ __('voyager::generic.delete_confirm') }}
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Delete File Modal -->
@stop

@section('javascript')
    <script>
        var params = {}
        var $image

        $('document').ready(function () {
            $('.toggleswitch').bootstrapToggle();

            //Init datepicker for date fields if data-datepicker attribute defined
            //or if browser does not handle date inputs
            $('.form-group input[type=date]').each(function (idx, elt) {
                if (elt.type != 'date' || elt.hasAttribute('data-datepicker')) {
                    elt.type = 'text';
                    $(elt).datetimepicker($(elt).data('datepicker'));
                }
            });

            @if ($isModelTranslatable)
                $('.side-body').multilingual({"editing": true});
            @endif

            $('.side-body input[data-slug-origin]').each(function(i, el) {
                $(el).slugify();
            });

            $('.form-group').on('click', '.remove-multi-image', function (e) {
                e.preventDefault();
                $image = $(this).siblings('img');

                params = {
                    slug:   '{{ $dataType->slug }}',
                    image:  $image.data('image'),
                    id:     $image.data('id'),
                    field:  $image.parent().data('field-name'),
                    _token: '{{ csrf_token() }}'
                }

                $('.confirm_delete_name').text($image.data('image'));
                $('#confirm_delete_modal').modal('show');
            });

            $('#confirm_delete').on('click', function(){
                $.post('{{ route('voyager.media.remove') }}', params, function (response) {
                    if ( response
                        && response.data
                        && response.data.status
                        && response.data.status == 200 ) {

                        toastr.success(response.data.message);
                        $image.parent().fadeOut(300, function() { $(this).remove(); })
                    } else {
                        toastr.error("Error removing image.");
                    }
                });

                $('#confirm_delete_modal').modal('hide');
            });
            $('[data-toggle="tooltip"]').tooltip();



            //Add Question for Survey Builder
            $('body').on('click', '.add-question', function (e) {
                e.preventDefault();
                var cloned_question_wrapper = $('.added-question-wrapper:first').clone(false,true);
                var count_questions = $('.added-question-wrapper').length;

                //set defaults
                cloned_question_wrapper.find('.options-wrapper').show();
                cloned_question_wrapper.find("input:hidden").val(null);
                cloned_question_wrapper.find("input:text").val("");
                cloned_question_wrapper.find('.added-option').not(':first').remove();

                //fix naming index for proper passing of question data
                cloned_question_wrapper.find("input").each(function(i) {
                    var input_name = $(this).attr('name');
                    input_name = input_name.replace(/\[[0-9]\]+/, "[" + count_questions +"]");
                    $(this).attr('name',input_name);
                });

                cloned_question_wrapper.find("select").each(function(i) {
                    var input_name = $(this).attr('name');
                    input_name = input_name.replace(/\[[0-9]\]+/, "[" + count_questions +"]");
                    $(this).attr('name',input_name);
                });

                $('#added-questions').append(cloned_question_wrapper);
            });

            //Delete Question
            $('body').on('click', '.del-question', function (e) {
                e.preventDefault();
                var current_question = $(this).parents('.added-question-wrapper');

                //count if there would still be an option left on the current question
                var question_count = $('.added-question-wrapper').length;

                if(question_count > 1){
                    current_question.remove();

                    //fix naming index for proper passing of question data
                    var count_questions = $('.added-question-wrapper').length;

                    $('.added-question-wrapper').each(function(index) {

                        $(this).find("input").each(function(i) {
                            var input_name = $(this).attr('name');
                            input_name = input_name.replace(/\[[0-9]\]+/, "[" + index +"]");
                            $(this).attr('name',input_name);
                        });

                        $(this).find("select").each(function(i) {
                            var input_name = $(this).attr('name');
                            input_name = input_name.replace(/\[[0-9]\]+/, "[" + index +"]");
                            $(this).attr('name',input_name);
                        });
                        
                    });

                    
                }else{
                    toastr.warning('There should be 1 or more questions!');
                }
                
            });

            //Add option
            $('body').on('click', '.add-option', function (e) {
                e.preventDefault();
                var parent_container = $(this).parents('.options-wrapper');
                var question_index = $(this).parents('.added-question-wrapper').index();
                var hidden_element = '<input type="hidden" name="question['+question_index+'][\'option_id\'][]">';
                var input_element = hidden_element+'<input name="question['+question_index+'][\'option_text\'][]" type="text" class="form-control" placeholder="Enter Options here">';

                var added_option_html = "<div class='row added-option'>"+
                                            "<div class='col-md-10'>"+
                                            input_element+
                                            "</div>"+
                                            "<div class='col-md-2'>"+
                                                "<button type='button' class='btn btn-warning del-option'><span class='voyager-trash icon'></span></button>"+
                                            "</div>"+
                                        "</div>";

                parent_container.find('.added-options-box').append(added_option_html);
                
            });

            //Delete option
            $('body').on('click', '.del-option', function (e) {
                e.preventDefault();
                //count if there would still be an option left on the current question
                var parent_container = $(this).parents('.options-wrapper');
                var option_count = parent_container.find('.added-option').length;

                if(option_count > 1){
                    $(this).parents('.added-option').remove();
                }else{
                    toastr.warning('There should be 1 or more options!');
                }
            });

            //Changing question type
            $('body').on('change','.question-type',function (e){
                var question_type = $(this).val();
                var question_wrapper = $(this).parents('.added-question-wrapper');

                if(question_type == 'text'){
                    question_wrapper.find('.options-wrapper').hide();
                }else{
                    question_wrapper.find('.options-wrapper').show();
                }

            });

            //Submitting the survey form data
            // $.ajax({
            //     method: "POST",
            //     url: "{{ route('voyager.brochures.order.change') }}",
            //     data: {
            //             order: sortedList,
            //             _token: "{{ csrf_token() }}"
            //         }
            // }).done(
            //     function( msg ) {
            //         toastr.success("Brochure Reordered");
            //     }
            // );


        });
    </script>
    <script>
        $('document').ready(function () {
            var pageTitle = $('.page-title').text();
            $('.breadcrumb li:last-child').text(pageTitle);
        });
    </script>
@stop
