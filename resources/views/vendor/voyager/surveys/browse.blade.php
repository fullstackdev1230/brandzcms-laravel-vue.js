@extends('vendor.voyager.bread.browse')

@php
	$dateColumn = 7;

	// For the Search Bar
    $elements = [
        View::make('elements.filters.date_range')
    ];

	// For the Bulk Edit
	$bulk_actions =[
		'delete' => 'Delete',
		'expiry_date' => 'Edit Expiry Date',
		'export' => 'Export',
	];

	$bulk_actions_modal = [

		View::make('elements.bulk_actions.delete',
			[
				'field_to_edit' => null,
				'column' => null,
				'field_type' => null,
				'dataType' => $dataType,
			]),

		View::make('elements.bulk_actions.edit_expiry',
			[
				'field_to_edit' => 'Expiry Date',
				'column' => 'expiry_date',
				'field_type' => 'ExpiryDate',
				'dataType' => $dataType,
			]),

		View::make('elements.bulk_actions.export',
			[
				'field_to_edit' => null,
				'column' => null,
				'field_type' => null,
				'dataType' => $dataType,
			]),
	];

@endphp

@section('search_bar')
    @include('elements.extra_search_bar',['elements' => $elements])
@stop

