@extends('voyager::master')

@section('page_title', __('voyager::generic.view').' '.$dataType->display_name_singular)

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i> {{ __('voyager::generic.viewing') }} {{ ucfirst($dataType->display_name_singular) }} &nbsp;

        @can('edit', $dataTypeContent)
            <a href="{{ route('voyager.'.$dataType->slug.'.edit', $dataTypeContent->getKey()) }}" class="btn btn-info">
                <span class="glyphicon glyphicon-pencil"></span>&nbsp;
                {{ __('voyager::generic.edit') }}
            </a>
        @endcan
        @can('delete', $dataTypeContent)
            <a href="javascript:;" title="{{ __('voyager::generic.delete') }}" class="btn btn-danger delete" data-id="{{ $dataTypeContent->getKey() }}" id="delete-{{ $dataTypeContent->getKey() }}">
                <i class="voyager-trash"></i> <span class="hidden-xs hidden-sm">{{ __('voyager::generic.delete') }}</span>
            </a>
        @endcan

        <a href="{{ route('voyager.'.$dataType->slug.'.index') }}" class="btn btn-warning">
            <span class="glyphicon glyphicon-list"></span>&nbsp;
            {{ __('voyager::generic.return_to_list') }}
        </a>
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content read container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered" style="padding-bottom:5px;">
                    <!-- form start -->

                    <div class="panel-body">

                        <div class="panel-body-title">
                            <h3>{{ $dataTypeContent->survey_title }}</h3>
                            <div class="sub-head">{{ $dataTypeContent->survey_description }}</div>
                        </div>
                        
                        <div class="fields-row row">
                            <div class="col-sm-2 col-md-2 field-name">Publish Date:</div>
                            <div class="col-sm-10 col-md-10 field-value"><strong>{{ $dataTypeContent->publish_date }}</strong></div>
                        </div>

                        <div class="fields-row row">
                            <div class="col-sm-2 col-md-2 field-name">Expiry Date:</div>
                            <div class="col-sm-10 col-md-10 field-value"><strong>{{ $dataTypeContent->expiry_date }}</strong></div>
                        </div>

                        <div class="fields-row row">
                            <div class="col-sm-2 col-md-2 field-name">Visible To:</div>
                            <div class="col-sm-10 col-md-10 field-value">
                                @if(!is_null($dataTypeContent->groups))
                                    <ul class="groups-list">
                                        @foreach($dataTypeContent->groups as $gkey => $group)
                                            <li><strong>{{ $group->group_name }}</strong></li>
                                        @endforeach
                                    </ul>
                                @else
                                    Not visible to any group
                                @endif
                            </div>
                        </div>
                        
                        <div class="fields-row row">
                           <hr>
                        </div>

                        <h4>Submitted Entries: {{ $dataTypeContent->submissions->count() }} </h4>
                        <br>

                       
                        @if(!is_null($dataTypeContent->questions))
                            @foreach($dataTypeContent->questions as $qkey => $question)
                             <div class="questions-row row">
                                <div class="question-box col-sm-push-1 col-md-push-1 col-sm-10 col-md-10">
                                    <div class="question-text"><strong>{{ $question->question_text }}</strong></div>
                                    
                                        @if($question->question_type !== 'text')
                                            <div class="question-options">
                                                @foreach($question->answers as $akey => $answer)
                                                    <div class="input-holder">
                                                        <input name="option_answer[{{$answer->survey_question_id}}]" type="{{$question->question_type}}" value="{{ strtolower(preg_replace('/[^a-zA-Z0-9-_\.]/','', $answer->answer_text)) }}">{{ $answer->answer_text }} 
                                                            @if($dataTypeContent->submissions->count() > 0)
                                                                <em class="survey-percentage">({{ number_format(($answer->chosen->count()/$dataTypeContent->submissions->count()) * 100,2) }}%)</em>
                                                            @endif
                                                    </div>
                                                @endforeach
                                            </div>
                                        @else
                                            <textarea class="form-control" name="option_answer"></textarea>
                                        @endif
                                    </div>
                                </div>
                            @endforeach
                        @else
                            No questions to be displayed
                        @endif
                            


                    </div>
                    
                </div>
            </div>
        </div>
    </div>

    {{-- Single delete modal --}}
    <div class="modal modal-danger fade" tabindex="-1" id="delete_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('voyager::generic.close') }}"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><i class="voyager-trash"></i> {{ __('voyager::generic.delete_question') }} {{ strtolower($dataType->display_name_singular) }}?</h4>
                </div>
                <div class="modal-footer">
                    <form action="{{ route('voyager.'.$dataType->slug.'.index') }}" id="delete_form" method="POST">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-danger pull-right delete-confirm"
                               value="{{ __('voyager::generic.delete_confirm') }} {{ strtolower($dataType->display_name_singular) }}">
                    </form>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@stop

@section('javascript')
    @if ($isModelTranslatable)
        <script>
            $(document).ready(function () {
                $('.side-body').multilingual();
            });
        </script>
        <script src="{{ voyager_asset('js/multilingual.js') }}"></script>
    @endif
    <script>
        var deleteFormAction;
        $('.delete').on('click', function (e) {
            var form = $('#delete_form')[0];

            if (!deleteFormAction) {
                // Save form action initial value
                deleteFormAction = form.action;
            }

            form.action = deleteFormAction.match(/\/[0-9]+$/)
                ? deleteFormAction.replace(/([0-9]+$)/, $(this).data('id'))
                : deleteFormAction + '/' + $(this).data('id');

            $('#delete_modal').modal('show');
        });

    </script>
@stop
