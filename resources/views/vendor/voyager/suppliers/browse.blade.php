@extends('vendor.voyager.bread.browse')

@php
	// For the Bulk Edit
	$bulk_actions =[
		'delete' => 'Delete',
		'export' => 'Export',
	];

	$bulk_actions_modal = [

		View::make('elements.bulk_actions.delete',
			[
				'field_to_edit' => null,
				'column' => null,
				'field_type' => null,
				'dataType' => $dataType,
			]),

		View::make('elements.bulk_actions.export',
			[
				'field_to_edit' => null,
				'column' => null,
				'field_type' => null,
				'dataType' => $dataType,
			]),
	];
@endphp

@section('search_bar')
    @include('elements.extra_search_bar')
@stop