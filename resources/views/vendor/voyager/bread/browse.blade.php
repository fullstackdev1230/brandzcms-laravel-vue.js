@extends('voyager::master')

@section('page_title', __('voyager::generic.viewing').' '.$dataType->display_name_plural)

@section('page_header')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-9 title-total">
                <h1 class="page-title">
                    <i class="{{ $dataType->icon }}"></i>
                    {{ $dataType->display_name_plural }}
                </h1>
                <h5>
                    Total: {{ $dataTypeContent->count() }}
                </h5>
            </div>
            <div class="col-sm-3 add-new-button">
                @can('add',app($dataType->model_name))
                    <a href="{{ route('voyager.'.$dataType->slug.'.create') }}" class="btn btn-add-new">
                        <i class="voyager-plus"></i>
                        <br />
                        <span>{{ __('voyager::generic.add_new') }}</span>
                    </a>
                @endcan
            </div>
        </div>
    </div>
@stop

@section('content')
    <div class="page-content browse container-fluid">
        @include('voyager::alerts')

        @if (!$isServerSide)
            @yield('search_bar',View::make('elements.search_bar'))
        @endif

        @if ($isServerSide)
            
                @if($dataType->slug == 'products')
                    <form method="get" class="form-inline" id="filter_form">
                        <div class="row">
                            <div class="col-md-12">
                               <div class="row">
                                    <div class="col-md-6 col-sm-9">
                                    <div class="form-group">
                                        <label for="filter_per_page">Per Page</label>
                                        <select name="filter_per_page" id="filter_per_page">
                                            <option {{ 10 == app('request')->input('filter_per_page') ? 'selected' : '' }} value="10">10</option>
                                            <option {{ 25 == app('request')->input('filter_per_page') ? 'selected' : '' }} value="25">25</option>
                                            <option {{ 50 == app('request')->input('filter_per_page') ? 'selected' : '' }} value="50">50</option>
                                            <option {{ 100 == app('request')->input('filter_per_page') ? 'selected' : '' }} value="100">100</option>
                                        </select>
                                    </div>

                                    @if(!empty($elements))
                                        @foreach($elements as $e)
                                            {!! $e !!}
                                        @endforeach
                                    </div>
                               </div>



                                <div class="row">
                                <div class="col-md-5 col-sm-9">
                                    <input type="text" id="filter_search"  class="input-search-custom form-control" style="width: auto;" placeholder="{{ __('voyager::generic.search') }}" name="s" value="{{ $search->value }}">

                                     <button type="submit" class="btn btn-primary" id="filter_submit">Filter</button>   
                                    
                                </div>
                                </div>

                                    @endif
                            </div>
                        </div>
                    </form>
                @else
                    <form method="get" class="form-search">
                        <div id="search-input">
                            <select id="search_key" name="key">
                                @foreach($searchable as $key)
                                    <option value="{{ $key }}" @if($search->key == $key){{ 'selected' }}@endif>{{ ucwords(str_replace('_', ' ', $key)) }}</option>
                                @endforeach
                            </select>
                            <select id="filter" name="filter">
                                <option value="contains" @if($search->filter == "contains"){{ 'selected' }}@endif>contains</option>
                                <option value="equals" @if($search->filter == "equals"){{ 'selected' }}@endif>=</option>
                            </select>
                            <div class="input-group col-md-12">
                                <input type="text" class="form-control" placeholder="{{ __('voyager::generic.search') }}" name="s" value="{{ $search->value }}">
                                <span class="input-group-btn">
                                    <button class="btn btn-info btn-lg" type="submit">
                                        <i class="voyager-search"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                    </form>
                @endif


        @endif

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        
                        <div class="table-responsive">
                            <table id="dataTable" class="table table-hover">
                                <thead>
                                    <tr>
                                        @can('delete',app($dataType->model_name))
                                            <th>
                                                <input type="checkbox" class="select_all">
                                            </th>
                                        @endcan
                                        @foreach($dataType->browseRows as $row)
                                        <th>
                                            @if ($isServerSide)
                                                <a href="{{ $row->sortByUrl() }}">
                                            @endif
                                            {{ $row->display_name }}
                                            @if ($isServerSide)
                                                @if ($row->isCurrentSortField())
                                                    @if (!isset($_GET['sort_order']) || $_GET['sort_order'] == 'asc')
                                                        <i class="voyager-angle-up pull-right"></i>
                                                    @else
                                                        <i class="voyager-angle-down pull-right"></i>
                                                    @endif
                                                @endif
                                                </a>
                                            @endif
                                        </th>
                                        @endforeach
                                        <th class="actions text-right">{{ __('voyager::generic.actions') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($dataTypeContent as $data)
                                    <tr>
                                        @can('delete',app($dataType->model_name))
                                            <td>
                                                <input type="checkbox" name="row_id" id="checkbox_{{ $data->getKey() }}" value="{{ $data->getKey() }}">
                                            </td>
                                        @endcan
                                        @foreach($dataType->browseRows as $row)
                                            <td>
                                                <?php
                                                    $options = json_decode($row->details);
                                                ?>
                                                @if($row->type == 'image')
                                                    <img src="@if( !filter_var($data->{$row->field}, FILTER_VALIDATE_URL)){{ Voyager::image( $data->{$row->field} ) }}@else{{ $data->{$row->field} }}@endif" style="width:100px">
                                                @elseif($row->type == 'relationship')
                                                    @include('voyager::formfields.relationship', ['view' => 'browse', 'show_count' => isset($show_count)?$show_count:null ])
                                                @elseif($row->type == 'select_multiple')
                                                    @if(property_exists($options, 'relationship'))

                                                        @foreach($data->{$row->field} as $item)
                                                            @if($item->{$row->field . '_page_slug'})
                                                            <a href="{{ $item->{$row->field . '_page_slug'} }}">{{ $item->{$row->field} }}</a>@if(!$loop->last), @endif
                                                            @else
                                                            {{ $item->{$row->field} }}
                                                            @endif
                                                        @endforeach

                                                    @elseif(property_exists($options, 'options'))

                                                        @if(is_array($data->{$row->field}))
                                                            @foreach($data->{$row->field} as $item)
                                                             {{ $options->options->{$item} . (!$loop->last ? ', ' : '') }}
                                                            @endforeach
                                                        @else
                                                            @foreach(json_decode($data->{$row->field}) as $item)
                                                             {{ $options->options->{$item} . (!$loop->last ? ', ' : '') }}
                                                            @endforeach
                                                        @endif


                                                    @endif

                                                @elseif($row->type == 'select_dropdown' && property_exists($options, 'options'))

                                                    @if($data->{$row->field . '_page_slug'})
                                                        <a href="{{ $data->{$row->field . '_page_slug'} }}">{!! $options->options->{$data->{$row->field}} !!}</a>
                                                    @else
                                                        {!! $options->options->{$data->{$row->field}} or '' !!}
                                                    @endif


                                                @elseif($row->type == 'select_dropdown' && $data->{$row->field . '_page_slug'})
                                                    <a href="{{ $data->{$row->field . '_page_slug'} }}">{{ $data->{$row->field} }}</a>
                                                @elseif($row->type == 'date' || $row->type == 'timestamp')
                                                {{ $options && property_exists($options, 'format') ? \Carbon\Carbon::parse($data->{$row->field})->formatLocalized($options->format) : $data->{$row->field} }}
                                                @elseif($row->type == 'checkbox')
                                                    @if($options && property_exists($options, 'on') && property_exists($options, 'off'))
                                                        @if($data->{$row->field})
                                                        <span class="label label-info">{{ $options->on }}</span>
                                                        @else
                                                        <span class="label label-primary">{{ $options->off }}</span>
                                                        @endif
                                                    @else
                                                    {{ $data->{$row->field} }}
                                                    @endif
                                                @elseif($row->type == 'color')
                                                    <span class="badge badge-lg" style="background-color: {{ $data->{$row->field} }}">{{ $data->{$row->field} }}</span>
                                                @elseif($row->type == 'text')
                                                    @include('voyager::multilingual.input-hidden-bread-browse')
                                                    <div class="readmore">{{ mb_strlen( $data->{$row->field} ) > 200 ? mb_substr($data->{$row->field}, 0, 200) . ' ...' : $data->{$row->field} }}</div>
                                                @elseif($row->type == 'text_area')
                                                    @include('voyager::multilingual.input-hidden-bread-browse')
                                                    <div class="readmore">{{ mb_strlen( $data->{$row->field} ) > 200 ? mb_substr($data->{$row->field}, 0, 200) . ' ...' : $data->{$row->field} }}</div>
                                                @elseif($row->type == 'file' && !empty($data->{$row->field}) )
                                                    @include('voyager::multilingual.input-hidden-bread-browse')
                                                    @if(json_decode($data->{$row->field}))
                                                        @foreach(json_decode($data->{$row->field}) as $file)
                                                            <a href="{{ Storage::disk(config('voyager.storage.disk'))->url($file->download_link) ?: '' }}" target="_blank">
                                                                {{ $file->original_name ?: '' }}
                                                            </a>
                                                            <br/>
                                                        @endforeach
                                                    @endif
                                                @elseif($row->type == 'rich_text_box')
                                                    @include('voyager::multilingual.input-hidden-bread-browse')
                                                    <div class="readmore">{{ mb_strlen( strip_tags($data->{$row->field}, '<b><i><u>') ) > 200 ? mb_substr(strip_tags($data->{$row->field}, '<b><i><u>'), 0, 200) . ' ...' : strip_tags($data->{$row->field}, '<b><i><u>') }}</div>
                                                @elseif($row->type == 'coordinates')
                                                    @include('voyager::partials.coordinates-static-image')
                                                @elseif($row->type == 'multiple_images')
                                                    @php $images = json_decode($data->{$row->field}); @endphp
                                                    @if($images)
                                                        @php $images = array_slice($images, 0, 3); @endphp
                                                        @foreach($images as $image)
                                                            <img src="@if( !filter_var($image, FILTER_VALIDATE_URL)){{ Voyager::image( $image ) }}@else{{ $image }}@endif" style="width:50px">
                                                        @endforeach
                                                    @endif
                                                @else
                                                    @include('voyager::multilingual.input-hidden-bread-browse')
                                                    <span>{{ $data->{$row->field} }}</span>
                                                @endif
                                            </td>
                                        @endforeach
                                        <td class="no-sort no-click" id="bread-actions">
                                            @foreach(Voyager::actions() as $action)
                                                @include('voyager::bread.partials.actions', ['action' => $action])
                                            @endforeach
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        @if ($isServerSide)
                            <div class="pull-left">
                                <div role="status" class="show-res" aria-live="polite">{{ trans_choice(
                                    'voyager::generic.showing_entries', $dataTypeContent->total(), [
                                        'from' => $dataTypeContent->firstItem(),
                                        'to' => $dataTypeContent->lastItem(),
                                        'all' => $dataTypeContent->total()
                                    ]) }}</div>
                            </div>
                            <div class="pull-right">
                                 @if($dataType->slug == 'products')
                                    {{ $dataTypeContent->appends([
                                        's' => $search->value,
                                        'order_by' => $orderBy,
                                        'filter_per_page' => $search->paging,
                                        'filter_supplier' => $search->supplier,
                                        'filter_category' => $search->category,
                                        'sort_order' => $sortOrder
                                    ])->links() }}
                                 @else
                                    {{ $dataTypeContent->appends([
                                        's' => $search->value,
                                        'order_by' => $orderBy,
                                        'filter' => $search->filter,
                                        'key' => $search->key,
                                        'sort_order' => $sortOrder
                                    ])->links() }}
                                 @endif
                                
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-12">

                  @can('edit',app($dataType->model_name))
                    @include('elements.bulk_edit')
                  @endcan

                  @can('edit',app($dataType->model_name))

                      @if(isset($dataType->order_column) && isset($dataType->order_display_column))
                          <a href="{{ route('voyager.'.$dataType->slug.'.order') }}" class="btn btn-primary">
                              <i class="voyager-list"></i> <span>{{ __('voyager::bread.order') }}</span>
                          </a>
                      @endif

                  @endcan

                  @include('voyager::multilingual.language-selector')

        </div>
      </div>
    </div>

    {{-- Single delete modal --}}
    <div class="modal modal-danger fade" tabindex="-1" id="delete_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('voyager::generic.close') }}"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><i class="voyager-trash"></i> {{ __('voyager::generic.delete_question') }} {{ strtolower($dataType->display_name_singular) }}?</h4>
                </div>
                <div class="modal-footer">
                    <form action="#" id="delete_form" method="POST">
                        {{ method_field("DELETE") }}
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-danger pull-right delete-confirm" value="{{ __('voyager::generic.delete_confirm') }}">
                    </form>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@stop

@section('css')

    @if(!$dataType->server_side && config('dashboard.data_tables.responsive'))
        <link rel="stylesheet" href="{{ voyager_asset('lib/css/responsive.dataTables.min.css') }}">
    @endif

@stop

@section('javascript')
    <!-- DataTables -->
    @if(!$dataType->server_side && config('dashboard.data_tables.responsive'))
        <script src="{{ voyager_asset('lib/js/dataTables.responsive.min.js') }}"></script>
    @endif
    <script>
        $(document).ready(function () {
            @if (!$dataType->server_side)

                if( $("#filter_date_from").length )
                {
                    // Data Table Date Range Filtering
                    $.fn.dataTableExt.afnFiltering.push(

                        function( oSettings, aData, iDataIndex ) {

                            // var dateColumn = 2;
                            // var startDate = new Date(document.getElementById('filter_date_from').value);
                            // var endDate = new Date(document.getElementById('filter_date_to').value);
                            // var filterDate = new Date(aData[dateColumn].substring(0,10));

                            var dateColumn = "<?php echo isset($dateColumn) ? $dateColumn : 0; ?>";
                            var startDate = new Date(document.getElementById('filter_date_from').value+"T00:00:00");
                            var endDate = new Date(document.getElementById('filter_date_to').value+"T00:00:00");
                            var dateFormat = aData[dateColumn].split("-");
                            var filterDate = new Date(dateFormat[2], dateFormat[1] - 1, dateFormat[0]);
                            

                            if ( isNaN(startDate) && isNaN(endDate) )
                            {
                                return true;
                            }
                            else if ( filterDate >= startDate  && isNaN(endDate))
                            {
                                return true;
                            }
                            else if ( filterDate <= endDate  && isNaN(startDate))
                            {
                                return true;
                            }
                            else if (filterDate <= endDate && filterDate >= startDate)
                            {
                                return true;
                            }
                            return false;
                        }
                    );
                }

                var table = $('#dataTable').DataTable({
                    dom: 'tip',
                    columnDefs: {
                        targets: -1,
                        searchable: false,
                        orderable: false
                    }
                });

                // Search
                $('#filter_search').keyup( function() {
                    table.search($(this).val()).draw();
                } );

                // Page
                $('#filter_per_page').change(function() {
                    table.page.len($(this).val()).draw();
                } );

                // Filter
                $('#filter_submit').on('click',function (e){

                    e.preventDefault();

                    table.draw();

                    $("#dataTable thead th").each( function ( i ) {

                        var columnName = $(this).html().trim().toLowerCase();

                        // Supplier
                        var supplierValue = $("#filter_supplier").val();
                        if( columnName == 'supplier' ) {

                            if(supplierValue) {
                                table.column( i ).search( supplierValue ).draw();
                            } else {
                                table.column( i ).search(".*",true).draw();
                            }

                            table.column( i ).search( supplierValue ).draw();
                        }

                        // Category
                        var categoryValue = $("#filter_category").val();
                        if( columnName == 'categories' ) {

                            if(categoryValue) {
                                table.column( i ).search( categoryValue ).draw();
                            } else {
                                table.column( i ).search(".*",true).draw();
                            }
                        }

                        // Role
                        var roleValue = $("#filter_role").val();
                        if( columnName == 'role' ) {

                            if(roleValue) {
                                table.column( i ).search( roleValue ).draw();
                            } else {
                                table.column( i ).search(".*",true).draw();
                            }
                        }
                    });
                });

            @else

                @if($dataType->name == 'products')
                    var table = $('#dataTable').DataTable({
                        dom: 'tip',
                        bPaginate: false,
                        bInfo: false,
                        columnDefs: {
                            targets: -1,
                            searchable: false,
                            orderable: false
                        }
                    });

                    // Search
                    $('#filter_search').keyup( function() {
                    //    table.search($(this).val()).draw();
                    } );
                @endif

                $('#search-input select').select2({
                    minimumResultsForSearch: Infinity
                });

            @endif

            @if ($isModelTranslatable)
                $('.side-body').multilingual();
                //Reinitialise the multilingual features when they change tab
                $('#dataTable').on('draw.dt', function(){
                    $('.side-body').data('multilingual').init();
                })
            @endif
            $('.select_all').on('click', function(e) {
                $('input[name="row_id"]').prop('checked', $(this).prop('checked'));
            });
        });


        var deleteFormAction;
        $('td').on('click', '.delete', function (e) {
            $('#delete_form')[0].action = '{{ route('voyager.'.$dataType->slug.'.destroy', ['id' => '__id']) }}'.replace('__id', $(this).data('id'));
            $('#delete_modal').modal('show');
        });
    </script>
    <script>
        $(document).ready(function () {
            var selectedCategory = "<?php 
                if (Session::has('filter_category'))
                {
                    echo Session::get('filter_category');
                }
             ?>";

            if(selectedCategory!=null){
                $("#filter_category").val(selectedCategory);
            }

            var selectedSupplier = "<?php
                if (Session::has('filter_supplier'))
                {
                    echo  Session::get('filter_supplier');
                }
              ?>";

            if(selectedSupplier!=null){
                $("#filter_supplier").val(selectedSupplier);
            }

            var selectedPerPage = "<?php echo Session::get('filter_per_page'); ?>";

            if(selectedPerPage!=null || selectedPerPage!=""){
                $("#filter_per_page").selected(selectedPerPage);
            }

        });
    </script>
@stop