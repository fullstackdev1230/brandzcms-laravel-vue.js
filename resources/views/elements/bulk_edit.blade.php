<div class="row">
	<div class="col-md-6">
		<form class="form-inline">
			<select name="bulk-edit-action" id="bulk-edit-action" class="col-md-3">
			    <option value=""> Bulk Actions </option>

				@if(isset($bulk_actions))
					@foreach($bulk_actions as $k => $v)
						@if(!empty($k))
							<option value="{{ $k }}"> {{ $v }} </option>
						@endif
					@endforeach
				@endif
			</select>
			&nbsp;
			<button type="submit" class="btn btn-primary col-md-2" id="bulk_actions_submit">Apply</button>
		</form>
	</div>
</div>

<div>
	@if(!empty($bulk_actions_modal))
		@foreach($bulk_actions_modal as $e)
			{!! $e !!}
		@endforeach
	@endif
</div>
