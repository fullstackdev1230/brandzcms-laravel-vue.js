<div class="row">
	<div class="col-md-12">
		<form class="form-inline">
			<div class="col-md-3">
				<div class="form-group">
					<label for="filter_per_page">Per Page</label>
					<select name="filter_per_page" id="filter_per_page">
						<option value="10">10</option>
						<option value="25">25</option>
						<option value="50">50</option>
						<option value="100">100</option>
					</select>
				</div>
			</div>

			<div class="col-md-6 text-center">

				@if(!empty($elements))
					@foreach($elements as $e)
						{!! $e !!}
					@endforeach

					<button type="submit" class="btn btn-primary" id="filter_submit">Filter</button>

				@endif

			</div>

			<div class="col-md-3">
				<div class="form-group float-right">
					<input type="type" name="filter_search" id="filter_search" placeholder="Search">
				</div>
			</div>
		</form>
	</div>
</div>
