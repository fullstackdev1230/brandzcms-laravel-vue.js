{{-- Bulk export modal --}}
<div class="modal modal-success fade" tabindex="-2" id="bulk_export_modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">
                    <i class="voyager-download"></i> {{ __('generic.are_you_sure_export') }} <span id="bulk_export_count"></span> <span id="bulk_export_display_name"></span>?
                </h4>
            </div>
            <div class="modal-body" id="bulk_export_modal_body">
            </div>
            <div class="modal-footer">
                <form action="{{ route('voyager.'.$dataType->slug.'.index') }}/export" id="bulk_export_form" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="ids" id="bulk_export_input" value="">
                    <input type="submit" class="btn btn-success pull-right export-confirm"
                             value="{{ __('generic.bulk_export_confirm') }} {{ strtolower($dataType->display_name_plural) }}"
                             onclick="hideModal();">
                </form>
                <button type="button" class="btn btn-default pull-right" data-dismiss="modal">
                    {{ __('generic.cancel') }}
                </button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

@section('javascript')

    <script type="text/javascript">
        $(document).ready(function(){

             // Bulk export selectors
            var $bulkExportBtn = $('#bulk_actions_submit');
            var $bulkExportModal = $('#bulk_export_modal');
            var $bulkExportCount = $('#bulk_export_count');
            var $bulkExportDisplayName = $('#bulk_export_display_name');
            var $bulkExportInput = $('#bulk_export_input');

            // Reposition modal to prevent z-index issues
            $bulkExportModal.appendTo('body');

            // Bulk export listener
            $bulkExportBtn.click(function (e) {

                e.preventDefault();

                var $dataToEdit = $( "#bulk-edit-action option:selected" ).val();

                if($dataToEdit == 'export') {

                    var ids = [];
                    var $checkedBoxes = $('#dataTable input[type=checkbox]:checked').not('.select_all');
                    var count = $checkedBoxes.length;

                    if (count) {

                        // Reset input value
                        $bulkExportInput.val('');

                        // Export info
                        var displayName = count > 1 ? '{{ $dataType->display_name_plural }}' : '{{ $dataType->display_name_singular }}';
                        displayName = displayName.toLowerCase();
                        $bulkExportCount.html(count);
                        $bulkExportDisplayName.html(displayName);

                        // Gather IDs
                        $.each($checkedBoxes, function () {
                            var value = $(this).val();
                            ids.push(value);
                        })

                        // Set input value
                        $bulkExportInput.val(ids);

                        // Show modal
                        $bulkExportModal.modal('show');

                    } else {

                        // No row selected
                        toastr.warning('{{ __('generic.bulk_export_nothing') }}');
                    }
                }
            });

        });

    </script>
    <script>
        function hideModal(){
            var $bulkExportModal = $('#bulk_export_modal');
            $bulkExportModal.modal('hide');
        }
    </script>

    @parent
@stop