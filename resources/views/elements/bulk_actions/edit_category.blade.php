{{-- Bulk edit modal --}}
<div class="modal modal-primary fade" tabindex="-2" id="bulk_edit_modal_{{ $column }}" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">
                    <i class="voyager-download"></i> {{ __('generic.are_you_sure_edit') }} <span id="bulk_edit_count_{{ $column }}"></span> <span id="bulk_edit_display_name_{{ $column }}"></span>?
                </h4>
            </div>
            <div class="modal-body" id="bulk_edit_modal_body_{{ $column }}">
                <form action="{{ route('voyager.'.$dataType->slug.'.index') }}/bulk-edit" id="bulk_edit_form_{{ $column }}" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="ids" id="bulk_edit_input_{{ $column }}" value="">
                    <input type="hidden" name="column" id="column" value="{{ $column }}">


                    <!-- Adding / Editing -->
                    @php
                        $dataTypeRows = $dataType->addRows;
                        $fields = [];

                        foreach($dataTypeRows as $row)
                        {
                            $fields[$row->field] = $row;
                        }

                        $dataTypeContent = new $dataType->model_name();

                    @endphp

                    <div class="form-group">
                        <label class="control-label col-sm-2" for="supplier">{{ $field_to_edit }}:</label>
                        <div class="col-sm-10">
                            @php
                                $row = $fields[$field_type];
                                $options = json_decode($row->details);
                                $display_options = isset($options->display) ? $options->display : NULL;
                            @endphp

                            @include('voyager::formfields.relationship')

                            @foreach (app('voyager')->afterFormFields($row, $dataType, $dataTypeContent) as $after)
                                {!! $after->handle($row, $dataType, $dataTypeContent) !!}
                            @endforeach

                            @include('voyager::multilingual.input-hidden-bread-edit-add')
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <input type="submit" class="btn btn-primary pull-right edit-confirm"
                         value="{{ __('generic.bulk_edit_confirm') }} {{ strtolower($dataType->display_name_plural) }}">
                </form>
                <button type="button" class="btn btn-default pull-right" data-dismiss="modal">
                    {{ __('generic.cancel') }}
                </button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

@section('javascript')

    <script type="text/javascript">
        $(document).ready(function(){

            // Bulk edit selectors
            var $bulkEditBtn = $('#bulk_actions_submit');
            var $bulkEditModal = $('#bulk_edit_modal_{{ $column }}');
            var $bulkEditCount = $('#bulk_edit_count_{{ $column }}');
            var $bulkEditDisplayName = $('#bulk_edit_display_name_{{ $column }}');
            var $bulkEditInput = $('#bulk_edit_input_{{ $column }}');

            // Reposition modal to prevent z-index issues
            $bulkEditModal.appendTo('body');

            // Bulk edit listener
            $bulkEditBtn.click(function (e) {

                e.preventDefault();

                var $dataToEdit = $( "#bulk-edit-action option:selected" ).val();

                if($dataToEdit == 'categories') {

                    var ids = [];
                    var $checkedBoxes = $('#dataTable input[type=checkbox]:checked').not('.select_all');
                    var count = $checkedBoxes.length;

                    if (count) {

                        // Reset input value
                        $bulkEditInput.val('');

                        // Edit info
                        var displayName = count > 1 ? '{{ $dataType->display_name_plural }}' : '{{ $dataType->display_name_singular }}';
                        displayName = displayName.toLowerCase();
                        $bulkEditCount.html(count);
                        $bulkEditDisplayName.html(displayName);

                        // Gather IDs
                        $.each($checkedBoxes, function () {
                            var value = $(this).val();
                            ids.push(value);
                        })
                        // Set input value
                        $bulkEditInput.val(ids);

                        // Show modal
                        $bulkEditModal.modal('show');

                    } else {
                        // No row selected
                        toastr.warning('{{ __('generic.bulk_edit_nothing') }}');
                    }
                }
            });

        });

    </script>
    @parent
@stop