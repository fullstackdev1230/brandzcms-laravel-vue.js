<select name="filter_category" id="filter_category">
    <option value=""> Category </option>

	@if($categories)
		@if(isset($use_id))
			@foreach($categories as $c)
				<option {{ $c->id == app('request')->input('filter_category') ? 'selected' : '' }} value="{{ $c->id }}"> {{ $c->name }} </option>
			@endforeach
		@else
			@foreach($categories as $c)
				<option {{ $c->id == app('request')->input('filter_category') ? 'selected' : '' }} value="{{ $c->name }}"> {{ $c->name }} </option>
			@endforeach
		@endif
	@endif
</select>