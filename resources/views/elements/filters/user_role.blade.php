<select name="filter_role" id="filter_role">
    <option value=""> Role </option>

	@if($roles)
		@foreach($roles as $c)
			<option value="{{ $c->display_name }}"> {{ $c->display_name }} </option>
		@endforeach
	@endif

</select>
