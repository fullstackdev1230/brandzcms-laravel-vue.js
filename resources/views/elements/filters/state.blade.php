<select name="filter_territory" id="filter_territory">
    <option value=""> Sales Territory </option>

	@if($states)
		@foreach($states as $k => $v)
			@if(!empty($k))
				<option value="{{ $v }}"> {{ $v }} </option>
			@endif
		@endforeach
	@endif
</select>