<select name="filter_group" id="filter_group">
    <option value=""> User Group </option>

	@if($groups)
		@foreach($groups as $c)
			<option value="{{ $c->group_name }}"> {{ $c->group_name }} </option>
		@endforeach
	@endif

</select>