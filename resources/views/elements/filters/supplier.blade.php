<select name="filter_supplier" id="filter_supplier">

	<option value=""> Supplier </option>

	@if($suppliers)
		@if(isset($use_id))
			@foreach($suppliers as $s)
				<option {{ $s->id == app('request')->input('filter_supplier') ? 'selected' : '' }} value="{{ $s->id }}"> {{ $s->name }} </option>
			@endforeach
		@else
			@foreach($suppliers as $s)
				<option value="{{ $s->name }}"> {{ $s->name }} </option>
			@endforeach
		@endif
	@endif

</select>