<!-- container -->
<div id="myContainer">

	<!-- toolbox -->

    <div class="row">
        <div class="col-md-4 col-sm-4"><button type="button" class="btn btn-primary" onclick="redips.merge(); setDefaultWidth();" title="Merge marked table cells horizontally and verically" style="width: 100%; height: 60px;">Merge</button></div>
        <div class="col-md-4 col-sm-4"><button type="button" class="btn btn-primary" onclick="redips.split('h'); setDefaultWidth();" title="Split marked table cell horizontally" style="width: 100%; height: 60px;">Split H</button></div>
        <div class="col-md-4 col-sm-4"><button type="button" class="btn btn-primary" onclick="redips.split('v'); setDefaultWidth();" title="Split marked table cell vertically" style="width: 100%; height: 60px;">Split V</button></div>
    </div>


	{{--
		<td>
			<input type="button" value="Row +" class="button" onclick="redips.row('insert')" title="Add table row"/>
			<input type="button" value="Row -" class="button" onclick="redips.row('delete')" title="Delete table row"/>
		</td>
		<td>
			<input type="button" value="Col +" class="button" onclick="redips.column('insert')" title="Add table column"/>
			<input type="button" value="Col -" class="button" onclick="redips.column('delete')" title="Delete table column"/>
		</td>
	--}}

	<!-- main table -->
	<div id="mainTableHolder">
		<table id="mainTable" style="width:94%; min-width: 280px;">
			<tbody>
				<tr>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
				</tr>
			</tbody>
		</table>
	</div>

</div>
