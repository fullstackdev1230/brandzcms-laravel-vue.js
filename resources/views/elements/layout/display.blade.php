@php

	// For the Search Bar
	$imageDetails = DB::table('layout_image_links')->where('layout_id', $dataTypeContent->getKey())->get();
	$tableCells = json_encode($imageDetails);
	$selected_td = 17;
@endphp
<div class="panel panel-bordered" style="padding-bottom:5px;">
	<div class="panel-heading" style="border-bottom:0;">
		<h3 class="panel-title">Layout</h3>
	</div>

	<div class="panel-body" style="padding-top:0;">
		<div id="myContainer">
			<p>{{ $fields['name']->display_name }}: {{ $dataTypeContent->name }}   </p>
			<!-- main table -->
			<div class="table">
				{!! $dataTypeContent->table_html !!}
			</div>
		</div>
	</div>

</div>

{{-- Add Layout Image modal --}}
<div class="modal modal-primary fade" tabindex="-2" id="add_layout_image_modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">
                    <i class="voyager-plus"></i>Add Layout Image
                </h4>
            </div>
            <div class="modal-body" id="add_layout_image_modal_body">

				 <form role="form" class="form-edit-add" action="{{ route('voyager.layout-image-links.store') }}" method="POST" enctype="multipart/form-data" id="add_layout_image_form">

				 	 <input type="hidden" name="layout_id" id="layout_id" value="">

                     <input type="hidden" name="td_id" id="td_id" value="">

					<!-- CSRF TOKEN -->
					{{ csrf_field() }}

					<div class="panel-body">
						<div class="form-group  col-md-12">
							<label for="name">Image</label>
							<input name="image" accept="image/*" type="file">
						</div>
						
						<div class="form-group  col-md-12">
							<label for="name">Data Type</label>
							<select name="data_type" id="" class="form-control">
								<option value="brochure">Brochure</option>
								<option value="product">Product</option>
								<option value="supplier">Supplier</option>
								<option value="category">Category</option>
								<option value="url">URL</option>
							</select>
						</div>

						<div class="form-group  col-md-12">
							<label for="name">Data Type Value</label>
							<input class="form-control" name="data_type_value" placeholder="Data Type Value" value="" type="text">
						</div>

					</div><!-- panel-body -->

					<div class="panel-footer">
						<button type="submit" id="submit_btn" class="btn btn-primary save">Save</button>
					</div>
				</form>

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


{{-- Edit Layout Image modal --}}
<div class="modal modal-primary fade" tabindex="-2" id="edit_layout_image_modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">
                    <i class="voyager-plus"></i>Edit Layout Image
                </h4>
            </div>
            <div class="modal-body" id="edit_layout_image_modal_body">




            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@section('javascript')
<script>
	$(document).ready(function () {
			$('td').css('cursor','pointer');
			$('td').text('');
			$('td').append('<i class="voyager-plus">');


			var imageDetails = <?php echo $tableCells ?>;

			if(imageDetails!=null){

				for(var n = 0; n < imageDetails.length; n++){

					var imageUrl = imageDetails[n].image;

					imageUrl = '"'+ decodeURI(imageUrl) + '"';
					$('#'+imageDetails[n].td_id).css('background-image', 'url(' + imageUrl + ')');
					$('#'+imageDetails[n].td_id).data('layoutImageLink_id', imageDetails[n].id);


				}
			}


			$('td').click( function() {
                var layout_id = "{{ $dataTypeContent->getKey() }}";
                $('#layout_id').val(layout_id);

				var td_id = $(this).attr("id");

				$('#td_id').val(td_id); //assign selected td_id to hidden field
				var hasImage = $(this).data("layoutImageLink_id");

				if(hasImage==null){
					$('#add_layout_image_modal').modal('show');
				}else{

					var url = window.location.href
					url = url.substr(0, url.indexOf("layouts")) + 'layout-image-links/' + hasImage + '/edit';
					//redirect to layout-image-links edit page
					window.location.href= url;
				}

			});

		});

	</script>

@stop